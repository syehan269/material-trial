    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0">
            <!-- <input class="search-fill m-0 col-6 border-0" type="text" name="search" id="searh" placeholder="Search" style="margin-left: 20px;"> -->
            <div class="dropdown ml-auto">
                <button type="button" class="btn c-color-primary text-white" id="dropdownNotiButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Notifications <span class="badge badge-light">0</span>
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownNotiButton">
                    <a class="dropdown-item d-none">TOP hutang id 10912 tanggal 06-10-2020</a>
                </div>
            </div>
            <div class="dropdown ml-3">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url('index.php/C_profile/index') ?>">Profil Instansi</a>
                    <hr>
                    <a class="dropdown-item" href="<?php echo base_url('index.php/Welcome/logout') ?>">Log out</a>
                </div>
            </div>
        </div>
        <!-- main section -->
        <div class="d-inline-flex col-12 p-0 mt-3">
            <div class="col-6 p-0" style="margin-left: 10px; margin-right: 30px;">
                <p class="mb-0 c-text-7 text-color medium-weight">Halo <?php echo $this->session->userdata("name"); ?> !</p>
                <p class="c-text-3 soft-title mb-3 medium-weight" id="date"></p>
                <?php if($this->session->userdata("user_level") == "kasir"): ?>
                    <div class="col-12 d-inline-flex p-0">
                        <a href="<?php echo base_url() ?>index.php/c_income/input" class="mr-auto">
                            <button class="c-text-2 my-auto quick-access-btn c-color-primary text-white medium-weight">
                                Tambah Transaksi Masuk
                            </button>
                        </a>
                        <a href="<?php echo base_url() ?>index.php/c_outcome/input" class="ml-auto">
                            <button class="c-text-2 my-auto quick-access-btn c-color-primary text-white medium-weight">
                                Tambah Transaksi Keluar
                            </button>
                        </a>
                    </div>
                <?php endif; ?>
                <div class="col-12 main-card mt-3">
                    <!-- <div class="text-center text-danger">CARD BODY</div> -->
                    <img src="<?php echo base_url() ?>asset/main.jpg" alt="" srcset="" style="width: 500px; height: 300px;">
                </div>
                <?php if($this->session->userdata("user_level") == "admin"): ?>
                    <div class="col-12 bottom-card mt-n3 d-inline-flex">
                        <p class="my-auto text-white c-text-3 regular-weight">Lihat Laporan Transaksi & Pengeluaran</p>
                        <a href="<?php echo base_url('index.php/c_laporan_keuangan') ?>" class="ml-auto">
                            <button class="white-button">Lihat Laporan</button>
                        </a>    
                    </div>
                <?php endif; ?>
                <div class="col-12 main-card mt-3">
                    <!-- <div class="text-center text-danger">CARD BODY</div> -->
                    <canvas id="myChart" width="400" height="250"></canvas>
                </div>
            </div>
            <!-- second section -->
            <div class="col-6 p-0" style="margin-right: 10px;">
                <!-- diagram section -->
                <p class="c-text-3 soft-title my-auto medium-weight">Overview Transaksi</p>
                <div class="col-11 main-card-1 mb-4 mt-4 p-2 d-inline-flex">
                    <div class="bottom-card" style="width: 180px; height: 180px;">
                        <canvas id="chartTrans" width = "400" height = "400"></canvas>
                    </div>
                    
                    <div class="ml-3 mt-4 mb-3">
                        <span class="mb-3 soft-title">
                            <span class="">Rp. </span>
                            <p class="c-text-primary c-text-6 medium-weight" id="totalTrans"><?php echo number_format($sum_out - $sum_in, 0, ".","."); ?></p> 
                        </span>
                        <div class="mt-3">
                            <span class="soft-title d-inline-flex">
                                <hr class="col-1 hr-second my-auto">
                                <p class="my-auto ml-2 regular-weight">Keluar</p>
                            </span><br>
                            <span class="soft-title d-inline-flex">
                                <hr class="col-1 hr-primary my-auto">
                                <p class="my-auto ml-2 regular-weight">Masuk</p>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- Sold & purchased goods section -->
                <div class="col-11 p-0 mt-2 mb-3 d-inline-flex">
                    <div class=" p-0" style="width: 220px;">
                        <span class="soft-title c-text-3 medium-weight">Transaksi Masuk</span>
                        <div class="col-12 mt-4 mr-2 main-card-2 p-3 d-inline-flex">
                            <span class="py-2 px-4 circle-1 d-flex justify-content-center">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAB3UlEQVRoQ+2Z4U3EMAyFnyeADYANYAOYADaA2wAmgQ24EWAC2AA24G4CYAIjS8kprZLU9aVpD6W/KjWJ3/ecpE1NOPCLDlw/GsDcGTRngJkvATwDON0TYgNgRUTvlnH2AfgqIN5r3hDRWXEAZhZ3HwGcFxSr1SmZ+QTwQERyH72SGXDiPwAcayNO1O4HwEUKIgfwAuB6IlFjh30loptYpxzA9wLcH1wjOQAOiYmo05aZO8/HWtpvPzR+/7nv3wD2dX7n5ECGWwZSDpTKQH+c/hornoGphPtxG4DWgakyoY1v3kanEn4QU0g+xYc+nxebAWa+c+eINRGtUplcJEAg3utOQiwOICLeQ1zFplN1gNy8zoiXo+Q6No2qAjCziLh1Z9uOIIt4AaoGEIj3Ru5ctYqvBuD+TLxFpoDfXeSvRf9KTpuwYc0M+G1R825Tia+WgeCtqYFQi68O4ALmIEaJnwUgAzFa/GwAEQiT+FkBAgikXlKa1V5tF9KIsbRpAFoHLO5q+mjjtxOZxk1Lm5YBrQMWdzV9tPFza0AKC0eaYBXabIkoWov71wUOIZYa1dxZ+JUa3egSk/skEIgnV+Q7qTBVwhBbZ+C9qchXWaw5nLlObI5YuGMDKGzo6OH+ALwXjECjBSpNAAAAAElFTkSuQmCC"/>
                            </span>
                            <div class="mx-auto my-auto">
                                <p class="c-text-5 c-text-primary mb-0 regular-weight" id="income"><?php echo $count_trans_in ?></p>
                                <p class="soft-title c-text-3 mb-0 regular-weight">Transaksi</p>
                            </div>
                        </div>
                    </div>
                    <div class=" p-0 ml-auto" style="width: 220px;">
                        <span class="soft-title c-text-3 medium-weight">Transaksi Keluar</span>
                        <div class="col-12 mt-4 mr-2 main-card-2 p-3 d-inline-flex">
                            <span class="py-2 px-4 circle-1 d-flex justify-content-center">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAB3UlEQVRoQ+2Z4U3EMAyFnyeADYANYAOYADaA2wAmgQ24EWAC2AA24G4CYAIjS8kprZLU9aVpD6W/KjWJ3/ecpE1NOPCLDlw/GsDcGTRngJkvATwDON0TYgNgRUTvlnH2AfgqIN5r3hDRWXEAZhZ3HwGcFxSr1SmZ+QTwQERyH72SGXDiPwAcayNO1O4HwEUKIgfwAuB6IlFjh30loptYpxzA9wLcH1wjOQAOiYmo05aZO8/HWtpvPzR+/7nv3wD2dX7n5ECGWwZSDpTKQH+c/hornoGphPtxG4DWgakyoY1v3kanEn4QU0g+xYc+nxebAWa+c+eINRGtUplcJEAg3utOQiwOICLeQ1zFplN1gNy8zoiXo+Q6No2qAjCziLh1Z9uOIIt4AaoGEIj3Ru5ctYqvBuD+TLxFpoDfXeSvRf9KTpuwYc0M+G1R825Tia+WgeCtqYFQi68O4ALmIEaJnwUgAzFa/GwAEQiT+FkBAgikXlKa1V5tF9KIsbRpAFoHLO5q+mjjtxOZxk1Lm5YBrQMWdzV9tPFza0AKC0eaYBXabIkoWov71wUOIZYa1dxZ+JUa3egSk/skEIgnV+Q7qTBVwhBbZ+C9qchXWaw5nLlObI5YuGMDKGzo6OH+ALwXjECjBSpNAAAAAElFTkSuQmCC"/>
                            </span>
                            <div class="mx-auto my-auto">
                                <p class="c-text-5 c-text-primary mb-0 regular-weight" id="outcome"><?php echo $count_trans_out ?></p>
                                <p class="soft-title c-text-3 mb-0 regular-weight">Transaksi</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- table history -->
                <div class="mt-2">
                    <span class="soft-title c-text-3  medium-weight">Transaksi Masuk Terbaru</span>
                    <div class="custom-card col-11 mt-4 pt-3 pb-3 pl-0 pr-0">
                        <table class="col-11 mx-auto" width="100%">
                            <thead>
                                <tr class="t-header col-12">
                                    <th class="p-3 primary-title medium-weight">Barang</th>
                                    <th class="p-3 primary-title medium-weight">Deskripsi</th>
                                </tr>
                            </thead>
                            <tbody id="tb_new">
                                <?php
                                    foreach ($recent as $item ) {
                                ?>
                                    <tr class="t-item">
                                        <td class="p-3"><?php echo $item->detail_in_material_name ?></td>
                                        <td class="p-3"><?php echo  "Rp ".number_format($item->detail_in_material_price * $item->detail_in_material_amount, 0,".",".") ?></td>
                                    </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <div class="mt-2">
                    <span class="soft-title c-text-3  medium-weight">Notifikasi Barang Menipis & Kosong</span>
                    <div class="custom-card col-11 mt-4 pt-3 pb-3 pl-0 pr-0">
                        <table class="col-11 mx-auto" id="table-tipis" width="100%">
                            <thead>
                                <tr class="t-header col-12">
                                    <th class="p-3 primary-title medium-weight">Barang</th>
                                    <th class="p-3 primary-title medium-weight">Deskripsi</th>
                                </tr>
                            </thead>
                            <tbody id="tb_tipis">
                                <?php
                                    foreach ($minimum as $item ) {
                                ?>
                                    <tr class="t-item">
                                        <td class="p-3"><?php echo $item->material_name ?></td>
                                        <td class="p-3"><?php if($item->material_stock == '0'){ echo "Stock habis";}else{ echo number_format($item->material_stock, 0,".",".")." stock tersisa";} ?></td>
                                    </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        setChart();
        //getChartData();
        getNewDate();
        //setTrans();
        //setNewUpdate();

        var table = "";

        dataTable();

        function dataTable(){
            table = $('#table-tipis').DataTable({
                stateSave: true,
                "lengthChange": false,
                "bInfo" : false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });

        }

        function setNewUpdate() {
            $.ajax({
                type: "get",
                url: "http://153.92.4.88:8080/transaction-in",
                dataType: "text",
                success: function (response) {
                    const obj = JSON.parse(response);
                    var getLength = obj.data.length;
                    var getNewest = parseInt(getLength)-1;
                    const context = obj.data[getNewest];
                    var getId = context.trans_in_id;
                    getNewestItem(getId);

                }
            });
        }

        function getNewestItem(id) {
            $.ajax({
                type: "get",
                url: "http://153.92.4.88:8080/transaction-in/"+id,
                dataType: "text",
                success: function (response) {
                    const obj = JSON.parse(response);
                    var payload = '<tr class="t-item">'
                                    '<th class="p-3"></th>'
                                    '<th class="p-3"></th>'
                                  '</tr>';

                    for (let i = 0; i < obj.data.detail.length; i++) {
                        const context = obj.data.detail[i];
                        var getMat = context.detail_in_material_name;
                        var getQuant = context.detail_in_material_amount;
                        var getPrice = context.detail_in_material_price;
                        var setPrice = getQuant*getPrice;
                        
                        var finPrice = "+ Rp "+setPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        console.log(getMat+" "+finPrice);
                        payload += '<tr class="t-item">'+
                                        '<td class="p-3 ">'+getMat+'</td>'+
                                        '<td class="p-3">'+finPrice+'</td>'+
                                    '</tr>';

                    }
                    $("#tb_new").html(payload);
                }
            });
        }

        function setTrans(){
            $.ajax({
                type: "get",
                url: "http://153.92.4.88:8080/transaction-in",
                dataType: "text",
                success: function (response) {
                    const obj = JSON.parse(response);
                    var inLength = obj.data.length;
                    var final = inLength.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");;
                    $("#income").text(final);
                }
            });

            $.ajax({
                type: "get",
                url: "http://153.92.4.88:8080/transaction-out",
                dataType: "text",
                success: function (response) {
                    const obj = JSON.parse(response);
                    var inLength = obj.data.length;
                    var final = inLength.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");;
                    $("#outcome").text(final);
                }
            });
        }

        function getNewDate() {
            var newDate = moment().format('D MMMM YYYY');
            var newDay = moment().format('dddd');
            $("#date").text(newDay+", "+newDate);
        }

        function getChartData() {
            $.ajax({
                type: "get",
                url: "http://153.92.4.88:8080/transaction-out/",
                dataType: "text",
                success: function (response) {
                    const obj = JSON.parse(response);
                    var getTotalOut = 0;

                    for (let i = 0; i < obj.data.length; i++) {
                        const element = obj.data[i];
                        var getAmount = element.trans_out_payment_amount;
                        getTotalOut += getAmount;
                    }

                    getChartDataIn(getTotalOut);
                }
            });
        }

        function getChartDataIn(totalOut) {
            $.ajax({
                type: "get",
                url: "http://153.92.4.88:8080/transaction-in",
                dataType: "text",
                success: function (response) {
                    const obj = JSON.parse(response);
                    var getTotalIn = 0;
                    
                    for (let o = 0; o < obj.data.length; o++) {
                        const context = obj.data[o];
                        var getAmount = context.trans_in_payment_amount;
                        getTotalIn += getAmount;
                    }
                    //console.log(getTotalIn);
                    //console.log(totalOut);
                    var result = parseInt(getTotalIn) - parseInt(totalOut);
                    var amount = result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    //console.log(amount);
                    $("#totalTrans").text(amount);
                    setChart(totalOut, getTotalIn);

                }
            });
        }

        function setChart() {
            var ctx = $("#chartTrans");
            var transOut = '<?php echo $sum_out ?>';
            var transIn = '<?php echo $sum_in ?>';
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: ['Outcome', 'Income'],
                    datasets: [{
                        label: '# of votes',
                        data: [transOut, transIn],
                        backgroundColor: [
                            '#ffbb00',
                            '#fff'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    legend: {
                        display: false
                    }
                }
            });
        }
    });
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>
