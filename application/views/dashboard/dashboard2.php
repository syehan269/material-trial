<div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0">
            <!-- <input class="search-fill m-0 col-6 border-0" type="text" name="search" id="searh" placeholder="Search" style="margin-left: 20px;"> -->
            <div class="dropdown ml-auto">
                <button type="button" class="btn c-color-primary text-white" id="dropdownNotiButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Notifications <span class="badge badge-light">4</span>
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownNotiButton">
                    <a class="dropdown-item">TOP hutang id 10912 tanggal 06-10-2020</a>
                    <hr>
                    <a class="dropdown-item">TOP hutang id 10915 tanggal 07-10-2020</a>
                </div>
            </div>
            <div class="dropdown ml-3">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url('index.php/C_profile/index') ?>">Profil Instansi</a>
                    <hr>
                    <a class="dropdown-item" href="<?php echo base_url('index.php/Welcome/logout') ?>">Log out</a>
                </div>
            </div>
        </div>
        <!-- main section -->
        <div class="col-12 px-0 py-2 mt-5">
            <div class="col-12 d-inline-flex">
                <div class="px-2 col-4">
                    <div class="card border-0 px-4 py-3" style="height: 180px; background-color: #5756B3">
                        <div class="mt-n5 mb-3">
                            <div class="card border-0 p-2" style="width: 55px; height: auto">
                                <i class='bx bx-dollar-circle bx-md text-center' style='color:#5756b3'  ></i>
                            </div>
                        </div>
                        <h4 class="text-white">Pembelian</h4>
                        <div class="mt-auto">
                            <h6 class="text-left text-white mb-0">
                                <?php echo number_format($count_trans_in, 0, ',','.') ?> Transaksi
                            </h6>
                        </div>
                        <?php if($this->session->userdata("user_level") == "admin"): ?>
                            <a href="<?php echo base_url() ?>index.php/c_income" class="ml-auto mt-3 btn primary-title" style="background-color: white">Detail</a>
                        <?php else: ?>
                            <a href="<?php echo base_url() ?>index.php/c_income/input" class="ml-auto mt-3 btn primary-title" style="background-color: white">Tambah Data</a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="px-2 col-4">
                    <div class="card border-0 px-4 py-3" style="height: 180px; background-color: #5756B3">
                        <div class="mt-n5 mb-3">
                            <div class="card border-0 p-2" style="width: 55px; height: auto">
                                <i class='bx bx-coin-stack bx-md text-center' style='color:#5756b3'  ></i>
                            </div>
                        </div>
                        <h4 class="text-white">Penjualan</h4>
                        <div class="mt-auto">
                            <h6 class="text-left text-white mb-0">
                                <?php echo number_format($count_trans_out, 0, ',','.') ?> Transaksi
                            </h6>
                        </div>
                        <?php if($this->session->userdata("user_level") == "admin"): ?>
                            <a href="<?php echo base_url() ?>index.php/c_outcome" class="ml-auto mt-3 btn primary-title" style="background-color: white">Detail</a>
                        <?php else: ?>
                            <a href="<?php echo base_url() ?>index.php/c_outcome/input" class="ml-auto mt-3 btn primary-title" style="background-color: white">Tambah Data</a>
                        <?php endif; ?>
                        
                    </div>
                </div>
                <div class="px-2 col-4">
                    <div class="card border-0 px-4 py-3" style="height: 180px; background-color: #5756B3">
                        <div class="mt-n5 mb-3">
                            <div class="card border-0 p-2" style="width: 55px; height: auto">
                                <i class='bx bx-wallet-alt bx-md text-center' style='color:#5756b3'  ></i>
                            </div>
                        </div>
                        <h4 class="text-white">Material Belum Terkirim</h4>
                        <div class="mt-auto">
                            <h6 class="text-left text-white mb-0">
                                <?php echo number_format($count_trans_not_sent,0,',','.') ?>
                            </h6>
                        </div>
                        <a  href="<?php echo base_url('index.php/c_laporan_mini') ?>" class="ml-auto mt-2 btn primary-title" style="background-color: white">
                            Detail
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 d-inline-flex mt-3">
                <div class="col-8 px-2">
                    <div class="card border-0 px-3 py-4" style="min-height: 300px">
                        <h5>Total Penjualan Bulanan</h5>
                        <div class="col-12 mb-3 mt-3" style="height: 100%">
                            <canvas id="myChart">

                            </canvas>
                        </div>
                    </div>
                </div>
                <div class="col-4 px-2">
                    <div class="card border-0 px-3 py-4 d-none">
                        <h5>Pengiriman Untuk Hari Ini</h5>
                        <div class="px-0 col-12 mt-3">
                            <a href="<?php echo base_url('index.php/c_laporan_mini') ?>" class="col-12 btn c-color-primary text-white">
                                Cek Daftar
                            </a>
                        </div>
                    </div>
                    <div class="card border-0 px-3 py-4">
                        <h5>Transaksi Bulan Ini</h5>
                        <div class="col-12 mb-3 mt-3" style="height: 100%">
                            <canvas id="chartTrans">

                            </canvas>
                        </div>
                        <div class="d-inline-flex mt-3">
                            <div style="float: left;width: 20px;height: 10px;margin: 5px;border: 1px solid rgba(0, 0, 0, .2);background: #5756B3;"></div>
                            <h6>T. Penjualan: </h6>
                            <h6 class="ml-2">Rp <?php echo number_format($keluar[0]->keluar, 0, ',','.') ?></h6>
                        </div>
                        <div class="d-inline-flex">
                            <div style="float: left;width: 20px;height: 10px;margin: 5px;border: 1px solid rgba(0, 0, 0, .2);background: #ffbb00;"></div>
                            <h6>T. Pembelian: </h6>
                            <h6 class="ml-2">Rp <?php echo number_format($masuk[0]->masuk, 0, ',','.') ?></h6>
                        </div>
                    </div>
                    <?php if($this->session->userdata("user_level") == "admin1"): ?>
                    <div class="px-0 col-12 mt-3">
                        <a href="<?php echo base_url('index.php/c_laporan_keuangan') ?>" class="col-12 btn c-color-primary text-white">Lihat Laporan</a>
                    </div>
                    <?php endif; ?>
                    <br>
                    <div class="card border-0 px-3 py-4">
                        <h5>Lihat Laporan Kecil</h5>
                        <div class="px-0 col-12 mt-3">
                            <a href="<?php echo base_url('index.php/c_laporan_mini/bank_transfer') ?>" class="col-12 btn c-color-primary text-white">Laporan rekening</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = "";
        
        setChart();      
        dataTable();
        convert();

        function dataTable(){
            table = $('#table-tipis').DataTable({
                stateSave: true,
                "lengthChange": false,
                "bInfo" : false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });

        }

        //convert object to array
        function convert() {
            moment.locale('id'); 
            let laba = <?php echo $pendapatan ?>;
            let profit = [];
            let waktu = [];

            for (let i = 0; i < laba.length; i++) {
                const context = laba[i];
                let bulan = moment().month(context.bulan - 1).format('MMMM');
                
                profit[i] = context.terbayar;
                waktu[i] = bulan+" "+ context.tahun;
            }
            setLine(profit, waktu)
        }

        function setChart() {
            let ctx = $("#chartTrans");
            let transOut = '<?php echo number_format($keluar[0]->keluar,0,',','.') ?>';
            let transIn = '<?php echo number_format($masuk[0]->masuk,0,',','.') ?>';
            let chart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: ['Pembelian', 'Penjualan'],
                    datasets: [{
                        label: 'Pendapatan',
                        data: [transIn, transOut],
                        backgroundColor: [
                            '#ffbb00',
                            '#5756B3'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    legend: {
                        display: false
                    }
                }
            });
        }

        function setLine(profit, waktu) {
            var line = document.getElementById('myChart').getContext('2d');
            var myChart = new Chart(line, {
                type: 'line',
                data: {
                    labels: waktu,
                    datasets: [{
                        label: 'Pendapatan',
                        data: profit,
                        backgroundColor: [
                            '#5756b3b0'
                        ],
                        borderColor: [
                            '#5756B3'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    legend: {
                        display: false
                    }
                }
            });
        }
    });
</script>
