<div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-5">
            <input autocomplete="off" class="search-fill col-6 border-0" type="search" name="search" id="search" placeholder="Cari pengeluaran">
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>
        
    <div class="d-inline-flex col-12 p-0">
        <button class="c-text-2 my-auto btn-add c-color-primary text-white medium-weight" data-toggle="modal" data-target="#prevAdd">
            Tambah Pengeluaran
        </button>
        <div class="d-flex my-auto ml-auto">
            <button class="btn-filter c-color-primary" id="filter">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
            </button>
        </div>
    </div>

    <div class="mt-4 custom-card p-3">
        <table width="100%" id="pengeluaran-table">
            <thead>
                <tr class="t-header border-0 col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight">No</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight">Jenis Pengeluaran</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Jumlah Pengeluaran</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Utilitas</td>
                </tr>
            </thead>
            <tbody id="show-in-table">
                <?php
                    $i = 1;
                    foreach ($pengeluaran as $item ) {
                ?>
                    <tr>
                        <td class="p-3 c-text-2"><?php echo $i++ .'.' ?></td>
                        <td class="p-3 c-text-2"><?php echo $item->pengeluaran_nama ?></td>
                        <td class="p-3 c-text-2"><?php echo "Rp ".number_format($item->pengeluaran_amount ,0,".",".")?></td>
                        <td>
                            <button class="ml-2 basic-btn c-color-primary" onclick="setEdit('<?php echo $item->pengeluaran_id ?>','<?php echo $item->pengeluaran_nama ?>','<?php echo $item->pengeluaran_amount ?>')">
                                <i class="bx bxs-pencil text-white" style="margin-top: 5px"></i>
                            </button>
                            <button class="ml-2 basic-btn-1 c-soft-background" style="border : solid 1px #5756B3 !important" onclick="delete_item('<?php echo $item->pengeluaran_id?>','<?php echo $item->pengeluaran_nama ?>')">
                                <i class="bx bxs-trash primary-title" style="margin-top: 5px;"></i>
                            </button>
                        </td>
                    </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="prevAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Pengeluaran Baru</p>
            </div>
            <form action="<?php echo base_url('index.php/c_pengeluaran/input') ?>" method="post">
                <div class="modal-body c-main-background">
                <input id="id" type="text" name="id" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r d-none">
                    <div class="col-12 p-0 mt-3 ">
                        <p class="c-text-3 soft-title regular-weight">Nama Pengeluaran</p>
                        <input id="prvName" type="text" name="addName" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                    </div>
                    <div class="col-12 p-0 mt-3 ">
                        <p class="c-text-3 soft-title regular-weight">Jumlah Pengeluaran</p>
                        <input id="prvAmount" type="number" name="addAmount" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r" value="0">
                    </div>
                </div>
                <div class="modal-footer c-main-background border-0">
                    <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Batal</button>
                    <button id="addMember" type="submit" class="btn-modal-positive medium-weight c-text-2">Tambah Pengeluaran</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="prevEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <p class="primary-title c-text-3 boldest-weight modal-title">Edit Pengeluaran</p>
        </div>
        <form id="formEdit" method="post">
            <div class="modal-body c-main-background">
            <!-- <input id="editId" type="text" name="id" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r d-none"> -->
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Nama Pengeluaran</p>
                    <input id="editName" type="text" name="editName" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Jumlah Pengeluaran</p>
                    <input id="editAmount" type="number" name="editAmount" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Batal</button>
                <button id="editPengeluaran" type="submit" class="btn-modal-positive medium-weight c-text-2">Tambah Pengeluaran</button>
            </div>
        </form>
    </div>
</div>


<script>
    $(document).ready(function () {
        var table="";

        $("#search").on("input", function () {
            search(1, this.value);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#pengeluaran-table').DataTable()
                .search('').columns()
                .search('').draw();
            $("#search").val("");
        });

        dataTable();

        function dataTable(){
            table = $('#pengeluaran-table').DataTable({
                stateSave: true,
                "lengthChange": false,
                "ordering": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

    });
    function setEdit(id, name, amount) {
        var url = '<?php echo base_url('index.php/c_pengeluaran/edit/') ?>' + id;
        $("#editId").val(id);
        $("#editName").val(name);
        $("#editAmount").val(amount);
        $("#formEdit").attr("action", url);
        $("#prevEdit").modal("show");
    }

    function delete_item(id, name){
        var getStatus = confirm("Hapus "+name+" ?");
        if (getStatus == true) {
            location.href = "<?php echo base_url('index.php/c_pengeluaran/delete/') ?>"+id;
        }
    }
</script>