    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-5">
            <input autocomplete="off" class="search-fill col-6 border-0" type="search" name="search" id="search" placeholder="Cari nama material">
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>
        
    <div class="d-inline-flex col-12 p-0">
        <a href="<?php echo base_url() ?>index.php/c_material/input">
            <button class="c-text-2 my-auto btn-add c-color-primary text-white medium-weight">
                Add New Material
            </button>
        </a>
        <div class="d-flex my-auto ml-auto">
            <div class="d-flex my-auto ml-3">
                <button class="btn-filter c-color-primary" id="filter">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                </button>
            </div>
        </div>
    </div>

    <div class="mt-4 custom-card p-3">
        <table width="100%" id="material-table">
            <thead>
                <tr class="t-header border-0 col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight">No</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Nama Material</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Stok</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >H. Beli</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >H. Ecer</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >H. Grosir</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >H. Reseller</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Utilitas</td>
                </tr>
            </thead>
            <tbody id="show-in-table">
                <?php
                    $i = 1;
                    foreach ($result as $item ) {
                ?>
                    <tr>
                        <td class="p-3 c-text-2"><?php echo $i++ .'.' ?></td>
                        <td class="p-3 c-text-2"><?php echo $item->material_name ?></td>

                        <?php if($item->material_stock > $item->material_min_stock && $item->material_stock > 0): ?>
                            <td class="p-3 c-text-2"><?php echo number_format($item->material_stock ,0,".",".") ?></td>
                        <?php elseif($item->material_stock <= $item->material_min_stock && $item->material_stock > 0): ?>
                            <td class="p-3 c-text-2"><?php echo number_format($item->material_stock ,0,".",".") ?> (LOW)</td>
                        <?php else: ?>
                            <td class="p-3 c-text-2"><span class="text-danger">Kosong</span></td>
                        <?php endif;  ?>

                        <td class="p-3 c-text-2"><?php echo "Rp. ".number_format($item->material_harga_beli ,0,".",".") ?></td>
                        <td class="p-3 c-text-2"><?php echo "Rp. ".number_format($item->material_harga_agen ,0,".",".") ?></td>
                        <td class="p-3 c-text-2"><?php echo "Rp. ".number_format($item->material_harga_grosir ,0,".",".") ?></td>
                        <td class="p-3 c-text-2"><?php echo "Rp. ".number_format($item->material_harga_reseller ,0,".",".") ?></td>
                        <td>
                            <button class="ml-2 basic-btn c-color-primary" onclick="edit_stock('<?php echo $item->material_id ?>',<?php echo $item->material_stock?>,<?php echo $item->material_sat_ratio?>)">
                                <i class="bx bx-add-to-queue text-white" style="margin-top: 5px"></i>
                            </button>
                            <a href="<?php echo base_url('index.php/c_material/edit/').$item->material_id ?>" class="" >
                                <button class="ml-2 basic-btn c-soft-primary">
                                    <i class="bx bxs-pencil primary-title" style="margin-top: 5px"></i>
                                </button>
                            </a>
                            <button onclick="delete_item('<?php echo $item->material_name ?>','<?php echo $item->material_id ?>')" class="ml-2 basic-btn-1 c-soft-background" style="border:solid 1px #5756B3 !important">
                                <i class="bx bxs-trash primary-title" style="margin-top: 5px;"></i>
                            </button>
                        </td>
                    </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="prevMat" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Preview Material</p>
                </div>
                <div class="modal-body c-main-background">
                    <div class="col-12 p-0 mt-3 ">
                            <p class="c-text-3 soft-title regular-weight">ID Material</p>
                            <input disabled id="prvId" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                        </div>
                    <div class="col-12 p-0 mt-3 ">
                        <p class="c-text-3 soft-title regular-weight">Nama Material</p>
                        <input disabled id="prvName" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                    </div>
                    <div class="col-12 p-0 mt-3 ">
                        <p class="c-text-3 soft-title regular-weight">Stok Material</p>
                        <input disabled id="prvStock" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                    </div>
                    <div class="col-12 p-0 mt-3 ">
                        <p class="c-text-3 soft-title regular-weight">Harga Beli</p>
                        <input disabled id="prvPrice" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                    </div>
                    <div class="col-12 p-0 mt-3 ">
                        <p class="c-text-3 soft-title regular-weight">Harga Reguler</p>
                        <input disabled id="prvPriceReg" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                    </div>
                    <div class="col-12 p-0 mt-3 ">
                        <p class="c-text-3 soft-title regular-weight">Harga Eceran</p>
                        <input disabled id="prvPriceEce" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                    </div>
                    <div class="col-12 p-0 mt-3 ">
                        <p class="c-text-3 soft-title regular-weight">Harga Distributor</p>
                        <input disabled id="prvPriceDis" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                    </div>
                </div>
                <div class="modal-footer c-main-background border-0">
                    <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete-Material" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Hapus Material </p>
            </div>
            <div class="modal-body c-main-background">
                <a>Apakah anda ingin menghapus data material <span id="Material-delete-name"></span></a>
                <input id="id-delete" type="hidden">
                <hr>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight" data-dismiss="modal">Batal</button>
                <button id="btn-delete" class="btn-modal-positive medium-weight">Hapus</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="prevStock" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form id="formPoin" method="post" action="<?php echo base_url('index.php/c_material/update_stock') ?>">
                    <div class="modal-header">
                        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Akses Cepat</p>
                    </div>
                    <div class="modal-body c-main-background">
                    <input id="matId" type="text" name="newId" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r d-none">
                        <div class="col-12 p-0 ">
                            <p class="c-text-3 soft-title regular-weight">Stok Material</p>
                            <input id="matAmount" name="newAmount" autocomplete="false" type="number" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r" value="0">
                        </div>
                        <hr>
                    </div>
                    <div class="modal-footer c-main-background border-0">
                        <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Batal</button>
                        <button id="addMember" type="submit" class="btn-modal-positive medium-weight c-text-2">Konfirmasi</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){

        var table = "";
        //js select2 dropdown
        $('#type').select2();
        $("#type").on("change", function () {
            search(2, this.value);
        });
        $("#search").on("input", function () {
            search(1, this.value);
        });
        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#material-table').DataTable()
                .search('').columns()
                .search('').draw();
            $("#type").val("");
            $("#search").val("");
        });

        //show_data();
        dataTable();

        function dataTable(){
            table = $('#material-table').DataTable({
                stateSave: true,
                "lengthChange": false,
                "ordering": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });

        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

    });

    function show_preview(id, name, price) {
        $("#prvId").val(id);
        $("#prvName").val(name);
        $("#prvPriceReg").val(price);
        $("#prevMat").modal("show");
    }

    function edit_stock(id,stok,ratio) {
        $("#matId").val(id);
        $("#matAmount").val(stok);
        $("#matRatio").val(ratio);
        $("#prevStock").modal("show");
    }

    function delete_Material(id, name){
        $('#id-delete').val(id);
        $('#Material-delete-name').html(name);
        $('#delete-Material').modal("show");
    }
    function delete_item(name, id){
        var getStatus = confirm("Hapus material "+name+" ?");
        if (getStatus == true) {
            location.href = "<?php echo base_url('index.php/c_material/delete/') ?>"+id;
        }
    }

</script>