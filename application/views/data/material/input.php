    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4">Tambah Material</p>
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <div class="col-12 d-inline-flex p-0">
            <form action="<?php echo base_url('index.php/c_material/input') ?>" autocomplete="off" method="post" class="col-12 p-0">
                <div class="flex-column col-12 main-padding-l pr-0">
                    <?php echo validation_errors(); ?>
                    <input type="text" name="id" class="d-none col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="ID..." id="id">
                    <input type="text" name="satuanAmount" class="d-none col-12 c-text-2 search-fill main-padding-l main-padding-r" id="satuanAmount">
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight">Nama Material</p>
                        <input type="text" name="name" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" autocomplete="false" placeholder="Name..." id="material">
                    </div>
                    <div class="col-12 p-0 mt-3 d-none">
                        <p class="c-text-3 soft-title regular-weight">Satuan</p>
                        <select name="satuan" id="satuan" style="width: 100%" class="dropdown-select2 col-12 c-text-2 search-fill main-padding-l main-padding-r" >
                            <option value="">Pilih Satuan</option>
                            <?php
                                foreach ($satuan as $item ) {
                            ?>
                                <option value="<?php echo $item->ratio_satuan.'|'.$item->satuan_atas.' - '.$item->satuan_bawah ?>"><?php echo $item->satuan_atas.' - '.$item->satuan_bawah ?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <br>
                    <hr class="hr-text" data-content="SATUAN">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-4 mt-4 p-0">
                                <p class="c-text-2 soft-title medium-weight" >Satuan Atas</p>
                                <input type="text" placeholder="Isi dengan satuan umum ...." name="sAtas" class="col-11 c-text-2 search-fill main-padding-l main-padding-r" id="satuanAtas">
                            </div>
                            <div class="col-4 mt-4 p-0">
                                <p class="c-text-2 soft-title medium-weight" >Satuan Bawah</p>
                                <input type="text" placeholder="Isi dengan satuan umum ...." name="sBawah" class="col-11 c-text-2 search-fill main-padding-l main-padding-r" id="satuanBawah">
                            </div>
                            <div class="col-2 mt-4 p-0">
                                <p class="c-text-2 soft-title medium-weight" >Isi</p>
                                <input type="number" value="0" placeholder="Isi satuan ...." name="isiRatio" class="col-11 c-text-2 search-fill main-padding-l main-padding-r" id="satuanRatio">
                            </div>
                            <div class="col-2 mt-4 p-0">
                                <p class="c-text-2 soft-title medium-weight" ></p>
                                <button class="btn-add col-12 text-white c-color-primary c-color-primary mt-4 c-text-2" type="button" data-toggle="modal" data-target="#satuanPil">Pilih Satuan</button>
                            </div>
                        </div>
                    </div>
                    <br>
                    <hr class="hr-text" data-content="UTILITAS">   
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Harga Beli</p>
                        <input type="number" value="0" name="hBeli" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="price">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Harga Ecer</p>
                        <input type="number" value="0" name="hEcer" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="priceReg">
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-8 mt-4 p-0">
                                <p class="c-text-2 soft-title medium-weight" >Harga Grosir</p>
                                <input type="number" value="0" name="hGrosir" class="col-11 c-text-2 search-fill main-padding-l main-padding-r" id="priceDist">
                            </div>
                            <div class="col-4 mt-4 p-0">
                                <p class="c-text-2 soft-title medium-weight" >Quantity Grosir</p>
                                <input type="number" value="0" name="minTrans" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="minTrans">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Harga Reseller</p>
                        <input type="number" value="0" name="hReseller" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="priceEcer">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Stock Minimal <b>* untuk pengingat saat jumlah stock mulai menipis</b></p>
                        <input type="number" value="0" name="minStock" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="minStock">
                    </div>
                    <button class="btn-add col-12 text-white c-color-primary c-color-primary mt-4 c-text-2" type="submit">Tambah Material</button>
                </div>
            </form>
        </div>
</div>

<div class="modal fade" id="satuanPil" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Pilih Data Satuan Material dari Master</p>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <div class="mt-4 custom-card p-3">
                <table id="addNewTable" class="col-12 p-2" width="100%">
                    <thead class="t-header primary-title">
                        <tr>
                            <th class="p-3 c-text-2 boldest-weight text-center">No.</th>
                            <th class="p-3 c-text-2 boldest-weight text-center">Nama</th>
                            <th class="p-3 c-text-2 boldest-weight text-center">Satuan atas</th>
                            <th class="p-3 c-text-2 boldest-weight text-center">Satuan bawah</th>
                            <th class="p-3 c-text-2 boldest-weight text-center">Utilitas</th>
                        </tr>
                    </thead>
                    <tbody id="newMaterialBody">
                        <?php
                            $i=1;
                            foreach ($pale as $item ) {
                        ?>
                            <tr>
                                <td class="p-3 c-text-2 text-center"><?php echo $i++ .'.' ?></td>
                                <td class="p-3 c-text-2 text-center"><?php echo $item->satuan_atas?> - <?php echo $item->satuan_bawah?></td>
                                <td class="p-3 c-text-2 text-center"><?php echo $item->satuan_atas?></td>
                                <td class="p-3 c-text-2 text-center"><?php echo $item->satuan_bawah?></td>
                                <td class="mx-auto">
                                    <button class=" basic-btn c-color-primary" 
                                     onclick="set_satuan('<?php echo $item->satuan_atas?>','<?php echo $item->satuan_bawah?>')">
                                        <i class="bx bx-check text-white" style="margin-top: 5px"></i>
                                    </button>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function () {

        var table="";

        function dataTable(){
            table = $('#addNewTable').DataTable({
                "lengthChange": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    },
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    }
                ]
            });
        }

        $("#brand").select2();
        $("#satuan").select2();
        set_dropdown();

        $("#satuan").change(function (e) { 
            e.preventDefault();
            var getValue = $("#satuan option:selected").val();
            $("#satuanAmount").val(getValue);
        });

        //TODO: input brand masih kurang field di api
        $("#btnSubmit").click(function (e) { 
            e.preventDefault();
            var getMaterial = $("#material").val();
            var getID = $("#id").val();
            var getBrand = $("#brand").val();
            var getPrice = parseInt($("#price").val());
            var getInfo = $("#info").val();

            request = $.ajax({
                        url: 'http://153.92.4.88:8080/material',
                        type: 'post',
                        data: {
                            material_name: getMaterial,
                            material_merek_name: getBrand,
                            material_price: getPrice
                        }
            });

          request.done(function(response) {
              window.location.href = "<?php echo base_url() ?>index.php/c_material";
          });
          request.fail(function(response) {
              var success = response.success;
              var message = response.message;
              var data = response.data;
              console.log(success);
              console.log(message);
              console.log(data);
          });

        });
        
        function set_dropdown() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/merek",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    for(i=0; i<obj.data.length; i++){
                        payload += '<option value="'+obj.data[i].merek_name+'">'+obj.data[i].merek_name+'</option>';
                        //$("#matName").append(payload);
                        $("#brand").html(payload);
                    }
                }
            });
        }
    });
        
    function set_satuan(atas, bawah) {
        
        $("#satuanAtas").val(atas);
        $("#satuanBawah").val(bawah);
        
        $("#satuanPil").modal('hide');
    }
</script>