    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-5">
            <input autocomplete="off" class="search-fill col-6 border-0" type="search" name="search" id="search" placeholder="Cari nama satuan">
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>
        
    <div class="d-inline-flex col-12 p-0">
        <button class="c-text-2 my-auto btn-add c-color-primary text-white medium-weight" data-toggle="modal" data-target="#prevAdd">
            Tambah Satuan
        </button>
        <div class="d-flex my-auto ml-auto">
            <button class="btn-filter c-color-primary" id="filter">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
            </button>
        </div>
    </div>
    <?php echo validation_errors(); ?>
    <div class="mt-4 custom-card p-3">
        <table width="100%" id="satuan-table">
            <thead>
                <tr class="t-header border-0 col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight">No</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight">Nama Satuan</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight">Satuan Atas</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight">Satuan Bawah</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Utilitas</td>
                </tr>
            </thead>
            <tbody id="show-in-table">
                <?php
                    $i=1;
                    foreach ($result as $item ) {
                ?>
                    <tr>
                        <td class="p-3 c-text-2"><?php echo $i++ .'.' ?></td>
                        <td class="p-3 c-text-2"><?php echo $item->satuan_atas?> - <?php echo $item->satuan_bawah?></td>
                        <td class="p-3 c-text-2"><?php echo $item->satuan_atas?></td>
                        <td class="p-3 c-text-2"><?php echo $item->satuan_bawah?></td>
                        <td>
                            <a href="#">
                                <button class="ml-2 basic-btn c-color-primary" onclick="editSat('<?php echo $item->satuan_id ?>','<?php echo $item->satuan_atas ?>','<?php echo $item->satuan_bawah?>')">
                                    <i class="bx bxs-pencil text-white" style="margin-top: 5px"></i>
                                </button>
                            </a >
                            <a href="#">
                                <button class="ml-2 basic-btn-1 c-soft-background" style="border: solid 1px #5756B3 !important" onclick="delete_item('<?php echo $item->satuan_atas ?> - <?php echo $item->satuan_bawah?>','<?php echo $item->satuan_id ?>')">
                                    <i class="bx bxs-trash primary-title" style="margin-top: 5px"></i>
                                </button>
                            </a >
                        </td>
                    </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="prevAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Satuan Material Baru</p>
        </div>
            <form action="<?php echo base_url('index.php/c_satuan/input') ?>" method="post">
                <div class="modal-body c-main-background">
                <?php echo validation_errors(); ?>
                    <div class="col-12 p-0 mt-3 ">
                        <p class="c-text-3 soft-title regular-weight">Satuan Atas</p>
                        <input id="crtAtas" name="namaAtas" type="text" autocomplete="false" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                    </div>
                    <div class="col-12 p-0 mt-3 ">
                        <p class="c-text-3 soft-title regular-weight">Satuan Bawah</p>
                        <input id="crtBawah" name="namaBawah" type="text" autocomplete="false" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                    </div>
                </div>
                <div class="modal-footer c-main-background border-0">
                    <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Batal</button>
                    <button id="addMember" type="submit" class="btn-modal-positive medium-weight c-text-2">Tambah Satuan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="prevEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Edit Satuan Material</p>
            </div>
            <div class="modal-body c-main-background">
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Satuan Atas</p>
                    <input type="hidden" id="editId">
                    <input id="editSatuan" type="text" name="nama-satuan" autocomplete="false" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Satuan Bawah</p>
                    <input type="hidden" id="editId">
                    <input id="editSatuan2" type="text" name="nama-satuan2" autocomplete="false" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Batal</button>
                <button id="addMember" type="button" onclick="submitEdit()" class="btn-modal-positive medium-weight c-text-2">Edit Satuan</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="prevDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Delete Satuan Material</p>
        </div>
        <div class="modal-body c-main-background text-center">
            <br>
            <b>Apakah anda ingin menghapus data satuan ini</b>
            <input type="hidden" id="deleteId">
            <br>
        </div>
        <div class="modal-footer c-main-background border-0">
            <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Batal</button>
            <button type="button" onclick="submitDelete()" class="btn-modal-positive medium-weight c-text-2">Hapus</button>
        </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var table = "";
        //js select2 dropdown
        $('.dropdown-select2').select2();

         $("#search").on("input", function () {
            search(1, this.value);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#supplier-table').DataTable()
                .search('').columns()
                .search('').draw();
            $("#search").val("");
        });

        dataTable();
        
        function dataTable(){
            table = $('#satuan-table').DataTable({
                stateSave: true,
                "lengthChange": false,
                "ordering": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }
    });

    function editSat(id, nama, nama2){
        $('#editId').val(id);
        $('#editSatuan').val(nama);
        $('#editSatuan2').val(nama2);

        $('#prevEdit').modal('show');
    }

    function delete_item(atas, id){
        var getStatus = confirm("Hapus Satuan "+atas+" ?");
        if (getStatus == true) {
            location.href = "<?php echo base_url('index.php/c_satuan/delete/') ?>"+id;
        }
    }

    function deleteSat(id){
        $('#deleteId').val(id);

        $('#prevDelete').modal('show');
    }

    function submitEdit() {
        var id = $('#editId').val();
        var satuan_atas = $('#editSatuan').val();
        var satuan_bawah = $('#editSatuan2').val();
        $.ajax({
            type : "POST",
            url  : "<?php echo base_url('index.php/c_satuan/edit')?>",
            dataType : "JSON",
            data : {id:id, satuan_atas:satuan_atas, satuan_bawah:satuan_bawah},
            success: function(data){
                window.location.href = "<?php echo base_url('index.php/c_satuan')?>";
            }
        });
        return false;
    }

    function submitDelete() {
        var id = $('#deleteId').val();
        $.ajax({
            type : "POST",
            url  : "<?php echo base_url('index.php/c_satuan/delete')?>",
            dataType : "JSON",
            data : {id:id},
            success: function(data){
                window.location.href = "<?php echo base_url('index.php/c_satuan')?>";
            }
        });
        return false;
    }
</script>