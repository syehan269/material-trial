    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4">Tambah Pelanggan</p>
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <div class="col-12 d-inline-flex p-0">
          <form class="flex-column col-12 main-padding-l pr-0" action="<?php echo base_url('index.php/c_customer/input') ?>" method="post">
          <?= validation_errors(); ?>   
                <input id="id" name="id" class="col-12 c-text-2 search-fill main-padding-l main-padding-r d-none" placeholder="ID...">
                <div class="col-12 p-0 mt-4">
                    <p class="c-text-2 soft-title medium-weight">Nama Pelanggan</p>
                    <input id="name" name="name" class="col-12 c-text-2 search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-4">
                    <p class="c-text-2 soft-title medium-weight">Tipe Pelanggan </p>
                    <select name="type" id="type" style="width: 100%" class="p-2 dropdown-select2 c-text-2 search-fill" >
                        <option value="">Select Customer</option>
                        <option value="Retail">Retail</option>
                        <option value="Reseller">Reseller</option>
                    </select>
                    
                </div>
                <div class="col-12 p-0 mt-4">
                    <p class="c-text-2 soft-title medium-weight">Saldo Pelanggan</p>
                    <input id="name" name="saldo" class="col-12 c-text-2 search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">No. Telp</p>
                    <input id="telephone" name="telephone" class="col-12 c-text-2 search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Address</p>
                    <input id="address" name="alamat" class="col-12 c-text-2 search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Informasi Tambahan</p>
                    <textarea id="info" name="info" class="search-fill c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b"rows="4" style="width: 100%; min-height: 150px;"></textarea>
                </div>
                <button type="submit" id="btnAdd" class="btn-add col-12 text-white c-color-primary c-color-primary mt-4 c-text-2">Tambah Pelanggan</button>
          </form>
        </div>
</div>
<script>

    $(document).ready(function () {

        $("#discount").select2();
        set_dropdown();

        $('#btnAdd').on('click',function(){
            var name  = $('#name').val();
            var type  = $('#type option:selected').val();
            console.log(type);
            var info  = $('#info').val();
            var address = $("#address").val();
            var getRaw = $("#discount option:selected").text();
            var getDis = getRaw.split("%");
            var getDisId = $("#discount option:selected").val();
            var id = $("#id").val();

            if (id.length < 1) {
                alert("Fill field customer id !");
            }
            if (info.length < 1) {
                info = "null";
            }
            if (address.length < 1) {
                address = "null";
            }

            request = $.ajax({
                            url: 'http://153.92.4.88:8080/customer',
                            type: 'post',
                            data: {
                                customer_id: id,
                                customer_name: name,
                                customer_address: address,
                                customer_type: type,
                                customer_discount_id: getDisId,
                                customer_discount_percentage: getDis[0]
                            }
                        });

            request.done(function(response) {
                window.location.href = "<?php echo base_url('index.php/c_customer') ?>";
            });
            request.fail(function(response) {
                var success = response.success;
                var message = response.message;
                var data = response.data;
            });

        });

        function set_dropdown() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/discount",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    for(i=0; i<obj.data.length; i++){
                        payload += '<option value="'+obj.data[i].discount_id+'">'+obj.data[i].discount_percentage+'%</option>';
                        //$("#matName").append(payload);
                        $("#discount").html(payload);
                    }
                }
            });
        }

    });

</script>

