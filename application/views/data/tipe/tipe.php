    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-5">
            <!-- <input class="search-fill col-6 border-0" type="text" name="search" id="search" placeholder="Cari nama tipe"> -->
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>
        
    <div class="d-inline-flex col-12 p-0">
            <button class="c-text-2 my-auto btn-add c-color-primary text-white medium-weight d-none">
                Tambah Tipe
            </button>
        <div class="d-flex my-auto ml-auto">
            <button class="btn-filter c-color-primary" id="filter">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
            </button>
        </div>
    </div>

    <div class="mt-4 custom-card p-3">
        <table width="100%" id="tipe-table">
            <thead>
                <tr class="t-header border-0 col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight">No</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight">Nama Tipe Pelanggan</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Utilitas</td>
                </tr>
            </thead>
            <tbody id="show-in-table">
                <tr>
                    <td class="p-3 c-text-2">1</td>
                    <td class="p-3 c-text-2">Distributor</td>
                    <td>
                        <a href="#">
                            <button class="ml-2 basic-btn c-soft-background" data-toggle="modal" data-target="#prevEdit">
                                <i class="bx bxs-pencil primary-title" style="margin-top: 5px"></i>
                            </button>
                        </a >
                    </td>
                </tr>
            </tbody>
        </table>
    </div>    
</div>
<div class="modal fade" id="prevEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Member Baru</p>
            </div>
            <div class="modal-body c-main-background">
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Nama Tipe Pelanggan</p>
                    <input id="prvPoin" type="number" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Batal</button>
                <button id="addMember" type="button" class="btn-modal-positive medium-weight c-text-2">Edit Tipe Pelanggan</button>
            </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function () {
        dataTable();

        function dataTable(){
            table = $('#tipe-table').DataTable({
                stateSave: true,
                "lengthChange": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

    });
</script>