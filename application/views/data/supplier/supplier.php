    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-5">
            <input autocomplete="off" class="search-fill col-6 border-0" type="search" name="search" id="search" placeholder="Cari nama pemasok">
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>
        
    <div class="d-inline-flex col-12 p-0">
        <a href="<?php echo base_url() ?>index.php/c_supplier/input">
            <button class="c-text-2 my-auto btn-add c-color-primary text-white medium-weight">
                Tambah Pemasok
            </button>
        </a>
        <div class="d-flex my-auto ml-auto">
            <button class="btn-filter c-color-primary" id="filter">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
            </button>
        </div>
    </div>

    <div class="mt-4 custom-card p-3">
        <table width="100%" id="supplier-table">
            <thead>
                <tr class="t-header border-0 col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight">No</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight">Nama Pemasok</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Alamat</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >No.Telp</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Utilitas</td>
                </tr>
            </thead>
            <tbody id="show-in-table">
                <?php
                    $i = 1;
                    foreach($result as $item){
                ?>
                    <tr>
                        <td class="p-3 c-text-2"><?php echo $i++ . "."; ?></td>
                        <td class="p-3 c-text-2"><?php echo $item->supplier_name ?></td>
                        <td class="p-3 c-text-2"><?php echo $item->supplier_address ?></td>
                        <td class="p-3 c-text-2"><?php echo $item->supplier_telphone ?></td>
                        <td>
                            <a href="<?php echo base_url("index.php/c_supplier/edit/") . $item->supplier_id ?>" class="" >
                                <button class="ml-2 basic-btn c-color-primary">
                                    <i class="bx bxs-pencil text-white" style="margin-top: 5px"></i>
                                </button>
                            </a>
                            <button onclick="delete_item('<?php echo $item->supplier_name ?>','<?php echo $item->supplier_id ?>')" class="ml-2 basic-btn-1 c-soft-background" style="border : solid 1px #5756B3 !important">
                                <i class="bx bxs-trash primary-title" style="margin-top: 5px;"></i>
                            </button>
                        </td>
                    </tr>
                <?php 
                    }
                ?>
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="delete-supplier" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Hapus Pemasok</p>
            </div>
            <div class="modal-body c-main-background c-text-primary pb-0">
                <p class="c-text-primary">Apakah anda ingin menghapus data <span id="supplier-delete-name" class="c-text-primary"> sda</span></p>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight" data-dismiss="modal">Batal</button>
                <button id="btn-delete" class="btn-modal-positive medium-weight">Hapus</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="prevSupp" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Detail Pemasok</p>
            </div>
            <div class="modal-body c-main-background">
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Nama Pemasok</p>
                    <input disabled id="prvName" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">No.Telp</p>
                    <input disabled id="prvTele" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 d-none">
                    <p class="c-text-3 soft-title regular-weight">No.Fax</p>
                    <input disabled id="prvFax" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Alamat</p>
                    <input disabled id="prvAddre" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Tutup</button>
            </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){

        var table = "";
        //js select2 dropdown
        $('.dropdown-select2').select2();
        //show_data();
        dataTable();

         $("#search").on("input", function () {
            search(1, this.value);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#supplier-table').DataTable()
                .search('').columns()
                .search('').draw();
            $("#search").val("");
        });

        function dataTable(){
            table = $('#supplier-table').DataTable({
                "lengthChange": false,
                "ordering": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

        $('#btn-delete').on('click',function(){
            
            var id  = $('#id-delete').val();

            request = $.ajax({
                            url: 'http://153.92.4.88:8080/suppliers/'+id,
                            type: 'delete'
                        });

            request.done(function(response) {
                window.location.href = "<?php echo base_url('index.php/c_supplier') ?>";
            });
            request.fail(function(response) {
                var success = response.success;
                var message = response.message;
                var data = response.data;
            });
        });

    });

    function show_preview(name, address, telephone, fax){
         $("#prvName").val(name);
         $("#prvAddre").val(address);
         $("#prvTele").val(telephone);
         $("#prvFax").val(fax);
        
         $('#prevSupp').modal("show");
    }

    function delete_item(name, id){
        var getStatus = confirm("Hapus supplier "+name+" ?");
        if (getStatus == true) {
            location.href = "<?php echo base_url('index.php/c_supplier/delete/') ?>"+id;
        }
    }
</script>