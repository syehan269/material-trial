    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4">Add Pemasok</p>
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <div class="col-12 d-inline-flex p-0">
            <form action="<?php base_url('index.php/c_supplier/input') ?>" method="post" class="col-12 p-0">
                <div class="flex-column col-12 main-padding-l pr-0">
                    <?= validation_errors(); ?>   
                    <div class="col-12 p-0 mt-4">
                        <p class="c-text-2 soft-title medium-weight">Nama Pemasok</p>
                        <input id="name" name="name" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Name...">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight">No.Telp</p>
                        <input id="telp" name="telephone" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Telp.No...">
                    </div>
                    <div class="col-12 mt-4 p-0 d-none">
                        <p class="c-text-2 soft-title medium-weight">Fax.No</p>
                        <input id="fax" name="fax" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Fax.No...">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight">Alamat</p>
                        <textarea id="address" name="alamat"  class="search-fill c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b" rows="4" id="info" style="width: 100%; min-height: 150px;"></textarea>
                    </div>
                    <button type="submit" class="btn-add col-12 text-white c-color-primary c-color-primary mt-4 c-text-2">
                        Tambah Pemasok
                    </button>
                </div>
            </form>
        </div>
    </div>

<script>
    $(document).ready(function(){

        $('#btn_send').on('click',function(){
          var name  = $('#name').val();
          var telp  = $('#telp').val();
          var address  = $('#address').val();
          var fax  = $('#fax').val();

          request = $.ajax({
                        url: 'http://153.92.4.88:8080/suppliers',
                        type: 'post',
                        data: {
                            supplier_name: name,
                            supplier_address: address,
                            supplier_telphone: telp,
                            supplier_fax: fax
                        }
                    });

          request.done(function(response) {
              window.location.href = "<?php echo base_url('index.php/c_supplier') ?>";
          });
          request.fail(function(response) {
              var success = response.success;
              var message = response.message;
              var data = response.data;
          });
        });

    });
</script>

