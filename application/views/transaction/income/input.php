    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4">Tambah Transaksi Masuk</p>
            <div class="dropdown ml-auto">
                <button tabindex="-1" class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>
        <?php echo validation_errors(); ?>
        <form class="col-12 d-inline-flex p-0" action="<?php echo base_url('index.php/c_income/input_all') ?>" method="post">
            <div class="col-6 main-padding">
                <button data-toggle="modal" data-target="#addNewMaterial" type="button" class="boldest-weight btn-outline col-12 c-border-primary primary-title c-main-background c-text-2">
                    Tambah Material
                </button>
                <div class="mt-4 custom-card p-3" style="min-height: 400px !important;">
                    <table class="col-12 p-3" width="100%">
                        <thead class="t-header primary-title">
                            <tr>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">Id</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Material</th>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">Brand </th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Kuantitas</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Harga</th>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">Total</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Utilitas</th>
                            </tr>
                        </thead>
                        <tbody id="tb_material">
                            
                        </tbody>
                    </table>
                </div>
                <div class="d-none">
                    <table style="width:100%">
                        <tr>
                            <th>saldo</th>
                            <th>tanggal</th>
                            <th>deadline</th>
                        </tr>
                        <tbody id="back-cicilan">
                            <tr>
                                <td><input  name="saldo-cicilan" id="saldoBack" type="text" value="0"></td>
                                <td><input  name="tanggal-cicilan" id="tanggalBack" type="text" ></td>
                                <td><input  name="text-cicilan" id="textBack" type="text" ></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="mt-3 d-none">
                    <input type="text" id="back-amount-material" name="materialAmount" placeholder="material amount">
                    <input type="number" id="back-item-material" name="materialItem" placeholder="material item">
                    <table style="width:100%">
                        <tr>
                            <th>id material</th>
                            <th>material</th>
                            <th>quant</th> 
                            <th>harga</th>
                            <th>total</th>
                        </tr>
                        <tbody id="back-material">
                            
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="flex-column col-6 main-padding-l pr-0">
                <div class="col-12 p-0">
                    <p class="c-text-2 soft-title medium-weight">Nama Supplier</p>
                    <!-- <input class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Name..."> -->
                    <select name="detail_in_supplier_id" id="name" class="dropdown-select2 col-12 c-text-2 search-fill main-padding-l main-padding-r" >
                        <option value="">Select Supplier</option>
                        <?php
                            foreach ($supplier as $item ) {
                        ?>
                        <option value="<?php echo $item->supplier_name ?>"><?php echo $item->supplier_name ?></option>
                        <?php
                            }
                        ?>
                    </select>
                    <input type="hidden" name="detail_in_supplier_name" id="detail_in_supplier_name">
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Tipe Pembayaran</p>
                    <select id="payment" name="payment" class="c-dropdown col-11 c-text-2" >
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="Cash">Cash</option>
                        <option class="dropdown-item" value="Credit">Credit</option>
                    </select>
                    <a tabindex="-1" href="#">
                        <button tabindex="-1" id="btnCicilan" type="button" class="ml-2 my-auto basic-btn c-color-primary" >
                            <i class="bx bxs-pencil bx-xs text-white" style="margin-top: 5px"></i>
                        </button>
                    </a >
                </div>
                <div class="col-12 mt-4 p-0" id="spacePembayaran">
                    
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Total Pembayaran</p>
                    <div class="p-0 d-inline-flex col-12">
                        <input id="amount" name="amount" class="col-11 c-text-2 secondary-field main-padding-l main-padding-r" value="Rp 0" disabled>
                        <button tabindex="-1" id="btnRefresh" type="button" class="ml-2 my-auto basic-btn c-color-primary my-auto" >
                            <i class="bx bx-refresh bx-xs text-white" style="margin-top: 5px"></i>
                        </button>
                    </div>
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Informasi Tambahan</p>
                    <textarea id="info" name="info" class="search-fill c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b col-12" rows="4" style="min-height: 150px;"></textarea>
                </div>
                <button id="btn-add-in1" type="submit" class="btn-add col-12 text-white c-color-primary c-color-primary mt-4 c-text-2">Tambah Transaksi</button>
            </div>
        </form>

</div>

<div class="modal fade" id="addNewMaterial" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Material</p>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <div class="d-inline-flex col-12 px-0">
                <input type="text" id="search-in" class="col-6 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Cari Material">
                <div class="d-flex my-auto p-0 ml-auto">
                    <button class="btn-filter c-color-primary" id="filter">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                    </button>
                </div>
            </div>
            <div class="mt-4 custom-card p-3">
                    <table id="addNewTable" class="col-12 p-2" width="100%">
                        <thead class="t-header primary-title">
                            <tr>
                                <th class="p-3 c-text-2 boldest-weight text-center">No.</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Material</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Kuantitas</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Harga</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Utilitas</th>
                            </tr>
                        </thead>
                        <tbody id="newMaterialBody">
                            <?php 
                                $i = 1;
                                foreach ($material as $item ) {
                            ?>
                                <tr>
                                    <td class="p-3 c-text-2 text-center"><?php echo $i++ .'.' ?></td>
                                    <td class="p-3 c-text-2 text-center"><?php echo $item->material_name ?></td>
                                    <?php if($item->material_stock < $item->material_min_stock): ?>
                                    <td class="p-3 c-text-2 text-credit text-center"><?php echo number_format($item->material_stock,0,'.','.') ?></td>
                                    <?php else:?>
                                    <td class="p-3 c-text-2 text-center"><?php echo number_format($item->material_stock,0,'.','.') ?></td>
                                    <?php endif;?>
                                    <td class="p-3 c-text-2 text-center"><?php echo 'Rp '.number_format($item->material_harga_beli ,0,".",".")  ?></td>
                                    <td class="text-center">
                                        <a href="#" class="" 
                                         onclick="showQuant('<?php echo $item->material_name ?>','<?php echo $item->material_harga_beli ?>','<?php echo $item->material_id ?>','<?php echo $item->material_sat_bawah ?>','<?php echo $item->material_sat_atas ?>','<?php echo $item->material_sat_ratio ?>')">
                                            <button class="ml-2 basic-btn c-color-primary">
                                                <i class="bx bx-message-square-add text-white " style="margin-top: 5px;"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modQuantMaterial" tabindex="-1" role="dialog" aria-labelledby="close" aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Material</p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0 mt-2">
            <p class="c-text-2 soft-title regular-weight">Kuantitas Material</p>
            <input type="number" id="editQuantMaterial" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Kuantitas...">
        </div>
        <div class="col-12 p-0 mt-2">
            <p class="c-text-2 soft-title regular-weight">Material satuan</p>
            <select name="name" id="matSatuan" style="width: 100%" class="dropdown-select2 col-12 c-text-2 search-fill main-padding-l main-padding-r" >
                <option value="">Pilih Satuan</option>
            </select>
        </div>
        <hr>
        <div class="col-12 p-0 mt-2">
            <p class="c-text-2 soft-title regular-weight">Harga Material (opsional)</p>
            <input type="number" id="editPriceOpsi" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight">Isi</p>
            <input type="number" id="editIsiMaterial" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Isi setiap satuan..." readonly>
        </div>
        <div class="col-12 p-0 mt-2  d-none">
            <p class="c-text-2 soft-title regular-weight">Harga Material</p>
            <input type="text" id="editPrice" class="col-12 c-text-2 search-fill main-padding-l main-padding-r">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight ">Id Material</p>
            <input type="text" id="editId" class="col-12  c-text-2 search-fill main-padding-l main-padding-r">
        </div>
        <div class="col-12 p-0 mt-2  d-none">
            <p class="c-text-2 soft-title regular-weight">Nama Material</p>
            <input type="text" id="editName" class="col-12 c-text-2 search-fill main-padding-l main-padding-r">
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button id="show_again" type="button" class="btn-modal-negative c-text-2 mr-3 medium-weight" data-dismiss="modal">Back</button>
        <button id="addMaterial" type="button" class="btn-modal-positive medium-weight c-text-2">Add Material</button>
      </div>
    </div>
  </div>
</div>

<!-- input cicilan  -->
<div class="modal fade" id="addCicilan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Cicilan</p>
            </div>
            
            <div class="modal-body c-main-background">
                <div class=" mt-3 p-0">
                    <p class="c-text-2 soft-title regular-weight">Term of Payment</p>
                    <div class="col-12 p-0 d-inline-flex">
                        <div class="col-9 p-0">
                            <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="deadline-cicilan">
                        </div>
                        <div class="col-3 p-0 ml-2">
                            <select id="waktu-cicilan" name="payment" class="c-dropdown  c-text-2" >
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                                <option class="dropdown-item" value="Hari">Hari</option>
                                <option class="dropdown-item" value="Minggu">Minggu</option>
                                <option class="dropdown-item" value="Bulan">Bulan</option>
                            </select>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="col-12 p-0">
                    <div class="col-12 p-0">
                        <p class="c-text-2 soft-title regular-weight">Pembayaran Sekarang (Opsional)</p>
                        <input type="number" autocomplete="off" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="saldo-cicilan">
                    </div>
                </div>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight" data-dismiss="modal">Batal</button>
                <button type="button" id="submitCicilan1" class="btn-modal-positive medium-weight">Konfirmasi</button>
            </div>
        </div>        
    </div>
</div>

<script>
    $(document).ready(function () {
        var table="";
        $('.dropdown-select2').select2();
        $("#search-in").on("input", function () {
            search(1, this.value);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#addNewTable').DataTable()
                .search('').columns()
                .search('').draw();
            $("#search").val("");
        });

        dataTable();
        //set_filter();        
        //show_data();
        addPembayaran();
        
        function dataTable(){
            table = $('#addNewTable').DataTable({
                "lengthChange": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    },
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

        function setBackCicilan() {
            //moment js set local region indo
            moment.locale('id');

            let getDeadline = $("#deadline-cicilan").val();
            let getWaktu = $("#waktu-cicilan option:selected").val();
            let getSaldo = $("#saldo-cicilan").val();
            let getTanggal = "";

            if (getWaktu == "Hari") {
                getTanggal = moment().add(getDeadline,'days').format('YYYY-MM-DD');
            } else if(getWaktu == "Minggu") {
                getTanggal = moment().add(getDeadline,'weeks').format('YYYY-MM-DD');
            }else{
                getTanggal = moment().add(getDeadline,'months').format('YYYY-MM-DD');
            }

            if (getSaldo.length < 1) {
                getSaldo = 0;
            }

            $("#saldoBack").val(getSaldo);
            $("#tanggalBack").val(getTanggal);
            $("#textBack").val(getDeadline+" "+getWaktu);
            $("#addCicilan").modal("hide");

        }        

        function addPembayaran() {
            var getMethod = $("#payment option:selected").text();
            if (getMethod == "Cash") {
                var payload = '<p class="c-text-2 soft-title medium-weight">Jumlah Uang Pembelian</p>'+
                    '<input id="amountPembayaran" name="paymentPaid" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" type="number">';
                $("#spacePembayaran").html(payload);
            }
        }

        $("#submitCicilan1").click(function (e) { 
            e.preventDefault();
            setBackCicilan();
        });

        $("#show_again").click(function (e) { 
            e.preventDefault();
            showModMate();
        });

        $("#payment").on("change", function () {
            var payment = $(this).find('option:selected').text()
            if (payment === "Cash") {
                $("#btnCicilan").prop("disabled", true);
                var payload = '<p class="c-text-2 soft-title medium-weight">Jumlah Uang Pembelian</p>'+
                    '<input name="paymentPaid" id="amountPembayaran" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" type="number">';
                $("#spacePembayaran").html(payload);
            }else{
                $("#btnCicilan").prop("disabled", false);
                var payload = '';
                $("#spacePembayaran").html(payload);
            }
            addPembayaran();
        });

        $("#name").on("change", function () {
            var name = $(this).find('option:selected').text()
            $("#detail_in_supplier_name").val(name);
        });

        $("#paymentCount1").on("input", function () {
            var getCount = $(this).val();
            var payload = '';
            console.log(getCount);
            for (let i = 0; i < getCount; i++) {
                const element = getCount;

                payload += '<p id="cicilan-number-'+(i+1)+'" class="c-text-2 soft-title mt-3 regular-weight">Deadline Payment '+(i + 1)+'</p>' +
                           '<input type="date" name="cicilan'+(i + 1)+'" id="cicilan'+(i + 1)+'" class="field-cicilan col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">';
                $("#countPay").html(payload);
            }
        });

        $("#submitCicilan").click(function (e) { 
            e.preventDefault();
            cicilanBundle();
        });

        $("#btnCicilan").click(function (e) { 
            e.preventDefault();
            var getQuantity = $("#tb_material tr").length;
            if (getQuantity > 0) {
                var payment1 = $("#payment option:selected").val();
                if (payment1 == "Credit") {
                    $("#addCicilan").modal("show");
                }else{
                    alert('Hanya opsi kredit menampilkan form cicilan');
                }
            }else{
                alert('Anda harus memilih material terlebih dahulu');
            }
        });

        $("#subMaterial").click(function (e) { 
            e.preventDefault();
            var getName = $("#brandName option:selected").text();
            var getQuant = $("#matQuant").val();
            var getRow = $("table tbody tr").length;
            var getID = $("#brandName").val();
            var createID = "comp"+"-"+(getRow+1);

            var payload = '<tr class="t-item" id="'+createID+'">'+
                                '<td class="p-2 c-text-2 text-center" id="'+createID+'-name">'+getName+' <></></td>'+
                                '<td class="p-2 c-text-2 text-center" id="'+createID+'-quant">'+getQuant+'</td>'+
                                '<td class="p-2 c-text-2 text-center ">'+
                                    '<a href="#" onclick="deleteItem(\''+createID+'\')">'+
                                        '<button class="ml-2 basic-btn c-soft-background" >'+
                                            '<i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                '</td>'+
                            '</tr>';
            $("#tb_material").append(payload);
        });

        $("#btnRefresh").click(function (e) { 
            e.preventDefault();
            totalQuant();
            setMatBack();
        });

        $("#addMaterial").click(function (e) { 
            e.preventDefault();
            $("#editQuant").val("");
            addNewMate()
        });

        $("#btn-add-in").click(function (e) { 
            e.preventDefault();
            if ($("#tb_material tr").length < 1) {
                alert("Insert material first !");
            }else{
                sendData();
            }
        });

        $("#matSatuan").change(function (e) { 
            e.preventDefault();
            let tipe = $(this).val();
            if (tipe === "atas") {
                $("#editIsiMaterial").prop('readonly',false);
            } else {
                //$("#editIsiMaterial").val("1");
                $("#editIsiMaterial").prop('readonly',true);
            }
        });

    });

    function setMatBack(){
        var getCount = $("#tb_material tr").length;
        $("#back-item-material").val(getCount);
        var payload = '';

        $("#tb_material").find("tr").each(function (index, element) {    
            var material = $("#add-"+(index+1)+"-name").text();
            var id = $("#add-"+(index+1)+"-id").text();
            var quant = $("#add-"+(index+1)+"-quant").val();
            var price = $("#add-"+(index+1)+"-price").text();
            var total = quant * price;
            payload += '<tr id="back-'+(index+1)+'">'+
                            '<td><input name="idBack'+(index+1)+'" id="idBack'+(index+1)+'" type="text" value="'+id+'"></td>'+
                            '<td><input name="materialBack'+(index+1)+'" id="materialBack'+(index+1)+'" type="text" value="'+material+'"></td>'+
                            '<td><input name="quantBack'+(index+1)+'" id="quantBack'+(index+1)+'" type="text" value="'+quant+'"></td>'+
                            '<td><input name="priceBack'+(index+1)+'" id="priceBack'+(index+1)+'" type="text" value="'+price+'"></td>'+
                            '<td><input name="totalBack'+(index+1)+'" id="totalBack'+(index+1)+'" type="text" value="'+total+'"></td>'+
                        '</tr>';
            
            $("#back-material").html(payload);
        });
    }

    function addNewMate() {

        let getMat = $("#editName").val();
        let getQuant = $("#editQuantMaterial").val();
        let getId = $("#editId").val();
        let getSatuan = $("#matSatuan option:selected").val();
        let ratio = $("#editIsiMaterial").val();
        let getPrice = $("#editPrice").val();
        let priceOpsional = $("#editPriceOpsi").val();
        
        let getRow = $("#tb_material tr").length;
        let createID = "add"+"-"+(getRow+1);

        if (getQuant < 1) {
            getQuant = 1;
        }

        if (priceOpsional.length > 1) {
            getPrice = priceOpsional;
        }

        let totalMaterial = parseInt(getQuant)*parseInt(getSatuan);
        let finalPrice = parseInt(getPrice)*2;

        let finTotal = finalPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        let tod = "#"+createID+"-quant";

         let payload = '<tr id="'+createID+'" class="t-item">'+
                                '<td class="p-2 c-text-2 text-center d-none" id="'+createID+'-id">'+getId+'</td>'+
                                '<td class="p-2 c-text-2 text-center" id="'+createID+'-name">'+getMat+'</td>'+
                                '<td class="p-2 c-text-2 text-center d-none" id="'+createID+'-brand"></td>'+
                                '<td class="p-2 c-text-2 text-center"><input id="'+createID+'-quant" type="text" class="text-center search-fill col-4 quant-total" value="'+totalMaterial+'"></td>'+
                                '<td class="p-2 c-text-2 text-center " id="'+createID+'-price">'+getPrice+'</td>'+
                                '<td class="p-2 c-text-2 text-center d-none" id="'+createID+'-total">'+finTotal+'</td>'+
                                '<td class="p-2 c-text-2 text-center ">'+
                                    '<a href="#" onclick="deleteItem(\''+createID+'\')">'+
                                        '<button class="ml-2 basic-btn c-soft-background" >'+
                                            '<i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                '</td>'+
                            '</tr>';
        $("#tb_material").append(payload);
        $("#editIsiMaterial").val("");
        $("#editPriceOpsi").val("");
        setBackMaterial(getMat, totalMaterial, getPrice, finTotal, getId);
        totalQuant();
    }

    function setBackMaterial(material, quant, price, total, id) {
        var getAmount = $("#tb_material tr").length;
        var payload = '';
        
        for (let o = 0; o < getAmount; o++) {
            payload = '<tr id="back-'+(o+1)+'">'+
                            '<td><input name="idBack'+(o+1)+'" id="idBack'+(o+1)+'" type="text" value="'+id+'"></td>'+
                            '<td><input name="materialBack'+(o+1)+'" id="materialBack'+(o+1)+'" type="text" value="'+material+'"></td>'+
                            '<td><input name="quantBack'+(o+1)+'" id="quantBack'+(o+1)+'" type="text" value="'+quant+'"></td>'+
                            '<td><input name="priceBack'+(o+1)+'" id="priceBack'+(o+1)+'" type="text" value="'+price+'"></td>'+
                            '<td><input name="totalBack'+(o+1)+'" id="totalBack'+(o+1)+'" type="text" value="'+total+'"></td>'+
                        '</tr>';
        }
        $("#back-item-material").val(getAmount);
        $("#back-material").append(payload);
    }

    function editItem(id) {
        $("#editMaterial").modal("show");
        var idQuant = "#"+id+"-quant";
        var idName = "#"+id+"-name";
        var getAmount = $(idQuant).text();
        var getName = $(idName).text();
        console.log(getAmount);
        console.log(getName);
        
        //$("#editQuant").val(getAmount);
        $("#editName").val(getName).change();

        $("#editMerkNew").click(function (e) { 
            e.preventDefault();
            var newQuant = $("#editQuant").val();
            var newName = $("#editName option:selected").text();

            $(idQuant).html(newQuant);
            $(idName).html(newName);
            $("#editMaterial").modal("hide");
            
        });
    }

    function totalQuant() {
        var total = 0;
        $("#tb_material").find("tr").each(function (index, element) {
            // element == this
            var quantId = "#add-"+(index+1)+"-quant";
            var totalId = "#add-"+(index+1)+"-total";
            var priceId = "#add-"+(index+1)+"-price";
            var getQuant = $(quantId).val();
            var getPrice = $(totalId).text();
            var getSingle = $(priceId).text();
            var semi = parseInt(getQuant) * parseInt(getSingle);
            var finTotal = "Rp "+semi.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $(totalId).val(finTotal);
            total += semi;

            // console.log("quant: "+getQuant);
            // console.log("sing: "+getSingle);
            // console.log("total: "+getPrice);
        });

        var gim =  "Rp "+total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        $("#amount").val(gim);
        $("#back-amount-material").val(total);
    }

    function totalPayment() {
        var total = 0;
        $("#tb_material").find("tr").each(function (index, element) {
            // element == this
            var quantId = "#add-"+(index+1)+"-quant";
            var totalId = "#add-"+(index+1)+"-total";
            var priceId = "#add-"+(index+1)+"-price";
            var getQuant = $(quantId).val();
            var getPrice = $(totalId).text();
            var getSingle = $(priceId).text();
            var semi = parseInt(getQuant) * parseInt(getSingle);
            var finTotal = "Rp "+semi.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $(totalId).val(finTotal);
            total += semi;

        });

        return total;
    }

    function showQuant(material, price, matId, bawah, atas, ratio) {
        
        $("#editName").val(material);
        $("#editPrice").val(price);
        $("#editId").val(matId);

        let satuanOps = 
            "<option value='1'>"+bawah+" (satuan bawah)</option>"+
            "<option value='"+ratio+"'>"+atas+" (satuan atas)</option>";
        
        $("#matSatuan").html(satuanOps);
        $("#modQuantMaterial").modal("show");
        $("#addNewMaterial").modal("hide");
    }

    function showModMate() {
        $("#modQuantMaterial").modal("hide");
        $("#addNewMaterial").modal("show");
    }

    function deleteItem(id) {
        var splitId = id.split("-");
        var getBackId = "#back-"+splitId[1];
        var fag = "#"+id;
        $(getBackId).remove();
        $(fag).remove();
        totalQuant();
    }

</script>