    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4">Edit Transaksi Masuk</p>
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <form class="col-12 d-inline-flex p-0" action="<?php echo base_url('index.php/c_income/update_all') ?>" method="post">
            <div class="col-6 main-padding">
                <button id="addMaterial" data-toggle="modal" data-target="#addNewMaterial" type="button" class="boldest-weight btn-outline col-12 c-border-primary primary-title c-main-background c-text-2">
                    Tambah Material
                </button>
                <div class="mt-4 custom-card p-3" style="min-height: 400px !important;">
                    <table class="col-12 p-3" width="100%">
                        <thead class="t-header primary-title">
                            <tr>
                                <th class="p-3 c-text-2 boldest-weight text-center">Material</th>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">Brand </th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Kuantitas</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Harga</th>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">Total</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Utilitas</th>
                            </tr>
                        </thead>
                        <tbody id="tb_material">
                            <?php
                                $i = 1;
                                foreach ($materialBundle as $item ) {
                            ?>
                                <tr id="add-<?php echo $i ?>" class="t-item">
                                    <td class="p-2 c-text-2 text-center" id="add-<?php echo $i ?>-name"><?php echo $item->detail_in_material_name ?></td>
                                    <td class="p-2 c-text-2 text-center d-none" id="add-<?php echo $i ?>-name"><?php echo $item->detail_in_material_name ?></td>
                                    <td class="p-2 c-text-2 text-center"><input id="add-<?php echo $i ?>-quant" class="text-center search-fill col-4" placeholder="Quantity..." value="<?php echo $item->detail_in_material_amount?>"></td>
                                    <td class="p-2 c-text-2 text-center" id="add-<?php echo $i ?>-price"><?php echo $item->detail_in_material_price ?></td>
                                    <td class="p-2 c-text-2 text-center d-none" id="add-<?php echo $i ?>-total"></td>
                                    <td class="p-2 c-text-2 text-center ">
                                        <a href="#" onclick="deleteItem('add-<?php echo $i ?>')">
                                            <button class="ml-2 basic-btn c-soft-background" >
                                                <i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>
                                            </button>
                                        </a >
                                    </td>
                                </tr>
                            <?php
                                $i++;
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div>
                    <input type="text" id="id" name="id-item" placeholder="id" value="<?php echo $id ?>">
                    <input type="text" id="back-amount-cicilan" name="cicilanAmount" placeholder="cicilan amount" value="<?php echo $data[0]->trans_in_utang_amount ?>">
                    <table style="width:100%">
                        <tr>
                            <th>cicilan</th>
                            <th>jumlah</th> 
                            <th>tanggal</th>
                        </tr>
                        <tbody id="back-cicilan">
                            
                        </tbody>
                    </table>
                </div>
                <div class="mt-3">
                    <input type="text" id="back-amount-material" name="materialAmount" placeholder="material amount" value="<?php echo $data[0]->trans_in_payment_amount ?>">
                    <input type="number" id="back-item-material" name="materialItem" placeholder="material item">
                    <table style="width:100%">
                        <tr>
                            <th>material</th>
                            <th>quant</th> 
                            <th>harga</th>
                            <th>total</th>
                        </tr>
                        <tbody id="back-material">
                            
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="flex-column col-6 main-padding-l pr-0">
                <div class="col-12 p-0">
                    <p class="c-text-2 soft-title medium-weight">Supplier Name</p>
                    <select name="name" id="name" class="dropdown-select2 col-12 c-text-2 search-fill main-padding-l main-padding-r" >
                        <option value="">Select Supplier</option>
                        <?php
                            foreach ($supplier as $item ) {
                            if($item->supplier_name == $data[0]->trans_in_supplier):
                        ?>
                        <option selected value="<?php echo $item->supplier_name ?>"><?php echo $item->supplier_name ?></option>
                            <?php else: ?>
                        <option value="<?php echo $item->supplier_name ?>"><?php echo $item->supplier_name ?></option>
                            <?php endif; ?>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Tipe Pembayaran</p>
                    <select id="payment" class="c-dropdown col-11 c-text-2" >
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <?php if($data[0]->trans_in_payment_type == "Cash" ):?>
                            <option selected class="dropdown-item" value="Cash">Cash</option>
                            <option class="dropdown-item" value="Credit">Credit</option>
                        <?php else: ?>
                            <option class="dropdown-item" value="Cash">Cash</option>
                            <option selected class="dropdown-item" value="Credit">Credit</option>
                        <?php endif; ?>
                    </select>
                    <button id="btnCicilan" type="button" class="ml-2 my-auto basic-btn c-color-primary" >
                        <i class="bx bxs-pencil bx-xs text-white" style="margin-top: 5px"></i>
                    </button>
                </div>
                <div class="col-12 mt-4 p-0" id="spacePembayaran">
                    
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Total Pembayaran</p>
                    <div class="p-0 d-inline-flex col-12">
                        <input id="amount" value="<?php echo "Rp ".number_format( $data[0]->trans_in_payment_amount,0,'.','.') ?>" class="col-11 c-text-2 secondary-field main-padding-l main-padding-r" value="Rp 0" disabled>
                        <a href="#" class="my-auto">
                            <button id="btnRefresh" type="button" class="ml-2 my-auto basic-btn c-color-primary" >
                                <i class="bx bx-refresh bx-xs text-white" style="margin-top: 5px"></i>
                            </button>
                        </a >
                    </div>
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Informasi Tambahan</p>
                    <textarea id="info" class="search-fill c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b col-12" rows="4" style="min-height: 150px;">
<?php echo $data[0]->trans_in_additional_info ?>
                    </textarea>
                    <input type="text" id="date" class="d-none col-6 mt-3 c-text-2 search-fill main-padding-l main-padding-r" placeholder="">
                </div>
                <button type="submit" id="btn-edit-in1" class="btn-add col-12 text-white c-color-primary c-color-primary mt-4 c-text-2">Edit Transaksi</button>
            </div>
        </form>
</div>

<div class="modal fade" id="addNewMaterial" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Material</p>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <div class="d-inline-flex col-12 px-0">
                <input type="text" id="search-in" class="col-6 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Cari Material">
                <div class="d-flex my-auto ml-auto">
                    <button class="btn-filter c-color-primary" id="filter">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                    </button>
                </div>
            </div>
            <div class="mt-4 custom-card p-3">
                    <table id="addNewTable" class="col-12 p-2" width="100%">
                        <thead class="t-header primary-title">
                            <tr>
                                <th class="p-3 c-text-2 boldest-weight text-center">No.</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Material</th>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">Kuantitas</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Harga</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Utilitas</th>
                            </tr>
                        </thead>
                        <tbody id="newMaterialBody">
                            <?php 
                                $i = 1;
                                foreach ($material as $item ) {
                            ?>
                                <tr>
                                    <td class="p-3 c-text-2 text-center"><?php echo $i++ .'.' ?></td>
                                    <td id='+setMate+' class="p-3 c-text-2 text-center"><?php echo $item->material_name ?></td>
                                    
                                    <td id='+setQuant+' class="p-3 c-text-2 text-center d-none">1</td>
                                    <td id='+setPrice+' class="p-3 c-text-2 text-center"><?php echo 'Rp '.number_format($item->material_harga_beli ,0,".",".")  ?></td>
                                    <td class="text-center">
                                        <a href="#" class="" onclick="showQuant('<?php echo $item->material_name ?>','<?php echo $item->material_harga_beli ?>','<?php echo $item->material_satuan ?>','<?php echo $item->material_satuan_amount ?>')">
                                            <button class="ml-2 basic-btn c-color-primary">
                                                <i class="bx bx-message-square-add text-white " style="margin-top: 5px;"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Cancel</button>
        <!-- <button id="subMaterial" type="button" class="btn-modal-positive medium-weight c-text-2">Add Material</button> -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modQuantMaterial" tabindex="-1" role="dialog" aria-labelledby="close" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Add Incoming Material</p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0 mt-2">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Material Quantity</p>
            <input type="number" id="editQuant" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Material satuan</p>
            <select name="name" id="matSatuan" style="width: 100%" class="dropdown-select2 col-12 c-text-2 search-fill main-padding-l main-padding-r" >
                <option value="">Select Supplier</option>
            </select>
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Material Price</p>
            <input type="text" id="editPrice" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Material name</p>
            <input type="text" id="editName" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button id="show_again" type="button" class="btn-modal-negative c-text-2 mr-3 medium-weight" data-dismiss="modal">Back</button>
        <button id="addMaterialFin" type="button" class="btn-modal-positive medium-weight c-text-2">Add Material</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addCicilan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Cicilan</p>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <p class="c-text-2 soft-title regular-weight">Jumlah Cicilan</p>
            <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" value="<?php echo $data[0]->trans_in_utang_amount ?>" id="paymentCount">
        </div>
        <hr>
        <div class="col-12 p-0" id="countPay">

        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight" data-dismiss="modal">Batal</button>
        <button type="button" id="submitCicilan1" class="btn-modal-positive medium-weight">Konfirmasi</button>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function () {
        
        var table="";
        $('.dropdown-select2').select2();
        $("#search-in").on("input", function () {
            search(1, this.value);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#addNewTable').DataTable()
                .search('').columns()
                .search('').draw();
            $("#search-in").val("");
        });

        //set_filter();
        set_dropdown_modal();
        set_dropdown_brand();
        //show_data();
        //set_data();
        addPembayaran();
        setMatCount();  
        setCicilanCount();
        setCicilanBack();

        $("#addMaterial").click(function (e) { 
            e.preventDefault();
            console.log(getTable());
        });

        $("#submitCicilan1").click(function (e) { 
            e.preventDefault();
            setBackCicilan();
        });

        $("#payment").on("change", function () {
            var payment = $(this).find('option:selected').text()
            if (payment === "Cash") {
                $("#btnCicilan").prop("disabled", true);
                var payload = '<p class="c-text-2 soft-title medium-weight">Jumlah Uang Pembelian</p>'+
                    '<input name="paymentPaid" value="<?php echo $data[0]->trans_in_paid_amount ?>" id="amountPembayaran" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" type="number">';
                $("#spacePembayaran").html(payload);
            }else{
                $("#btnCicilan").prop("disabled", false);
                var payload = '';
                $("#spacePembayaran").html(payload);
            }
            addPembayaran();
        });

        $("#paymentCount").on("input", function () {
            var getCount = $(this).val();
            var payload = '';
            for (let i = 0; i < getCount; i++) {
                const element = getCount;

                payload += '<p id="cicilan-number-'+(i+1)+'" class="c-text-2 soft-title mt-3 regular-weight">Deadline Payment '+(i + 1)+'</p>' +
                           '<input type="date" id="cicilan'+(i + 1)+'" class="field-cicilan col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">';
                $("#countPay").html(payload);
            }
        });

        $("#submitCicilan").click(function (e) { 
            e.preventDefault();
            console.log(cicilanBundle());
        });

        $("#btnCicilan").click(function (e) { 
            e.preventDefault();
            var getQuantity = $("#tb_material tr").length;
            if (getQuantity > 0) {
                var payment1 = $("#payment option:selected").val();
                if (payment1 == "Credit") {
                    $("#addCicilan").modal("show");
                }else{
                    alert('Hanya opsi kredit menampilkan form cicilan');
                }
            }else{
                alert('Anda harus memilih material terlebih dahulu');
            }
        });

        function setCicilanCount() {
            var getCount = $("#paymentCount").val();
            var payload = '';
            for (let i = 0; i < getCount; i++) {
                const element = getCount;

                payload += '<p id="cicilan-number-'+(i+1)+'" class="c-text-2 soft-title mt-3 regular-weight">Deadline Payment '+(i + 1)+'</p>' +
                           '<input type="date" id="cicilan'+(i + 1)+'" class="field-cicilan col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">';
                $("#countPay").html(payload);
            }
        }

        function setMatCount(){
            var getCount = $("#tb_material tr").length;
            $("#back-item-material").val(getCount);

            $("#tb_material").find("tr").each(function (index, element) {    
                var payload = '';
                var material = $("#add-"+(index+1)+"-name").text();
                var quant = $("#add-"+(index+1)+"-quant").val();
                var price = $("#add-"+(index+1)+"-price").text();
                var total = quant * price;
                payload = '<tr id="back-'+(index+1)+'">'+
                                '<td><input name="materialBack'+(index+1)+'" id="materialBack'+(index+1)+'" type="text" value="'+material+'"></td>'+
                                '<td><input name="quantBack'+(index+1)+'" id="quantBack'+(index+1)+'" type="text" value="'+quant+'"></td>'+
                                '<td><input name="priceBack'+(index+1)+'" id="priceBack'+(index+1)+'" type="text" value="'+price+'"></td>'+
                                '<td><input name="totalBack'+(index+1)+'" id="totalBack'+(index+1)+'" type="text" value="'+total+'"></td>'+
                            '</tr>';
                
                $("#back-material").append(payload);
            });
        }

        function setMatBack(){
            var getCount = $("#tb_material tr").length;
            $("#back-item-material").val(getCount);
            var payload = '';

            $("#tb_material").find("tr").each(function (index, element) {    
                var material = $("#add-"+(index+1)+"-name").text();
                var quant = $("#add-"+(index+1)+"-quant").val();
                var price = $("#add-"+(index+1)+"-price").text();
                var total = quant * price;
                payload += '<tr id="back-'+(index+1)+'">'+
                                '<td><input name="materialBack'+(index+1)+'" id="materialBack'+(index+1)+'" type="text" value="'+material+'"></td>'+
                                '<td><input name="quantBack'+(index+1)+'" id="quantBack'+(index+1)+'" type="text" value="'+quant+'"></td>'+
                                '<td><input name="priceBack'+(index+1)+'" id="priceBack'+(index+1)+'" type="text" value="'+price+'"></td>'+
                                '<td><input name="totalBack'+(index+1)+'" id="totalBack'+(index+1)+'" type="text" value="'+total+'"></td>'+
                            '</tr>';
                
                $("#back-material").html(payload);
            });
        }

        function setCicilanBack() {
            var getAmount = $("#back-amount-cicilan").val();
            
            var payload = '';
            var i = 0
            <?php foreach ($cicilanBundle as $item ) {   ?>
            for (let o = 0; o < 1; o++) {
                var getVal = '<?php echo $item->utang_due_date ?>';
                var getTotal = totalPayment();
                var final = getTotal/getAmount;
                payload += '<tr>'+
                                '<td><input  name="cicilanBack'+(o+1)+'" id="cicilanBack'+(o+1)+'" type="text" value="cicilan-'+(i+1)+'"></td>'+
                                '<td><input  name="finalBack'+(o+1)+'"  id="finalBack'+(o+1)+'" type="text" value="'+final+'"></td>'+
                                '<td><input  name="amountBack'+(o+1)+'" id="amountBack'+(o+1)+'" type="text" value="'+getVal+'"></td>'+
                            '</tr>';
            }
            i++;
            <?php }?>
            $("#back-amount-cicilan").val(getAmount);
            $("#back-cicilan").html(payload);
        }

        function setBackCicilan() {
            var getAmount = $("#paymentCount").val();
            var payload = '';
            
            for (let o = 0; o < getAmount; o++) {
                var getVal = $("#cicilan"+(o+1)).val();
                var getTotal = totalPayment();
                var final = getTotal/getAmount;
                payload += '<tr>'+
                                '<td><input  name="cicilanBack'+(o+1)+'" id="cicilanBack'+(o+1)+'" type="text" value="cicilan-'+(o+1)+'"></td>'+
                                '<td><input  name="finalBack'+(o+1)+'"  id="finalBack'+(o+1)+'" type="text" value="'+final+'"></td>'+
                                '<td><input  name="amountBack'+(o+1)+'" id="amountBack'+(o+1)+'" type="text" value="'+getVal+'"></td>'+
                            '</tr>';
            }
            $("#back-amount-cicilan").val(getAmount);
            $("#back-cicilan").html(payload);
        }


        function dataTable(){
            table = $('#addNewTable').DataTable({
                "lengthChange": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        }

        function addPembayaran() {
            var getMethod = $("#payment option:selected").text();
            if (getMethod == "Cash") {
                var payload = '<p class="c-text-2 soft-title medium-weight">Jumlah Uang Pembelian</p>'+
                    '<input id="amountPembayaran" value="<?php echo $data[0]->trans_in_paid_amount ?>" name="paymentPaid" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" type="number">';
                $("#spacePembayaran").html(payload);
            }
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

        function set_dropdown_brand(){
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/merek",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value='+obj.data[i].merek_name+'>'+obj.data[i].merek_name+'</option>';
                        $("#brandName").html(payload);
                    }
                }
            });
        }

        function set_filter() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/suppliers",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value='+obj.data[i].supplier_name+'>'+obj.data[i].supplier_name+'</option>';
                        $("#name").html(payload);
                    }
                }
            });
        }

        function set_data() {
            var tot = "<?php echo $id ?>";
            console.log(tot);
            $.ajax({
                type: "get",
                url: "http://153.92.4.88:8080/transaction-in/"+tot,
                async: true,
                dataType: "text",
                success: function (response) {  
                    var obj = JSON.parse(response);
                    var getId = obj.data.transaction.trans_in_id;
                    var getAmount = obj.data.transaction.trans_in_payment_amount;
                    var getType = obj.data.transaction.trans_in_payment_type;
                    var getInfo = obj.data.transaction.trans_in_additional_info;

                    var rawDate = obj.data.transaction.trans_in_due_date;
                    var split = rawDate.split("T")
                    //var split1 = split[0].split("-")
                    //var getDate = split1[1]+"/"+split1[2]+"/"+split1[0];
                    var getTime = split[1].split(".");
                    var payload = "";
                    var amount = "Rp "+getAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                    var getRow = $("#tb_material tr").length;
                    console.log(getRow);
                    var createID = "add"+"-"+(getRow+1);

                    if (getInfo == "null" || getInfo == "COK") {
                        getInfo = "";
                    }

                    $("#date").val(split[0]+" "+getTime[0]);
                    $("#info").val(getInfo);
                    $("#amount").val(amount);

                    $("#payment option").filter(function() {
                        return $(this).text() == getType;
                    }).prop('selected', true);

                    $("#name option").filter(function() {
                        //may want to use $.trim in here
                        return $(this).text() == "";
                    }).prop('selected', true);

                    for (let i = 0; i < obj.data.detail.length; i++) {
                        const context = obj.data.detail[i];
                        var getMat = context.detail_in_material_name;
                        var getQuant = context.detail_in_material_amount;
                        var getBrand = context.detail_in_merek_name;
                        var getPrice = context.detail_in_material_price;
                        var createID = "add"+"-"+(i+1);
                        var getTotal = parseInt(getQuant)*parseInt(getPrice);

                        var payload = '<tr id="'+createID+'" class="t-item">'+
                                            '<td class="p-2 c-text-2 text-center" id="'+createID+'-name">'+getMat+'</td>'+
                                            '<td class="p-2 c-text-2 text-center d-none" id="'+createID+'-brand">'+getBrand+'</td>'+
                                            '<td class="p-2 c-text-2 text-center"><input id="'+createID+'-quant" class="text-center search-fill col-4" placeholder="Quantity..." value="'+getQuant+'"></td>'+
                                            '<td class="p-2 c-text-2 text-center d-none" id="'+createID+'-price">'+getPrice+'</td>'+
                                            '<td class="p-2 c-text-2 text-center" id="'+createID+'-total">'+getTotal+'</td>'+
                                            '<td class="p-2 c-text-2 text-center ">'+
                                                '<a href="#" onclick="deleteItem(\''+createID+'\')">'+
                                                    '<button class="ml-2 basic-btn c-soft-background" >'+
                                                        '<i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>'+
                                                    '</button>'+
                                                '</a >'+
                                            '</td>'+
                                        '</tr>';
                        $("#tb_material").append(payload);
                    }

                }
            });
        }

        function set_dropdown_modal() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/material",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value='+obj.data[i].material_name+'>'+obj.data[i].material_name+'</option>';
                        $("#mat_name").html(payload);
                    }
                }
            });
        }

        function show_data(){

            $.ajax({
                type  : 'GET',
                url   : 'http://153.92.4.88:8080/material',
                async : true,
                dataType : 'text',
                success : function(data){
                    var html = '';
                    var i;
                    var text = data;
                    obj = JSON.parse(text);
                    for(i=0; i<obj.data.length; i++){
                        var setId = "new-mat-"+(i+1);
                        var setMate = setId+"-mate";
                        var setBrand = setId+"-brand";
                        var setQuant = setId+"-quant";
                        var setPrice = setId+"-price";
                        
                        html += '<tr>'+
                                    '<td class="p-3 c-text-2 text-center">'+(i + 1)+'</td>'+
                                    '<td id='+setMate+' class="p-3 c-text-2 text-center">'+obj.data[i].material_name+'</td>'+
                                    '<td id='+setQuant+' class="p-3 c-text-2 text-center d-none">1</td>'+
                                    '<td id='+setPrice+' class="p-3 c-text-2 text-center">'+obj.data[i].material_price+'</td>'+
                                    '<td class="text-center">'+
                                        '<a href="#" class="" onclick="showQuant(\''+setMate+'\',\''+setBrand+'\',\''+setPrice+'\')">'+
                                            '<button class="ml-2 basic-btn c-color-primary">'+
                                                '<i class="bx bx-message-square-add text-white " style="margin-top: 5px;"></i>'+
                                            '</button>'+
                                        '</a>'+
                                    '</td>'+
                                '</tr>';                        
                    }
                    $('#newMaterialBody').html(html);
                    dataTable();
                }
            });
        }

        function sendNew() {
            var bundle = getTable();
            var tot = "<?php echo $id ?>"
            var getSupplier = $("#name").val();
            var getPayment = $("#payment").val();
            var getAmount = totalPayment();
            var getInfo = $("#info").val();
            var getDate = $("#date").val();
            console.log(getDate);
            //console.log(bundle);

            request = $.ajax({
                url: 'http://153.92.4.88:8080/transaction-in/'+tot,
                type: 'put',
                data: {
                    trans_in_payment_amount: getAmount,
                    trans_in_payment_type: getPayment,
                    trans_in_additional_info: getInfo,
                    trans_in_due_date: getDate,
                    trans_in_detail: bundle
                }
            });
            request.done(function (response) {  
                window.location.href = "<?php echo base_url() ?>index.php/c_income";
            });
            request.fail(function(response) {
              var success = response.success;
              var message = response.message;
              var data = response.data;
            });
        }

        $("#btn-edit-in").click(function (e) { 
            e.preventDefault();
            if ($("#tb_material").length < 1) {
                alert("Input material first !");
            }else{
                sendNew();
            }
        });

        $("#subMaterial").click(function (e) { 
            e.preventDefault();
            var getName = $("#brandName option:selected").text();
            var getQuant = $("#matQuant").val();
            var getRow = $("table tbody tr").length;
            var getID = $("#brandName").val();
            var createID = getID+"-"+(getRow+1);

            var payload = '<tr class="t-item" id="'+createID+'">'+
                                '<td class="p-3 c-text-2 text-center">'+getName+'</td>'+
                                '<td class="p-3 c-text-2 text-center">'+getQuant+'</td>'+
                                '<td class="p-3 c-text-2 text-center ">'+
                                    '<a href="#">'+
                                        '<button class="ml-2 basic-btn c-color-primary" >'+
                                            '<i class="bx bx-show bx-xs text-white" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                    '<a href="#" onclick="deleteItem(\''+createID+'\')">'+
                                        '<button class="ml-2 basic-btn c-soft-background" >'+
                                            '<i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                '</td>'+
                            '</tr>';
            $("#tb_material").append(payload);
        });

        $("#show_again").click(function (e) { 
            e.preventDefault();
            showModMate();
        });

        $("#btnRefresh").click(function (e) { 
            e.preventDefault();
            totalQuant();
            setMatBack();
        });


        $("#addMaterialFin").click(function (e) { 
            e.preventDefault();
            addNewMate()
        });

    });

    function getTable() {
        var material = [];
        var getLong = $("#tb_material tr").length;

        $("#tb_material").find("tr").each(function (index, element) {
            // element == this
            var getId = $(element).find('td');
            var getId1 = $(element).find('td input');
            var quantId = "#add-"+(index+1)+"-quant";
            var getMatName = "";
            var getQuant = $(quantId).val();
            var getMatId = "null";
            var getPrice = "10000";
            var getBraId = "null";
            var getBraName = "null";

            var item = {};
            item.detail_in_material_name= getMatName;
            item.detail_in_material_id = getMatId;
            item.detail_in_material_amount = getQuant;
            item.detail_in_material_price = getPrice;
            item.detail_in_merek_id = getBraId;
            item.detail_in_merek_name = getBraName;
            material.push(item);            
            
        });
        return material;
    }

    function cicilanBundle() {
        var getCicilanBundle = [];
        
        $(".field-cicilan").each(function (index, element) {
            // element == this
            var id = "#cicilan-number-"+(index+1);
            console.log(id);
            var getCicilan = $(element).val();
            var getInfo = $(id).text();
            var item = {};
            var split1 = getCicilan.split("-");
            var getDate = getCicilan+" 00:00:00";            
            var getCount = $("#paymentCount").val();
            var getInstal = totalPayment()/parseInt(getCount);

            item.credit_id = "PAY-"+(parseInt(index)+1);
            item.credit_amount = getInstal;
            item.credit_additional_info = getInfo;
            item.credit_due_date = getDate;
            getCicilanBundle.push(item);
        });
        
        return getCicilanBundle;
    }

    function deleteItem(id) {
        console.log(id);
        var fag = "#"+id;
        var splitId = id.split("-");
        var getBackId = "#back-"+splitId[1];
        $(getBackId).remove();
        $(fag).remove();
        totalQuant();
    }

    function showQuant(material, price, satuan, satuanAmount) {
        
        $("#editName").val(material);
        $("#editPrice").val(price);
        var getSatuan = satuan.split(' - ');
        var finSatuan = getSatuan.reverse();
        var payload = '';

        for (let i = 0; i < finSatuan.length; i++) {
            const context = finSatuan[i];
            payload += '<option value="'+satuanAmount+'">'+context+'</option>';
        }
        $("#matSatuan").html(payload);

        $("#modQuantMaterial").modal("show");
        $("#addNewMaterial").modal("hide");
    }

    function showModMate() {
        $("#modQuantMaterial").modal("hide");
        $("#addNewMaterial").modal("show");
    }

    function addNewMate() {

        var getMat = $("#editName").val();
        var getBrand = $("#editBrand").val();
        var getQuant = $("#editQuant").val();
        var getPrice = $("#editPrice").val();
        var getIndex = $("#matSatuan").prop("selectedIndex");

        var getRow = $("#tb_material tr").length;
        var createID = "add"+"-"+(getRow+1);

        if (getQuant < 1) {
            getQuant = 1;
        }

        //cek dropdown satuan material
        if (getIndex == 1) {
            var getFinal = getQuant * $("#matSatuan option:selected").val();
            getQuant = getFinal;
        }

        var finalPrice = parseInt(getQuant)*parseInt(getPrice);  
        console.log(finalPrice);

         var payload = '<tr id="'+createID+'" class="t-item">'+
                                '<td class="p-2 c-text-2 text-center" id="'+createID+'-name">'+getMat+'</td>'+
                                '<td class="p-2 c-text-2 text-center d-none" id="'+createID+'-name">'+getBrand+'</td>'+
                                '<td class="p-2 c-text-2 text-center"><input id="'+createID+'-quant" class="text-center search-fill col-4" placeholder="Quantity..." value="'+getQuant+'"></td>'+
                                '<td class="p-2 c-text-2 text-center" id="'+createID+'-price">'+getPrice+'</td>'+
                                '<td class="p-2 c-text-2 text-center d-none" id="'+createID+'-total">'+finalPrice+'</td>'+
                                '<td class="p-2 c-text-2 text-center ">'+
                                    '<a href="#" onclick="deleteItem(\''+createID+'\')">'+
                                        '<button class="ml-2 basic-btn c-soft-background" >'+
                                            '<i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                '</td>'+
                            '</tr>';
        $("#tb_material").append(payload);
        setBackMaterial(getMat, getQuant, getPrice, finalPrice);
        totalQuant();
    }

    function setBackMaterial(material, quant, price, total) {
        var getAmount = $("#tb_material tr").length;
        var payload = '';
        
        for (let o = 0; o < getAmount; o++) {
            payload = '<tr id="back-'+(o+1)+'">'+
                            '<td><input name="materialBack'+(o+1)+'" id="materialBack'+(o+1)+'" type="text" value="'+material+'"></td>'+
                            '<td><input name="quantBack'+(o+1)+'" id="quantBack'+(o+1)+'" type="text" value="'+quant+'"></td>'+
                            '<td><input name="priceBack'+(o+1)+'" id="priceBack'+(o+1)+'" type="text" value="'+price+'"></td>'+
                            '<td><input name="totalBack'+(o+1)+'" id="totalBack'+(o+1)+'" type="text" value="'+total+'"></td>'+
                        '</tr>';
        }
        $("#back-item-material").val(getAmount);
        $("#back-material").append(payload);
    }

    function totalQuant() {
        var total = 0;

        $("#tb_material").find("tr").each(function (index, element) {
            var quantId = "#add-"+(index+1)+"-quant";
            //var priceId = "#add-"+(index+1)+"-total";
            var getPrice = "#add-"+(index+1)+"-price";
            var getQuant = $(quantId).val();
            var getPrice = $(getPrice).text();
            var semi = parseInt(getQuant) * parseInt(getPrice);
            total += parseInt(semi);
        });

        var gim =  "Rp "+total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        $("#amount").val(gim);
        $("#back-amount-material").val(total);
    }

    function totalPayment() {
        var total = 0;

        $("#tb_material").find("tr").each(function (index, element) {
            // element == this
            var quantId = "#add-"+(index+1)+"-quant";
            //var priceId = "#add-"+(index+1)+"-total";
            var getPrice = "#add-"+(index+1)+"-price";
            var getQuant = $(quantId).val();
            var getPrice = $(getPrice).text();
            var semi = parseInt(getQuant) * parseInt(getPrice);
            total += parseInt(semi);
        });

        return total;
    }

</script>