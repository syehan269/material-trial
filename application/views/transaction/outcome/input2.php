<div class="main-content col-10" style="padding-bottom: 100px !important">
    <div class="d-inline-flex col-12 pl-3 p-0 mb-4">
        <p class="mb-0 c-text-6 text-color regular-weight ml-4" id="title">Tambah Transaksi Keluar</p>
        <div class="dropdown ml-auto">
            <button tabindex="-1" class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                AM
            </button>
            <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
            </div>
        </div>
    </div>

    <div class="col-12 pl-4 p-0">
        <div class="col-12 main-padding-r">
            <div class="col-12 px-0 mx-0 row">
                <button id="btnNewMaterial" type="button" data-toggle="modal" data-target="#modal-material" class="btn-outline col-4 c-border-primary primary-title c-main-background c-text-2 boldest-weight">
                    Tambah Material
                </button>
                <input name="jumlah-material" class="d-none search-fill ml-auto col-1 border-0 my-auto" type="number" id="jumlah-material" tabindex="-1" value="0" readonly>
            </div>
            <div class="mt-4 custom-card p-3" style="min-height: 300px !important;">
                <table id="mainMaterialTable" class="col-12 p-3"  width="100%">
                    <thead class="t-header primary-title">
                        <tr>
                            <th class="p-3 c-text-2 boldest-weight text-center d-none">ID</th>
                            <th class="p-3 c-text-2 boldest-weight">Nama</th>
                            <th class="p-3 c-text-2 boldest-weight">Jumlah</th>
                            <th class="p-3 c-text-2 boldest-weight text-center d-none">Harga Retail</th>
                            <th class="p-3 c-text-2 boldest-weight text-center d-none">Harga Reseller</th>
                            <th class="p-3 c-text-2 boldest-weight text-center d-none">Harga Grosir</th>
                            <th class="p-3 c-text-2 boldest-weight text-center d-none">Min</th>
                            <th class="p-3 c-text-2 boldest-weight">Utilitas</th>
                        </tr>
                    </thead>
                    <tbody id="tbMaterial">
                        
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-12 mt-4">
            <div class="col-12 row">
                <div class="col-6 p-0">
                    <label class="c-text-2 soft-title medium-weight">Nama Pembeli</label>
                    <input name="type-pembeli" class="c-text-2 medium-weight d-none" id="status-pembeli" value="db">
                    <!-- <input class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Name..."> -->
                    <span class="px-0" id="nama-pembeli-option">
                        <select name="pembeli" id="nama-pembeli" style="display: none" class=" c-text-2 search-fill col-11" >
                            <option value="">Pilih Pelanggan</option>
                            <?php
                                foreach ($customer as $item ) {
                            ?>
                                <option value="<?php echo $item->customer_membership.'|'.$item->customer_type.'|'.$item->customer_saldo.'|'.$item->customer_address.'|'.$item->customer_id.'|'.$item->customer_name ?>"><?php echo $item->customer_name ?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </span>
                    <input style="display: none" name="pembeli-field" class="search-fill col-11 border-0 my-auto" type="name" autocomplete="off" id="nama-pembeli-field">
                    <button tabindex="-1" id="switch-pembeli" type="button" class="ml-2 my-auto basic-btn c-color-primary" >
                        <i class="bx bx-rotate-right bx-xs text-white" style="margin-top: 5px"></i>
                    </button>
                </div>
                <div class="col-3 pl-4">
                    <label class="c-text-2 soft-title medium-weight">Tipe Pembeli</label>
                    <input tabindex="-1" class="search-fill col-12 border-0 my-auto" name="tipe-pembeli" id="tipe-pembeli" value="-" readonly>
                </div>
                <div class="col-3">
                    <label class="c-text-2 soft-title medium-weight">Saldo Pembeli</label>
                    <input tabindex="-1" class="search-fill col-12 border-0 my-auto" name="saldo-pembeli" id="saldo-pembeli" value="0" readonly>
                </div>
            </div>
            <div class="col-12 mt-4 row">
                <div class="col-6 px-0">
                    <label class="c-text-2 soft-title medium-weight">Metode Pembayaran</label>
                    <select name="metode-pembayaran" id="metode-pembayaran" class="c-text-2 search-fill col-11" >
                        <option value="">Pilih Metode</option>
                        <option value="Cash">Tunai</option>
                        <option value="Credit">Kredit</option>
                        <option value="ATM">ATM</option>
                        <option value="Saldo">Saldo Pelanggan</option>
                    </select>
                    <button data-toggle="modal" data-target="#modal-cicilan" id="add-cicilan" type="button" style="display: none" class="ml-2 my-auto basic-btn c-color-primary" >
                        <i class="bx bxs-pencil bx-xs text-white" style="margin-top: 5px"></i>
                    </button>
                </div>
                <div class="col-3 pl-4" id="form-term-of-payment" style="display: none">
                    <label class="c-text-2 soft-title medium-weight">Term of Payment</label>
                    <input tabindex="-1" name="term-payment" class="search-fill col-12 border-0 my-auto" id="term-payment" value="-" readonly>
                </div>
                <div class="col-3 pl-4 pr-3" id="form-rekening" style="display: none">
                    <label class="c-text-2 soft-title medium-weight">No. Rekening Tujuan</label>
                    <select name="rekening" id="nama-bank" class="dropdown-select2 c-text-2 search-fill col-12" >
                        <option value="">Pilih Rekening</option>
                        <?php
                            foreach ($rekening as $item ) {
                        ?>
                            <option value="<?php echo $item->bank_rekening.'|'.$item->nomor_rekening ?>"><?php echo $item->bank_rekening." - ".$item->nomor_rekening ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-12 mt-4 row">
                <div class="col-6 px-0">
                    <label class="c-text-2 soft-title medium-weight">Jumlah Uang Pembelian</label>
                    <input name="uang-terbayar" class="search-fill col-12 border-0 my-auto" type="number" id="jumlah-pembayaran" value="0" required>
                </div>
                <div class="col-3 my-auto pt-4">
                    <input name="diantar" type="checkbox" value="1" class="mx-2 my-auto" id="delivery-check">
                    <label class="my-auto c-text-2 soft-title medium-weight" for="delivery-check">Pembelian Diantar ?</label>
                </div>
            </div>
            <div class="col-12 mt-4 row" id="delivery-form" style="display: none !important">
                <div class="col-6 px-0">
                    <label class="c-text-2 soft-title medium-weight">Alamat Pengiriman</label>
                    <input name="alamat-pengiriman" class="search-fill col-12 border-0 my-auto" type="text" id="alamat-pengiriman">
                </div>
                <div class="col-3 pl-4">
                    <label class="c-text-2 soft-title medium-weight">Ongkos Pengiriman</label>
                    <input name="ongkos-pengiriman" class="search-fill col-12 border-0 my-auto" type="number" id="ogkos-pengiriman" value="0">
                </div>
            </div>
            <hr class="mt-4 mb-0">
            <div class="col-12 mt-4 row">
                <div class="col-6 px-0">
                    <label class="c-text-2 soft-title medium-weight">Total Pembayaran</label>
                    <input name="total-pembayaran" tabindex="-1" id="total-pembayaran" class="col-12 c-text-2 secondary-field main-padding-l main-padding-r" readonly value="Rp 0">
                </div>
                <div class="col-3 pl-4">
                    <label class="c-text-2 soft-title medium-weight">Poin Transaksi</label>
                    <input name="poin" tabindex="-1" id="poinForm" class="col-12 c-text-2 secondary-field main-padding-l main-padding-r" readonly value="-">
                </div>
                <button id="addSubmit" class="btn-add col-6 c-text-2 text-white c-color-primary c-color-primary mt-5">Tambah Transaksi</button>
                <input class="d-none" id="mate-total" type="number" value="0">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-material" tabindex="-1" role="dialog" aria-labelledby="modal-material" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Material</p>
        <button type="button" class="close" data-dismiss="modal" tabindex="-1" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <div class="d-inline-flex col-12 px-0">
                <input type="text" name="search-mat-out" id="search-in" class="col-6 my-auto c-text-2 search-fill main-padding-l main-padding-r" placeholder="Cari Material..." autofocus>
                <div class="d-flex my-auto ml-auto">
                    <button class="btn-filter c-color-primary" id="filter">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                    </button>
                </div>
            </div>
            <div class="mt-4 custom-card p-3">
                <table id="material-table" class="col-12 p-2" width="100%">
                    <thead class="t-header primary-title">
                        <tr>
                            <th class="p-3 c-text-2 boldest-weight text-center d-none">No.</th>
                            <th class="p-3 c-text-2 boldest-weight text-center">Material</th>
                            <th class="p-3 c-text-2 boldest-weight text-center">Jumlah</th>
                            <th class="p-3 c-text-2 boldest-weight text-center">H. Ecer</th>
                            <th class="p-3 c-text-2 boldest-weight text-center">H. Grosir</th>
                            <th class="p-3 c-text-2 boldest-weight text-center">H. Reseller</th>
                            <th class="p-3 c-text-2 boldest-weight text-center">Utilitas</th>
                        </tr>
                    </thead>
                    <tbody id="newMaterialBody">
                        <?php 
                            $i = 1;
                            foreach ($material as $item ) {
                        ?>
                            <tr>
                                <td class="p-3 c-text-2 text-center d-none"><?php echo $i++ .'.' ?></td>
                                <td id='' class="p-3 c-text-2 text-center"><?php echo $item->material_name ?></td>
                                <?php if($item->material_stock == 0): ?>
                                <td id='' class="p-3 c-text-2 text-credit text-center"><?php echo number_format($item->material_stock ,0,".",".") ?></td>
                                <?php elseif($item->material_stock <= $item->material_min_stock): ?>
                                <td id='' class="p-3 c-text-2 text-center"><?php echo number_format($item->material_stock ,0,".",".") ?> (LOW)</td>
                                <?php else: ?>
                                <td id='' class="p-3 c-text-2 text-center"><?php echo number_format($item->material_stock ,0,".",".") ?></td>
                                <?php endif; ?>
                                <td id='' class="p-3 c-text-2 text-center"><?php echo 'Rp '.number_format($item->material_harga_agen ,0,".",".")  ?></td>
                                <td id='' class="p-3 c-text-2 text-center"><?php echo 'Rp '.number_format($item->material_harga_grosir ,0,".",".")  ?></td>
                                <td id='' class="p-3 c-text-2 text-center"><?php echo 'Rp '.number_format($item->material_harga_reseller ,0,".",".")  ?></td>
                                <td class="text-center">
                                    <?php if($item->material_stock > 0): ?>
                                    <button class="ml-2 basic-btn c-color-primary"
                                        onclick="setData('<?php echo $item->material_id ?>','<?php echo $item->material_name ?>','<?php echo $item->material_harga_reseller ?>',
                                                        '<?php echo $item->material_harga_agen ?>','<?php echo $item->material_harga_grosir ?>','<?php echo $item->material_min_stock ?>',
                                                        '<?php echo $item->material_min_trans ?>','<?php echo $item->material_sat_atas ?>','<?php echo $item->material_sat_bawah ?>',
                                                        '<?php echo $item->material_sat_ratio ?>','<?php echo $item->material_stock ?>','<?php echo $item->material_is_pass ?>')">
                                        <i class="bx bx-message-square-add text-white " style="margin-top: 5px;"></i>
                                    </button>
                                    <?php else: ?>
                                        <button class="ml-2 basic-btn c-color-default" title="Stock Habis">
                                            <i class="bx bx-message-square-add text-white " style="margin-top: 5px;"></i>
                                        </button>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<!-- input quantity -->
<div class="modal fade" id="modal-jumlah" tabindex="-1" role="dialog" aria-labelledby="close" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Material</p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0 mt-1">
            <p class="c-text-2 soft-title regular-weight" id="">Kuantitas Material</p>
            <input type="number" id="kuantitas-material" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Nilai default '1'">
        </div>
        <div class="col-12 p-0 mt-3 ">
            <p class="c-text-3 soft-title regular-weight">Satuan</p>
            <select name="name" id="satuan-material" style="width: 100%" class=" col-12 c-text-2 search-fill main-padding-l main-padding-r" >
                <option value="">Pilih Satuan</option>
            </select>
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight d-none" id="">Status Material</p>
            <input type="text" id="status-material" class="col-12 d-non c-text-2 search-fill main-padding-l main-padding-r" placeholder="">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight d-none" id="">Harga retail Material</p>
            <input type="text" id="harga-retail" class="col-12 d-none c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight d-none" id="">Harga reseller Material</p>
            <input type="text" id="harga-reseller" class="col-12 d-none c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight d-none" id="">Id Material</p>
            <input type="text" id="id-material" class="col-12 d-none c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight " id="">min-grosir Material</p>
            <input type="text" id="min-grosir" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight " id="">Stock </p>
            <input type="text" id="stock-material" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight " id="">harga grosir Material</p>
            <input type="text" id="harga-grosir" class="col-12  c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-3 ">
            <p class="c-text-2 soft-title regular-weight" id="">Harga Harian (Opsional)</p>
            <input type="number" id="harga-harian" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Harga Harian...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="">Nama Material</p>
            <input type="text" id="nama-material" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button id="show_again" type="button" class="btn-modal-negative c-text-2 mr-3 medium-weight" data-dismiss="modal">Kembali</button>
        <button id="btn-material" type="button" class="btn-modal-positive medium-weight c-text-2">Tambah Material</button>
      </div>
    </div>
  </div>
</div>

<!-- input cicilan  -->
<div class="modal fade" id="modal-cicilan" tabindex="-1" role="dialog" aria-labelledby="modal-cicilan" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Cicilan</p>
      </div>
      <div class="modal-body c-main-background">
        <div class=" mt-3 p-0">
            <p class="c-text-2 soft-title regular-weight">Term of Payment</p>
            <div class="col-12 p-0 d-inline-flex">
                <div class="col-9 p-0">
                    <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="deadline-cicilan">
                </div>
                <div class="col-3 p-0 ml-2">
                    <select id="waktu-cicilan" name="payment" class="c-dropdown  c-text-2" >
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="Hari">Hari</option>
                        <option class="dropdown-item" value="Minggu">Minggu</option>
                        <option class="dropdown-item" value="Bulan">Bulan</option>
                    </select>
                </div>
            </div>
        </div>
        <hr>
        <div class="col-12 p-0">
            <div class="col-12 p-0">
                <p class="c-text-2 soft-title regular-weight">Pembayaran Sekarang (Opsional)</p>
                <input type="number" autocomplete="off" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="saldo-cicilan">
            </div>
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" tabindex="-1" class="btn-modal-negative mr-3 medium-weight" data-dismiss="modal">Batal</button>
        <button type="button" id="btn-cicilan" class="btn-modal-positive medium-weight">Konfirmasi</button>
      </div>
    </div>
  </div>
</div>

<!-- edit jumlah  -->
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-edit" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Edit Jumlah</p>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <div class="col-12 p-0 d-none">
                <p class="c-text-2 soft-title regular-weight">id Material</p>
                <input type="number" autocomplete="off" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="id-row">
            </div>
            <div class="col-12 p-0">
                <p class="c-text-2 soft-title regular-weight">Jumlah Material</p>
                <input type="number" autocomplete="off" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="edit-jumlah">
            </div>
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" tabindex="-1" class="btn-modal-negative mr-3 medium-weight" data-dismiss="modal">Batal</button>
        <button type="button" id="btn-edit" class="btn-modal-positive medium-weight">Edit</button>
      </div>
    </div>
  </div>
</div>

<!-- out of stock  -->
<div class="modal fade" id="modal-habis" tabindex="-1" role="dialog" aria-labelledby="modal-edit" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Peringatan !</p>
      </div>
      <div class="modal-body c-main-background">
        <p>Anda tidak bisa mengambil <span id="habis-stok"></span> stok untuk barang <span id="habis-nama"></span> karena <span id="habis-batas"></span></p>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-positive medium-weight" data-dismiss="modal">Kembali</button>
      </div>
    </div>
  </div>
</div>

<!-- print out layout -->
<div class="modal fade" id="printOut" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
  
    <form action="<?php echo base_url('index.php/c_outcome/input_all') ?>" method="post" class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Check Out</p>
      </div>
      <div id="print-invoice" class="modal-body c-main-background">
        
        <!-- INVOICE -->
        <div class="container-fluid py-2" style="border:solid black 2px">
            <div class="col h-100" style="position:relative">
                <div class="row">
                    <div class="w-25">
                        <img src="https://www.google.com/logos/doodles/2020/thank-you-doctors-nurses-and-medical-workers-copy-6753651837108778-law.gif" class="img-fluid img-thumbnail"/>
                    </div>
                </div>
                <div class="row w-100 h-100" style="position:absolute;top:0">
                    <h3 class="text-center font-weight-bold m-auto w-100"><b>INVOICE</b></h3>
                </div>
            </div>
            <div class="col mt-2">
                <tr><h4><b>TOKO BANGUNAN</b></h4></tr>
                <div class="row mt-2">
                    <div class="col-7">
                        <table class="d-none">
                            <tr ><h4 class="d-none"><b>Alamat . . . .</b></h4></tr>
                            <tr ><h4 class="d-none"><b>Alamat . . . .</b></h4></tr>
                            <tr>
                                <td><b>Phone</b></td>
                                <td><b>:</b></td>
                                <td><b>(xxx - xxxxxxx)</b></td>
                            </tr>
                            <tr>
                                <td><b>Fax</b></td>
                                <td><b>:</b></td>
                                <td><b>(xxx - xxxxxxx)</b></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-5">
                        <table>
                            <tr>
                                <td><b>Tanggal Invoice</b></td>
                                <td><b>:</b></td>
                                <td><b id="invoice-check"></b></td>
                            </tr>
                            <tr>
                                <td><b>Metode Pembayaran</b></td>
                                <td><b>:</b></td>
                                <td><b id="payment-check"></b></td>
                            </tr>
                            <tr>
                                <td><b>Proses Pembelian</b></td>
                                <td><b>:</b></td>
                                <td><b id="package-check"></b></td>
                            </tr>
                            <tr id="ongkirPrint">
                                
                            </tr>
                            <tr id="alamatPrint">
                                
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col">
                <table>
                    <tr>
                        <td style="vertical-align:top"><b>Customer</b> </td>
                        <td style="vertical-align:top"><b>:</b> <span id="buyerName-check"></span></td>
                    </tr>
                </table>
            </div>
            <div class="col mt-2">
                <table class="table font-weight-bold" >
                    <thead>
                        <tr>
                            <th><b>No.</b></th>
                            <th><b>Nama Barang</b></th>
                            <th><b>Kuantitas</b></th>
                            <th><b>Harga</b></th>
                            <th><b>Jumlah</b></th>
                        </tr>
                    </thead>
                    <tbody id="check-out-table">

                    </tbody>
                    <tfoot>
                        <tr id="setTotalPayment">
                            
                        </tr>
                        <tr id="setTotalKembalian">

                        </tr>
                    </tfoot>
                </table>
                <div class="col text-right">
                    <b>
                        <h3>Harga Total : </h3>
                        <h2><b>Rp. <span id="check-out-total"></span></b></h2>
                    </b>
                </div>
                <br>
                <div class="row">
                <div class="col-3 text-center">
                    <b>
                        <h5>Tanda Terima</h5>
                        <br>
                        <br>
                        (...............................)
                    </b>
                </div>
                <div class="col-6">
                </div>
                <div class="col-3 text-center">
                    <b>
                        <h5>Hormat Kami</h5>
                        <br>
                        <br>
                        (...............................)
                    </b>
                </div>
            </div>
        <!-- temporary table -->
        <!-- temporary table -->
        <!-- temporary table -->
        <!-- temporary table -->
        <!-- temporary table -->
            <div class="">
                <input type="text" id="id" name="id-item" placeholder="id">
                <input type="text" id="info-custom" name="info-item" placeholder="info">
                <input type="text" id="sales" name="sales-item" placeholder="sales">
                <input type="text" id="bank-dest" name="bank-item" placeholder="bank">
                <input type="text" id="rekening-no" name="rekening-no-item" placeholder="rekening-no">
                <input type="text" id="method-custom" name="method-item" placeholder="method">
                <input type="text" id="ongkir-pay" name="ongkir-pay" placeholder="ongkir-pay">
                <input type="text" id="paid-amount" name="paid-item" placeholder="paid">
                <input type="text" id="state" name="state-item" placeholder="state" value="db">
                <table style="width:100%">
                    <tr>
                        <th>saldo</th>
                        <th>tanggal</th>
                        <th>deadline</th>
                    </tr>
                    <tbody id="back-cicilan">
                        <tr>
                            <td><input  name="saldo-cicilan" id="saldoBack" type="text" value="0"></td>
                            <td><input  name="tanggal-cicilan" id="tanggalBack" type="text" ></td>
                            <td><input  name="text-cicilan" id="textBack" type="text" ></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="mt-3 d-none">
                <input type="text" id="back-amount-material" name="materialAmount" placeholder="material amount">
                <input type="number" id="back-item-material" name="materialItem" placeholder="material item">
                <input type="text" id="back-customer-id" name="backCustomerId" placeholder="customer id">
                <input type="text" id="back-customer" name="backCustomer" placeholder="customer">
                <input type="text" id="back-member" name="backMember" placeholder="member">
                <input type="text" id="back-saldo" name="backSaldo" placeholder="saldo">
                <input type="text" id="back-alamat" name="backAlamat" placeholder="alamat">
                <table style="width:100%">
                    <tr>
                        <th>id material</th>
                        <th>material</th>
                        <th>quant</th> 
                        <th>harga</th>
                        <th>total</th>
                    </tr>
                    <tbody id="back-material">
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.INVOICE -->
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Cancel</button>
        <button type="submit" onclick="" id="checkout-send1" class="btn-modal-negative mr-3 medium-weight c-text-2 c-color-primary text-white">Submit</button>
        <!-- <button id="subMaterial" type="button" class="btn-modal-positive medium-weight c-text-2">Add Material</button> -->
      </div>
    </form>
  </div>
</div>

<script>
    $(document).ready(function () {
        let table = null;
        dataTable();
        checkout();

        $(".dropdown-select2").select2({
            width: '100%'
        });

        $("#nama-pembeli").select2();

        function dataTable(){
            table = $('#material-table').DataTable({
                "lengthChange": false,
                "ordering": false,
                "tabIndex": -1,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                }
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

        $("#search-in").on("input", function () {
            search(1, this.value);
        });        

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#material-table').DataTable()
                .search('').columns()
                .search('').draw();
            $("#search-in").val("");
        });

        $("#delivery-check").change(function (e) { 
            e.preventDefault();
            $("#delivery-form").toggle(this.checked);
        });

        $("#nama-pembeli").change(function (e) { 
            e.preventDefault();
            $("#tipe-pembeli").val(getType());
            $("#saldo-pembeli").val(getSaldo());
            $("#alamat-pengiriman").val(getAlamat());
            countHarga();
            countPoin();
        });

        $("#addSubmit").click(function (e) { 
            checkout();
            $("#printOut").modal("show");
        });

        $("#metode-pembayaran").change(function (e) { 
            e.preventDefault();
            let metode = this.value;
            if (metode === "Cash") {
                $("#jumlah-pembayaran").val(getTotal());
                $("#form-term-of-payment").toggle(false);
                $("#form-rekening").toggle(false);
                $("#add-cicilan").toggle(false);
            }else if (metode === "Credit") {
                $("#add-cicilan").toggle(true)
                $("#jumlah-pembayaran").val("0");
                $("#form-term-of-payment").toggle(true);
                $("#form-rekening").toggle(false);
            }else if(metode === "ATM"){
                $("#form-rekening").toggle(true);
                $("#jumlah-pembayaran").val(getTotal());
                $("#form-term-of-payment").toggle(false);
            }else{
                $("#jumlah-pembayaran").val(getTotal());
                $("#form-term-of-payment").toggle(false);
                $("#form-rekening").toggle(false);
                $("#add-cicilan").toggle(false);
            }
        });

        $("#switch-pembeli").click(function (e) { 
            e.preventDefault();
            let status = $("#status-pembeli").val();
            if (status === "db") {
                $("#tipe-pembeli").val("Retail");
                $("#status-pembeli").val("custom");
                $("#saldo-pembeli").val("0");
                $("#alamat-pengiriman").val("");
                $("#nama-pembeli-field").toggle(true);
                $("#nama-pembeli-option").toggle(false);
            }else{
                $("#poinForm").val("-");
                $("#tipe-pembeli").val(getType());
                $("#status-pembeli").val("db");
                $("#nama-pembeli-field").toggle(false);
                $("#nama-pembeli-option").toggle(true);
            }
            countHarga();
            countPoin();
        });

        $("#btn-cicilan").click(function (e) { 
            e.preventDefault();
            let hari = $("#deadline-cicilan").val();
            let waktu = $("#waktu-cicilan option:selected").val();
            let pembayaran = $("#saldo-cicilan").val();

            if (hari === "") {
                hari = 1;
            }

            $("#term-payment").val(hari+" "+waktu);
            $("#jumlah-pembayaran").val(pembayaran);
            $("#modal-cicilan").modal("hide");
        });

        $("#show_again").click(function (e) { 
            e.preventDefault();
            showMaterial();
            hideJumlah();
        });

        $("#btn-edit").click(function (e) { 
            e.preventDefault();
            let id = $("#id-row").val();
            editRow(id)
        });

        $("#btn-material").click(function (e) { 
            e.preventDefault();

            var batas = $("#stock-material").val();
            let nama = $("#nama-material").val();
            let jumlah = $("#kuantitas-material").val();
            let ratio = $("#satuan-material option:selected").val();
        
            jumlah = jumlah * ratio;

            if (jumlah === 0 || jumlah === "") {
                jumlah = 1;
            }
            
            if(jumlah <= batas){

                let id = $("#id-material").val();
                let grosir = $("#harga-grosir").val();
                let reseller = $("#harga-reseller").val();
                let retail = $("#harga-retail").val();
                let minTrans = $("#min-grosir").val();
                let stok = $("#stock-material").val();
                let harian = $("#harga-harian").val();
                let status = $("#status-material").val();
                let count = $("#mainMaterialTable").find("tr").length;
                //class nandai buat hitung poin
                let mark = "not-marked";
                let retailMark = "not-retail";
                let resellerMark = "not-reseller";
                let grosirMark = "not-grosir";
                let minMark = "not-min";

                if (status === "1") {
                    mark = "marked";
                    retailMark = "";
                    resellerMark = "";
                    grosirMark = "";
                    minMark = "";
                }

                if (harian != "" && harian > 0) {
                    reseller = harian;
                    retail = harian;
                }

                let tbRow = '<tr id="'+count+'">'+
                                '<td class="p-3 c-text-2 text-center d-none">'+
                                    '<input name="id-'+count+'" class="search-fill col-10 border-0 my-auto" type="text" value="'+id+'" tabindex="-1" readonly>'+
                                '</td>'+
                                '<td class="c-text-2">'+
                                    '<input name="nama-'+count+'" id="nama-'+count+'" class="search-fill col-10 border-0 my-auto" type="text" value="'+nama+'" tabindex="-1" readonly>'+
                                '</td>'+
                                '<td class="p-3 c-text-2">'+
                                    '<input name="jumlah-'+count+'" id="jumlah-'+count+'" class="jumlah search-fill col-10 border-0 my-auto '+mark+' global" type="text" value="'+jumlah+'" tabindex="-1" readonly>'+
                                '</td>'+
                                '<td class="p-3 c-text-2 text-center d-none">'+
                                    '<input name="Retail-'+count+'" id="Retail-'+count+'" class="retail '+retailMark+' search-fill col-10 border-0 my-auto" type="text" value="'+retail+'" tabindex="-1" readonly>'+
                                '</td>'+
                                '<td class="p-3 c-text-2 text-center d-none">'+
                                    '<input name="Reseller-'+count+'" id="Reseller-'+count+'" class="reseller '+resellerMark+' search-fill col-10 border-0 my-auto" type="text" value="'+reseller+'" tabindex="-1" readonly>'+
                                '</td>'+
                                '<td class="p-3 c-text-2 text-center d-none">'+
                                    '<input name="Grosir-'+count+'" id="Grosir-'+count+'" class="grosir '+grosirMark+' search-fill col-10 border-0 my-auto" type="text" value="'+grosir+'" tabindex="-1" readonly>'+
                                '</td>'+
                                '<td class="p-3 c-text-2 text-center d-none">'+
                                    '<input name="min-'+count+'" id="min-'+count+'" class="min-grosir '+minMark+' search-fill col-10 border-0 my-auto" type="text" value="'+minTrans+'" tabindex="-1" readonly>'+
                                '</td>'+
                                '<td>'+
                                    '<button tabindex="-1" onclick="showEdit('+count+','+jumlah+')" type="button" class="ml-2 basic-btn c-soft-primary" >'+
                                        '<i class="bx bx-pencil bx-xs primary-title" style="margin-top: 5px"></i>'+
                                    '</button>'+
                                    '<button tabindex="-1" onclick="deleteRow('+count+')" type="button" class="ml-2 my-auto basic-btn c-color-primary" >'+
                                        '<i class="bx bx-trash bx-xs text-white" style="margin-top: 5px"></i>'+
                                    '</button>'+
                                '</td>'+
                            '</tr>';

                $("#tbMaterial").append(tbRow);
                countHarga();
                countPoin();
                jumlahMaterial();
            }else{
                var status = '';
                $("#habis-nama").html(nama);
                $("#habis-stok").html(jumlah);

                if(batas == '0'){
                    status += 'habis';
                }else{
                    status += 'tersisa '+batas;
                }

                $("#habis-batas").html(status);
                $("#modal-habis").modal('show');
            }
        });

    });

    function checkout(){
        var count = parseInt($("#mate-total").val());
        var checkOutTable = '';
        var i;

        for(i = 0; i < count; i++){

            var realCount = i + 1;

            var getTotal = $("#jumlah-"+realCount).val();
            var getQuant = $("#jumlah-"+realCount).val();
            var getPrice = $("#Grosir-"+realCount).val();
            var getMate = $("#nama-"+realCount).val();

            checkOutTable += '<tr>'+
                                '<th>'+realCount+'</th>'+
                                '<th>'+getMate+'</th>'+
                                '<th>'+count+'</th>'+
                                '<th>Rp. ,-</th>'+
                                '<th>Rp. ,-</th>'+
                            '</tr>';
        }
        
        $("#check-out-table").html(checkOutTable);
    }

    countHarga = () => {
        let total = getTotal();
        $("#total-pembayaran").val("Rp "+total);
    }

    getTotal = () => {
        let total = 0;
        let getType = $("#tipe-pembeli").val();
        let count = $("#tbMaterial").find("tr").length;

        if (getType === "Reseller") {
            for (let i = 0; i < count; i++) {
                let jumlah = document.getElementsByClassName('jumlah')[i];
                let harga = document.getElementsByClassName('reseller')[i];
                let grosir = document.getElementsByClassName('grosir')[i];
                let min = document.getElementsByClassName('min-grosir')[i];

                if (jumlah && harga && grosir) {
                    if (parseInt(jumlah.value) >= parseInt(min.value)) {
                        harga = grosir;
                    }
                    total += jumlah.value*harga.value;
                } else {
                    continue;
                }
            }
        } else {
            for (let i = 0; i < count; i++) {
                let jumlah = document.getElementsByClassName('jumlah')[i];
                let harga = document.getElementsByClassName('retail')[i];
                let grosir = document.getElementsByClassName('grosir')[i];
                let min = document.getElementsByClassName('min-grosir')[i];
                
                if (jumlah && harga && grosir) {
                    if (parseInt(jumlah.value) >= parseInt(min.value)) {
                        harga = grosir;
                    }
                    total += jumlah.value*harga.value;
                } else {
                    continue;
                }
            }
        }
        
        return total;
    }

    setData = (id, nama, hReseller, hAgen, hGrosir, minStock, minTrans, satAtas, satBawah, ratio, stok, status) => {
        $("#harga-retail").val(hAgen);   
        $("#harga-reseller").val(hReseller);
        $("#harga-grosir").val(hGrosir);
        $("#id-material").val(id);
        $("#nama-material").val(nama);
        $("#min-grosir").val(minTrans);
        $("#stock-material").val(stok);
        $("#status-material").val(status);

        let optRatio = '<option value="1" >'+satBawah+' - Satuan Bawah</option>'+'<option value="'+ratio+'" >'+satAtas+' - Satuan Atas</option>'
        $("#satuan-material").html(optRatio);

        var total = parseInt($("#mate-total").val());
        $("#mate-total").val(total+1)

        hideMaterial();
        showJumlah();
    }

    hideMaterial = () => {
        $("#modal-material").modal("hide");
    }

    showMaterial = () => {
        $("#modal-material").modal("show");
    }

    hideJumlah = () => {
        $("#kuantitas-material").val("");
        $("#modal-jumlah").modal("hide");
    }

    showJumlah = () => {
        $("#modal-jumlah").modal("show");
    }

    hideEdit = () => {
        $("#modal-edit").modal("hide");
    }

    showEdit = (id, jumlah) => {
        $("#id-row").val(id);
        $("#edit-jumlah").val(jumlah);
        $("#modal-edit").modal("show");
    }

    //hitung poin
    countPoin = () => {
        let count = $(".not-marked").length;
        let globalCount = $(".global").length;
        let custType = $("#tipe-pembeli").val();
        //total transaksi material yg diblacklist
        let total = 0;
        let totalTransaksi = getTotal();

        if (count > 0) {
            for (let i = 0; i < count; i++) {
                let reseller = document.getElementsByClassName('not-reseller')[i];
                let retail = document.getElementsByClassName('not-retail')[i];
                let min = document.getElementsByClassName('not-min')[i];
                let grosir = document.getElementsByClassName('not-grosir')[i];
                let jumlah = document.getElementsByClassName('not-marked')[i];

                if (custType === "Reseller") {
                    if (jumlah.value >= min.value) {
                        let quant = jumlah.value * grosir.value;
                        total += quant;
                    } else {
                        let quant = jumlah.value * reseller.value
                        total += quant;
                    }
                } else {
                    if (jumlah.value >= min.value) {
                        let quant = jumlah.value * grosir.value;
                        total += quant;
                    } else {
                        let quant = jumlah.value * retail.value
                        total += quant;
                    }
                }
            }
        }

        //total pembayaran seluruh material & yg diblacklist

        let hasil = 0;
        let sisa = totalTransaksi - total;

        console.log("total aman: "+ sisa);
        while(sisa >= 50000){
            hasil = hasil + 1;
            sisa = sisa - 50000;
        }

        if (hasil > 0) {
            $("#poinForm").val(hasil);
        } else {
            $("#poinForm").val(0);
        }
    }

    getMembership = () => {
        let pembeli = $("#nama-pembeli").val();
        let splited = pembeli.split('|');

        if (splited[0] === "1") {
            splited = true;
        } else {
            splited = false;
        }
        return splited;
    }

    getNama = () => {
        let pembeli = $("#nama-pembeli").text();
        let splited = pembeli.split('|');
        return pembeli;
    }

    getType = () => {
        let pembeli = $("#nama-pembeli").val();
        let splited = pembeli.split('|');
        return splited[1];
    }

    getSaldo = () => {
        let pembeli = $("#nama-pembeli").val();
        let splited = pembeli.split('|');
        return splited[2];
    }

    getAlamat = () => {
        let pembeli = $("#nama-pembeli").val();
        let splited = pembeli.split('|');
        return splited[3];
    }

    jumlahMaterial = () => {
        let count = $("#tbMaterial").find("tr").length;
        $("#jumlah-material").val(count);
    }

    editRow = (id) => {
        let jumlah = $("#edit-jumlah").val();
        $("#jumlah-"+id).val(jumlah);
        countHarga();
        countPoin();
        hideEdit();
    }

    deleteRow = (id) => {
        $("#"+id).remove();
        countHarga();
        countPoin();
        jumlahMaterial();
    }
</script>