    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4" id="title">Tambah Transaksi Keluar</p>
            <div class="dropdown ml-auto">
                <button tabindex="-1" class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <div class="col-12 d-inline-flex p-0">
            <div class="col-6 main-padding-r">
                <button id="btnNewMaterial" type="button" class="btn-outline col-12 c-border-primary primary-title c-main-background c-text-2 boldest-weight">
                    Tambah Material
                </button>
                <div class="mt-4 custom-card p-3" style="min-height: 400px !important;">
                    <table id="mainMaterialTable" class="col-12 p-3" width="100%">
                        <thead class="t-header primary-title">
                            <tr>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">Id</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Material</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Kuantitas</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Harga</th>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">Total</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Utilitas</th>
                            </tr>
                        </thead>
                        <tbody id="tbMaterial">
                            
                        </tbody>
                    </table>
                </div>
                
            </div>

            <div class="flex-column col-6 main-padding-l pr-0">
                <div class="col-12 p-0">
                    <p class="c-text-2 soft-title medium-weight">Nama Pembeli</p>
                    <input class="c-text-2 medium-weight d-none" id="stateBuy" value="db">
                    <!-- <input class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Name..."> -->
                    <span id="spaceBuyer">
                        <select name="name" id="buyerName" class="dropdown-select2 c-text-2 search-fill col-11" >
                            <option value="">Select Pelanggan</option>
                            <?php
                                foreach ($customer as $item ) {
                            ?>
                                <option value="<?php echo $item->customer_membership.'|'.$item->customer_type.'|'.$item->customer_saldo.'|'.$item->customer_address.'|'.$item->customer_id ?>"><?php echo $item->customer_name ?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </span>
                    <button tabindex="-1" id="switchBuyer" type="button" class="ml-2 my-auto basic-btn c-color-primary" >
                        <i class="bx bx-rotate-right bx-xs text-white" style="margin-top: 5px"></i>
                    </button>
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Alamat</p>
                    <input id="alamatBuyer" class="col-12 c-text-2 search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Tipe Pembayaran</p>
                    <!-- <input class="col-11 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Payment..."> -->
                    <select class="c-dropdown col-11 c-text-2" id="paymentMethod">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="Cash">Cash</option>
                        <option class="dropdown-item" value="Credit">Credit</option>
                        <option class="dropdown-item" value="ATM">ATM</option>
                        <option id="optSaldo" class="dropdown-item">Saldo Pelanggan</option>
                    </select>
                    <button tabindex="-1" id="btnCicilan" type="button" class="ml-2 my-auto basic-btn c-color-primary" >
                        <i class="bx bxs-pencil bx-xs text-white" style="margin-top: 5px"></i>
                    </button>
                </div>
                <div id="rekeningAtm">

                </div>
                <div class="col-12 mt-4 p-0 d-none">
                    <p class="c-text-2 soft-title medium-weight">Dana Pembelian</p>
                    <select id="sellMoney1" class="c-dropdown col-12 c-text-2" >
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="Bayarlangsung">Bayar Langsung</option>
                        <option class="dropdown-item" value="Saldo">Saldo Pelanggan</option>
                    </select>
                </div>
                <div id="remainSaldo">
                    
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Metode Pembelian</p>
                    <select id="sellInteract" class="c-dropdown col-12 c-text-2" >
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="Takeaway">Take Away</option>
                        <option class="dropdown-item" value="Delivery">Delivery</option>
                    </select>
                </div>
                <div id="increasePrice">
                    
                </div>
                <div id="demo">
                    
                </div>
                <div id="spacePembayaran" class="col-12 p-0">
                    
                </div>
                <div class="d-inline-flex col-12 p-0">
                    <div class="col-5 mt-4 p-0 mr-3">
                        <p class="c-text-2 soft-title medium-weight">Poin Membership</p>
                        <input id="discountForm" class="col-12 c-text-2 secondary-field main-padding-l main-padding-r" disabled value="0">
                    </div>
                    <div class="col-6 ml-auto mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight">Total Pembayaran</p>
                        <div class="p-0 d-inline-flex col-12">
                            <input id="countTotal" class="col-10 c-text-2 secondary-field main-padding-l main-padding-r" value="Rp 0" disabled>
                            <button tabindex="-1" id="btnRefresh" type="button" class="ml-2 my-auto basic-btn c-color-primary my-auto" >
                                <i class="bx bx-refresh bx-xs text-white" style="margin-top: 5px"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Informasi Tambahan</p>
                    <textarea id="info-trans" class="search-fill c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b" rows="4" style="min-height: 150px; width: 100%"></textarea>
                </div>
                <button id="addSubmit" class="btn-add col-12 c-text-2 text-white c-color-primary c-color-primary mt-4">Tambah Transaksi</button>
            </div>
        </div>
</div>

<!-- input cicilan  -->
<div class="modal fade" id="addCicilan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Cicilan</p>
      </div>
      <div class="modal-body c-main-background">
        <div class=" mt-3 p-0">
            <p class="c-text-2 soft-title regular-weight">Term of Payment</p>
            <div class="col-12 p-0 d-inline-flex">
                <div class="col-9 p-0">
                    <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="deadline-cicilan">
                </div>
                <div class="col-3 p-0 ml-2">
                    <select id="waktu-cicilan" name="payment" class="c-dropdown  c-text-2" >
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="Hari">Hari</option>
                        <option class="dropdown-item" value="Minggu">Minggu</option>
                        <option class="dropdown-item" value="Bulan">Bulan</option>
                    </select>
                </div>
            </div>
        </div>
        <hr>
        <div class="col-12 p-0">
            <div class="col-12 p-0">
                <p class="c-text-2 soft-title regular-weight">Pembayaran Sekarang (Opsional)</p>
                <input type="number" autocomplete="off" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="saldo-cicilan">
            </div>
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight" data-dismiss="modal">Batal</button>
        <button type="button" id="submitCicilan1" class="btn-modal-positive medium-weight">Konfirmasi</button>
      </div>
    </div>
  </div>
</div>

<!-- input quantity -->
<div class="modal fade" id="modQuantMaterial" tabindex="-1" role="dialog" aria-labelledby="close" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Material</p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0 mt-1">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Kuantitas Material</p>
            <input type="number" id="editQuant" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Nilai default '1'">
        </div>
        <div class="col-12 p-0 mt-3 ">
            <p class="c-text-3 soft-title regular-weight">Satuan</p>
            <select name="name" id="satuan" style="width: 100%" class=" col-12 c-text-2 search-fill main-padding-l main-padding-r" >
                <option value="">Pilih Satuan</option>
            </select>
        </div>
        <div class="col-12 p-0 mt-2  d-none">
            <p class="c-text-2 soft-title regular-weight " id="mateQuant">Ratio Satuan</p>
            <input type="number" id="editRatio" class="col-12  c-text-2 search-fill main-padding-l main-padding-r" placeholder="Nilai default '1'" readonly>
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Harga Material</p>
            <input type="text" id="editPrice" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none ">
            <p class="c-text-2 soft-title regular-weight d-none" id="mateQuant">Id Material</p>
            <input type="text" id="editId" class="col-12 d-none c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none ">
            <p class="c-text-2 soft-title regular-weight " id="mateQuant">min-grosir Material</p>
            <input type="text" id="editMin" class="col-12 d-none c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2  d-none">
            <p class="c-text-2 soft-title regular-weight " id="mateQuant">harga grosir Material</p>
            <input type="text" id="editGrosir" class="col-12  c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-3 ">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Harga Harian (Opsional)</p>
            <input type="number" id="editPriceDaily" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Harga Harian...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Nama Material</p>
            <input type="text" id="editName" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button id="show_again" type="button" class="btn-modal-negative c-text-2 mr-3 medium-weight" data-dismiss="modal">Kembali</button>
        <button id="addMaterial" type="button" class="btn-modal-positive medium-weight c-text-2">Tambah Material</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addNewMaterial" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Material</p>
        <button type="button" class="close" data-dismiss="modal" tabindex="-1" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <div class="d-inline-flex col-12 px-0">
                <input type="text" name="search-mat-out" id="search-in" class="col-6 my-auto c-text-2 search-fill main-padding-l main-padding-r" placeholder="Cari Material..." autofocus>
                <div class="d-flex my-auto ml-auto">
                    <button class="btn-filter c-color-primary" id="filter">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                    </button>
                </div>
            </div>
            <div class="mt-4 custom-card p-3">
                    <table id="addNewTable" class="col-12 p-2" width="100%">
                        <thead class="t-header primary-title">
                            <tr>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">No.</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Material</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Jumlah</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">H. Ecer</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">H. Grosir</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">H. Reseller</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Utilitas</th>
                            </tr>
                        </thead>
                        <tbody id="newMaterialBody">
                            <?php 
                                $i = 1;
                                foreach ($material as $item ) {
                            ?>
                                <tr>
                                    <td class="p-3 c-text-2 text-center d-none"><?php echo $i++ .'.' ?></td>
                                    <td id='' class="p-3 c-text-2 text-center"><?php echo $item->material_name ?></td>
                                    
                                    <td id='' class="p-3 c-text-2 text-center"><?php echo number_format($item->material_stock ,0,".",".") ?></td>
                                    <td id='' class="p-3 c-text-2 text-center"><?php echo 'Rp '.number_format($item->material_harga_agen ,0,".",".")  ?></td>
                                    <td id='' class="p-3 c-text-2 text-center"><?php echo 'Rp '.number_format($item->material_harga_grosir ,0,".",".")  ?></td>
                                    <td id='' class="p-3 c-text-2 text-center"><?php echo 'Rp '.number_format($item->material_harga_reseller ,0,".",".")  ?></td>
                                    <td class="text-center">
                                    <?php if($item->material_stock > 0): ?>
                                        <button class="ml-2 basic-btn c-color-primary" onclick="showQuant('<?php echo $item->material_name ?>','<?php echo $item->material_harga_agen ?>','<?php echo $item->material_harga_reseller ?>','<?php echo $item->material_harga_grosir ?>'
                                          ,'<?php echo $item->material_id ?>','<?php echo $item->material_sat_bawah ?>','<?php echo $item->material_sat_atas ?>','<?php echo $item->material_min_trans ?>','<?php echo $item->material_sat_ratio ?>')">
                                            <i class="bx bx-message-square-add text-white " style="margin-top: 5px;"></i>
                                        </button>
                                    <?php else: ?>
                                        <button class="ml-2 basic-btn c-color-secondary" title="Stock kosong">
                                            <i class="bx bx-message-square-add text-white " style="margin-top: 5px;"></i>
                                        </button>
                                    <?php endif; ?>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Batal</button>
        <!-- <button id="subMaterial" type="button" class="btn-modal-positive medium-weight c-text-2">Add Material</button> -->
      </div>
    </div>
  </div>
</div>

<!-- print out layout -->
<div class="modal fade" id="printOut" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
  
    <form action="<?php echo base_url('index.php/c_outcome/input_all') ?>" method="post" class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Check Out</p>
      </div>
      <div id="print-invoice" class="modal-body c-main-background">
        
        <!-- INVOICE -->
        <div class="container-fluid py-2" style="border:solid black 2px">
            <div class="col h-100" style="position:relative">
                <div class="row">
                    <div class="w-25">
                        <img src="https://www.google.com/logos/doodles/2020/thank-you-doctors-nurses-and-medical-workers-copy-6753651837108778-law.gif" class="img-fluid img-thumbnail"/>
                    </div>
                </div>
                <div class="row w-100 h-100" style="position:absolute;top:0">
                    <h3 class="text-center font-weight-bold m-auto w-100"><b>INVOICE</b></h3>
                </div>
            </div>
            <div class="col mt-2">
                <tr><h4><b>TOKO BANGUNAN</b></h4></tr>
                <div class="row mt-2">
                    <div class="col-7">
                        <table class="d-none">
                            <tr ><h4 class="d-none"><b>Alamat . . . .</b></h4></tr>
                            <tr ><h4 class="d-none"><b>Alamat . . . .</b></h4></tr>
                            <tr>
                                <td><b>Phone</b></td>
                                <td><b>:</b></td>
                                <td><b>(xxx - xxxxxxx)</b></td>
                            </tr>
                            <tr>
                                <td><b>Fax</b></td>
                                <td><b>:</b></td>
                                <td><b>(xxx - xxxxxxx)</b></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-5">
                        <table>
                            <tr>
                                <td><b>Tanggal Invoice</b></td>
                                <td><b>:</b></td>
                                <td><b id="invoice-check"></b></td>
                            </tr>
                            <tr>
                                <td><b>Metode Pembayaran</b></td>
                                <td><b>:</b></td>
                                <td><b id="payment-check"></b></td>
                            </tr>
                            <tr>
                                <td><b>Proses Pembelian</b></td>
                                <td><b>:</b></td>
                                <td><b id="package-check"></b></td>
                            </tr>
                            <tr id="ongkirPrint">
                                
                            </tr>
                            <tr id="alamatPrint">
                                
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col">
                <table>
                    <tr>
                        <td style="vertical-align:top"><b>Customer</b> </td>
                        <td style="vertical-align:top"><b>:</b> <span id="buyerName-check"></span></td>
                    </tr>
                </table>
            </div>
            <div class="col mt-2">
                <table class="table font-weight-bold" >
                    <thead>
                        <tr>
                            <th><b>No.</b></th>
                            <th><b>Nama Barang</b></th>
                            <th><b>Kuantitas</b></th>
                            <th><b>Harga</b></th>
                            <th><b>Jumlah</b></th>
                        </tr>
                    </thead>
                    <tbody id="check-out-table">

                    </tbody>
                    <tfoot>
                        <tr id="setTotalPayment">
                            
                        </tr>
                        <tr id="setTotalKembalian">

                        </tr>
                    </tfoot>
                </table>
                <div class="col text-right">
                    <b>
                        <h3>Harga Total : </h3>
                        <h2><b>Rp. <span id="check-out-total"></span></b></h2>
                    </b>
                </div>
                <br>
                <div class="row">
                <div class="col-3 text-center">
                    <b>
                        <h5>Tanda Terima</h5>
                        <br>
                        <br>
                        (...............................)
                    </b>
                </div>
                <div class="col-6">
                </div>
                <div class="col-3 text-center">
                    <b>
                        <h5>Hormat Kami</h5>
                        <br>
                        <br>
                        (...............................)
                    </b>
                </div>
            </div>
        <!-- temporary table -->
        <!-- temporary table -->
        <!-- temporary table -->
        <!-- temporary table -->
        <!-- temporary table -->
            <div class="">
                <input type="text" id="id" name="id-item" placeholder="id">
                <input type="text" id="info-custom" name="info-item" placeholder="info">
                <input type="text" id="sales" name="sales-item" placeholder="sales">
                <input type="text" id="bank-dest" name="bank-item" placeholder="bank">
                <input type="text" id="rekening-no" name="rekening-no-item" placeholder="rekening-no">
                <input type="text" id="method-custom" name="method-item" placeholder="method">
                <input type="text" id="ongkir-pay" name="ongkir-pay" placeholder="ongkir-pay">
                <input type="text" id="paid-amount" name="paid-item" placeholder="paid">
                <input type="text" id="state" name="state-item" placeholder="state" value="db">
                <table style="width:100%">
                    <tr>
                        <th>saldo</th>
                        <th>tanggal</th>
                        <th>deadline</th>
                    </tr>
                    <tbody id="back-cicilan">
                        <tr>
                            <td><input  name="saldo-cicilan" id="saldoBack" type="text" value="0"></td>
                            <td><input  name="tanggal-cicilan" id="tanggalBack" type="text" ></td>
                            <td><input  name="text-cicilan" id="textBack" type="text" ></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="mt-3 d-none">
                <input type="text" id="back-amount-material" name="materialAmount" placeholder="material amount">
                <input type="number" id="back-item-material" name="materialItem" placeholder="material item">
                <input type="text" id="back-customer-id" name="backCustomerId" placeholder="customer id">
                <input type="text" id="back-customer" name="backCustomer" placeholder="customer">
                <input type="text" id="back-member" name="backMember" placeholder="member">
                <input type="text" id="back-saldo" name="backSaldo" placeholder="saldo">
                <input type="text" id="back-alamat" name="backAlamat" placeholder="alamat">
                <table style="width:100%">
                    <tr>
                        <th>id material</th>
                        <th>material</th>
                        <th>quant</th> 
                        <th>harga</th>
                        <th>total</th>
                    </tr>
                    <tbody id="back-material">
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.INVOICE -->
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Cancel</button>
        <button type="submit" onclick="" id="checkout-send1" class="btn-modal-negative mr-3 medium-weight c-text-2 c-color-primary text-white">Submit</button>
        <!-- <button id="subMaterial" type="button" class="btn-modal-positive medium-weight c-text-2">Add Material</button> -->
      </div>
    </form action>
  </div="<?php echo base_url('index.php/c_income/input_all') ?>" method="post">
</div>

<script>
    var getCicilanBundle = [];

    $(document).ready(function () {
        var table = "";
        $('.dropdown-select2').select2();

        $("#search-in").on("input", function () {
            search(1, this.value);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#addNewTable').DataTable()
                .search('').columns()
                .search('').draw();
            $("#search-in").val("");
        });

        getMemberStat();
        addSaldo();
        dataTable();
        checkout();
        addPembayaran();

        function dataTable(){
            table = $('#addNewTable').DataTable({
                "lengthChange": false,
                "tabIndex": -1,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },
                columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    },
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

        function setMatBack(){
            var getCount = $("#tbMaterial tr").length;
            $("#back-item-material").val(getCount);
            var payload = '';
            
            $("#tbMaterial").find("tr").each(function (index, element) {    
                var material = $("#"+(index+1)+"-name").text();
                var id = $("#"+(index+1)+"-id").text();
                var quant = $("#temp-quant-"+(index+1)).val();
                var price = $("#"+(index+1)+"-price").text();
                var total = quant * price;
                payload += '<tr id="back-'+(index+1)+'">'+
                                '<td><input name="idBack'+(index+1)+'" id="idBack'+(index+1)+'" type="text" value="'+id+'"></td>'+
                                '<td><input name="materialBack'+(index+1)+'" id="materialBack'+(index+1)+'" type="text" value="'+material+'"></td>'+
                                '<td><input name="quantBack'+(index+1)+'" id="quantBack'+(index+1)+'" type="text" value="'+quant+'"></td>'+
                                '<td><input name="priceBack'+(index+1)+'" id="priceBack'+(index+1)+'" type="text" value="'+price+'"></td>'+
                                '<td><input name="totalBack'+(index+1)+'" id="totalBack'+(index+1)+'" type="text" value="'+total+'"></td>'+
                            '</tr>';
                
                $("#back-material").html(payload);
            });
        }

        function addPembayaran() {
            var getMethod = $("#paymentMethod option:selected").text();
            if (getMethod == "Cash") {
                var payload = '<p class="c-text-2 mt-4 soft-title medium-weight">Jumlah Uang Pembelian</p>'+
                    '<input id="amountPembayaran" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" type="number">';
                $("#spacePembayaran").html(payload);
            }
        }

        function checkout(){
            
            var f = document.getElementById("paymentMethod");
            var data2 = f.options[f.selectedIndex].value;
            var g = document.getElementById("sellInteract");
            var data3 = g.options[g.selectedIndex].value;
            var getMethodPay = $("#paymentMethod option:selected").text();
            var getAlamat = $("#deliveryAddress").val();
            var getTotalPembayaran = "";
            
            if (getMethodPay === "Cash") {
                var getPembayaran = $("#amountPembayaran").val();
                getTotalPembayaran = getPembayaran;
            }else if(getMethodPay === "Credit"){
                var getPembayaran = $("#paid-amount").val();
                getTotalPembayaran = getPembayaran;
            }else if (getMethodPay === "ATM") {
                var getPembayaran = $("#amountPembayaran").val();
                getTotalPembayaran = getPembayaran;
            }else if (getMethodPay === "Saldo Pelanggan") {
                var getPembayaran = $("#amountPembayaran").val();
                getTotalPembayaran = getPembayaran;
            }
            
            var setNew = numeral(getTotalPembayaran).format('0,0');
            var setFin = setNew.replace(',','.');
            var getKembalian = getTotalPembayaran - getTotalPay();
            
            if (data2 == "Cash") {
                var setPembayaran = '<td></td>'+
                    '<td colspan="2">Total Uang Pembayaran: </td>'+
                    '<td></td>'+
                    '<td colspan="2">'+"Rp. "+setFin+'</td>';
            
                if (getKembalian > 0) {
                    var setKembalian = '<td></td>'+
                        '<td colspan="2">Total Uang Kembalian: </td>'+
                        '<td></td>'+
                        '<td colspan="2">'+"Rp. "+getKembalian.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</td>';
                    $("#setTotalKembalian").html(setKembalian);
                }
                $("#setTotalPayment").html(setPembayaran);
            }else if (data2 == "Credit") {
                let deadline = $("#tanggalBack").val();
                console.log("tanggal back "+deadline);
                let finDeadline = moment(deadline).format('DD/MM/YYYY');
                let saldoAwal = $("#saldoBack").val();
                let totalPembelian = $("#back-amount-material").val();

                var setTerm = '<td></td>'+
                    '<td colspan="1">Term of Payment: </td>'+
                    '<td colspan="1">'+finDeadline+'</td>'+
                    '<td></td>'+
                    '<td></td>';

                if (setFin.length > 0) {
                    var setPembayaran = '<td></td>'+
                    '<td colspan="2">Total Uang Pembayaran: </td>'+
                    '<td></td>'+
                    '<td></td>';
                    $("#setTotalKembalian").html(setKembalian);
                }
            }else if (data2 == "ATM") {
                var skillsSelect = document.getElementById("paymentList");
                var method = skillsSelect.options[skillsSelect.selectedIndex].text;
                var setPembayaran = '<td></td>'+
                    '<td colspan="2">Dikirim ke rekening : </td>'+
                    '<td colspan="3">'+method+'</td>';
                
                $("#setTotalPayment").html(setPembayaran);
                $("#setTotalKembalian").html(setTerm);
            }

            var i;
            var totalKeseluruhan = 0;

            var totalGet = $("#back-material tr").length;
            var totalDis = $("#percentage").val();
            var checkOutTable = '';

            let today = moment().format('DD/MM/YYYY');

            $("#invoice-check").html(today);
            $("#payment-check").html(data2);
            $("#package-check").html(data3);

            if(data3 == "Delivery"){
                var payload0 = '<td><b>Package Price</b></td>'+
                                '<td><b>:</b></td>'+
                                '<td><b id="package-price-check"></b></td>';

                var payload1 = '<td><b>Alamat Pengiriman</b></td>'+
                                '<td><b>:</b></td>'+
                                '<td><b id="buyerAddress-check"></b></td>';

                $("#alamatPrint").html(payload1);
                $("#ongkirPrint").html(payload0);

                var price = $("#deliveryPrice").val();
                $("#package-price-check").html("Rp. "+price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            }else{
                $("#package-price-check").html("-");
            }

            for(i = 0; i < totalGet; i++){

                var realCount = i + 1;

                var getTotal = $("#totalBack"+realCount).val();
                var getQuant = $("#quantBack"+realCount).val();
                var getPrice = $("#priceBack"+realCount).val();
                var getMate = $("#materialBack"+realCount).val();
                
                var quantity = parseInt($("#temp-quant-"+realCount).val());

                var amount = getTotalPay();

                checkOutTable += '<tr>'+
                                    '<th>'+realCount+'</th>'+
                                    '<th>'+getMate+'</th>'+
                                    '<th>'+getQuant+'</th>'+
                                    '<th>Rp. '+getPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+',-</th>'+
                                    '<th>Rp. '+getTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+',-</th>'+
                                 '</tr>';
            }

            $("#buyerName-check").html(getCustomerName());
            $("#buyerAddress-check").html(getAlamat);
            $("#check-out-table").html(checkOutTable);
            $("#check-out-total").html(getTotalPay().toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));

        }

        function setBackCicilan() {
            //moment js set local region indo
            moment.locale('id');

            let getDeadline = $("#deadline-cicilan").val();
            let getWaktu = $("#waktu-cicilan option:selected").val();
            let getSaldo = $("#saldo-cicilan").val();
            let getTanggal = "";

            if (getWaktu == "Hari") {
                getTanggal = moment().add(getDeadline,'days').format('YYYY-MM-DD');
            } else if(getWaktu == "Minggu") {
                getTanggal = moment().add(getDeadline,'weeks').format('YYYY-MM-DD');
            }else{
                getTanggal = moment().add(getDeadline,'months').format('YYYY-MM-DD');
            }

            if (getSaldo.length >= 0) {
                $("#paid-amount").val(getSaldo);
            }

            $("#saldoBack").val(getSaldo);
            $("#paid-amount").val(getSaldo);
            $("#tanggalBack").val(getTanggal);
            $("#textBack").val(getDeadline+" "+getWaktu);
            $("#addCicilan").modal("hide");
        }

        function getMemberStat() {
            if (getMembership() == 1) {
                $("#discountForm").val("0");
            }else{
                $("#discountForm").val("-");
            }
        }

        function setBackTable(){
            var getMethod = $("#paymentMethod option:selected").text();
            var getpembelian = $("#sellInteract option:selected").val();
            
            var getState = $("#stateBuy").val();
            var info = $("#info-trans").val();
            var getPaid = $("#amountPembayaran").val();

            $("#method-custom").val(getMethod);
            $("#sales").val(getpembelian);
            $("#info-custom").val(info);

            if (getState === "db") {
                var getName = $("#buyerName option:selected").text();
                $("#back-customer").val(getName);
            }else{
                var getName = $("#buyerName").val();
                $("#back-customer").val(getName);
            }
            
            if (getMethod === "Cash") {
                $("#paid-amount").val(getPaid);
            }else if (getMethod === "ATM" || getMethod === "Saldo Pelanggan") {
                var getJum = $("#back-amount-material").val();
                $("#paid-amount").val(getJum);
            }else if(getMethod === "Credit"){

            }else{
                $("#paid-amount").val("0");
            }

            if (getMethod === "ATM") {
                var getBank = $("#paymentList").val();
                var getRek = $("#rekening").val();

                $("#rekening-no").val(getRek);
                $("#bank-dest").val(getBank);
            }

            if (getpembelian === "Delivery") {
                var getAddress = $("#deliveryAddress").val();
                var getOngkir = $("#deliveryPrice").val();

                $("#back-alamat").val(getAddress);
                $("#ongkir-pay").val(getOngkir);
            }else{
                $("#ongkir-pay").val("0");
            }
        }

        $("#addSubmit").click(function (e) { 
            e.preventDefault();
            //console.log($("#paymentMethod option:selected").text());
            setBackTable();

            if ($("#tbMaterial tr").length < 1) {
                alert("Masukkan material dulu !");
            }else{
                if ($("#paymentMethod option:selected").text() == "Saldo Pelanggan") {
                    if ($("#paymentMethod option:selected").val() < getTotalPay()) {
                        alert("Saldo pelanggan tidak cukup !");
                    }else{
                        totalQuant();
                        checkout();
                        $("#printOut").modal("show");
                    }
                }else{
                    checkout();
                    totalQuant();
                    $("#printOut").modal("show");
                }
            }
        });

        $("#submitCicilan1").click(function (e) { 
            e.preventDefault();
            setBackCicilan();
        });

        $("#switchBuyer").click(function (e) { 
            e.preventDefault();
            if ($("#stateBuy").val() == "db") {
                $("#stateBuy").val("custom");
                $("#state").val("custom");
                $("#tbMaterial").empty();
                var payload = '<input id="buyerName" autocomplete="off" class="col-11 c-text-2 search-fill main-padding-l main-padding-r" type="text">';
                $("#spaceBuyer").html(payload);
            }else{
                $("#stateBuy").val("db");
                $("#state").val("db");
                $("#tbMaterial").empty();
                var payload = '<select name="name" id="buyerName" class="dropdown-select2 c-text-2 search-fill col-11" >'+
                                '<option value="">Select Pelanggan</option>'+
                                <?php
                                    foreach ($customer as $item ) {
                                ?>
                                    '<option value="<?php echo $item->customer_membership.'|'.$item->customer_type.'|'.$item->customer_saldo ?>"><?php echo $item->customer_name ?></option>'+
                                <?php
                                    }
                                ?>
                           ' </select>';
                $("#spaceBuyer").html(payload);
                $('.dropdown-select2').select2();
            }
        });

        $("#btnNewMaterial").click(function (e) { 
            e.preventDefault();
            var getName = $("#buyerName").val();
            if ( getName.length < 1) {
                alert("Pilih nama customer dulu !");
            }else{
                $("#addNewMaterial").modal("show");
                $('#addNewMaterial').on('shown.bs.modal', function() {
                    $('#search-in').focus();
                })
            }
        });

        $("#btnRefresh").click(function (e) { 
            e.preventDefault();
            totalQuant();
            setMatBack();
        });

        $("#buyerName").on("change", function () {
            getMemberStat();
            addSaldo();
            var getName = $("#buyerName option:selected").text();
            var getSaldo = $("#buyerName option:selected").val();
            var finSaldo = getSaldo.split("|");

            $("#tbMaterial").empty();

            $("#back-customer-id").val(finSaldo[4]);
            $("#back-customer").val(getName);
            $("#back-saldo").val(finSaldo[2]);
            $("#back-alamat").val(finSaldo[3]);
        });

        function addSaldo() {
            var getValue = $("#buyerName option:selected").val();
            var splitVal = getValue.split("|");

            var payMet = '<option class="dropdown-item" value="Cash">Cash</option>'+
                        '<option class="dropdown-item" value="Credit">Credit</option>'+
                        '<option class="dropdown-item" value="ATM">ATM</option>'+
                        '<option value="'+splitVal[2]+'" class="dropdown-item">Saldo Pelanggan</option>';
            $("#paymentMethod").html(payMet);
        }

        $("#payment").on("change", function () {
            var payment = $(this).find('option:selected').text()
            if (payment === "Cash") {
                $("#btnCicilan").prop("disabled", true);
            }else{
                $("#btnCicilan").prop("disabled", false);
            }
        });

        $("#sellMoney").on("change", function () {
            var action = $(this).find('option:selected').text()
            var customer = $(this).find('option:selected').text()
            if (action === "Saldo") {
                var form = '<div class="col-12 mt-4 p-0"><p class="c-text-2 soft-title regular-weight">Delivery Price</p>'+
                        '<input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="deliveryPrice"></div>'+
                        '<div class="col-12 mt-4 p-0"><p class="c-text-2 soft-title regular-weight">Delivery Address</p>'+
                        '<input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="deliveryAddress"></div>';

                $("#remainSaldo").html(form);
            }else{
                $("#remainSaldo").html("");
            }
        });

        $("#sellInteract").on("change", function () {
            var action = $(this).find('option:selected').text()
            if (action === "Delivery") {
                var form = '<div class="col-12 mt-4 p-0"><p class="c-text-2 soft-title regular-weight">Delivery Price</p>'+
                           '<input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="deliveryPrice" value="0"></div>'+
                           '<div class="col-12 mt-4 p-0"><p class="c-text-2 soft-title regular-weight">Delivery Address</p>'+
                           '<input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="deliveryAddress"></div>';
                $("#increasePrice").html(form);
                totalQuant();
            }else{
                $("#increasePrice").html("");
            }
        });

        $("#paymentMethod").click(function (e) { 
            e.preventDefault();
            var getName = $("#buyerName").val();
            if (getName.length < 1) {
                alert("Masukkan nama pelanggan !");
            }
        });

        $("#paymentMethod").change(function (e) { 
            e.preventDefault();
            var action = $(this).find('option:selected').text();        
            
            if (action === "ATM") {
                var form = '<div class="col-12 mt-4 p-0">'+
                               '<p class="c-text-2 soft-title medium-weight">Dikirim ke Rekening</p>'+
                               '<select name="get-rek-id" class="c-dropdown col-12 c-text-2" id="paymentList">'+
                                   '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>'+
                                    <?php
                                        foreach ($rekening as $item ) {
                                    ?>
                                        '<option value="<?php echo $item->id_rekening ?>"><?php echo $item->bank_rekening ?> - <?php echo $item->nomor_rekening ?></option>'+
                                    <?php
                                        }
                                    ?>
                               '</select>'+
                           '</div>'+
                           '<input type="hidden" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="bank">'+
                           '<input type="hidden" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="rekening">';

                $("#rekeningAtm").html(form);
                $("#spacePembayaran").html("");
            }else if(action === "Cash"){
                var payload = '<p class="c-text-2 mt-4 soft-title medium-weight">Jumlah Uang Pembelian</p>'+
                    '<input id="amountPembayaran" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" type="number">';
                $("#spacePembayaran").html(payload);
                $("#rekeningAtm").html("");
            }
            else{
                var payload = '';
                $("#spacePembayaran").html(payload);
                $("#rekeningAtm").html("");
            }
        });

        $("#paymentList").change(function (e) { 
            e.preventDefault();
            var action = $(this).find('option:selected').text();        
            $("#bank").val(action);
        });

        $("#addMaterial").click(function (e) { 
            e.preventDefault();
            addNewMate()
        });

        $("#show_again").click(function (e) { 
            e.preventDefault();
            showModMate();
        });

        $("#btnCicilan").click(function (e) { 
            e.preventDefault();
            
            let payment1 = $("#paymentMethod option:selected").val();
            if (payment1 == "Credit") {
                $("#addCicilan").modal("show");
            }else{
                alert('Hanya opsi kredit menampilkan form cicilan');
            }
            
        });

        $("#paymentCount").on("input", function () {
            var getCount = $(this).val();
            var payload = '';
            console.log(getCount);
            for (let i = 0; i < getCount; i++) {
                const element = getCount;

                payload += '<p id="cicilan-number-'+(i+1)+'" class="c-text-2 soft-title mt-3 regular-weight">Payment '+(i + 1)+'</p>' +
                           '<input type="date" id="cicilan'+(i + 1)+'" class="field-cicilan col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">';
                $("#countPay").html(payload);
            }
        });

        $("#satuan").change(function (e) { 
            e.preventDefault();
            let tipe = $(this).val();
            if (tipe === "atas") {
                $("#editRatio").prop('readonly',false);
            } else {
                //$("#editIsiMaterial").val("1");
                $("#editRatio").prop('readonly',true);
            }
        });

    });

    function addNewMate() {

        var getMate = $("#editName").val();
        var getId = $("#editId").val();
        var getQuant = $("#editQuant").val();
        let getPrice = $("#editPrice").val();
        let priceGrosir = $("#editGrosir").val();
        let minGrosir = $("#editMin").val();
        var getPriceDaily = $("#editPriceDaily").val();
        let ratio = $("#satuan option:selected").val();
        let ratioVal = $("#editRatio").val();

        if (getQuant < 1) {
            getQuant = 1;
        }
        
        if (getPriceDaily.length > 0) {
            console.log("dai "+getPriceDaily);
            getPrice = getPriceDaily;
        }

        var getQuant = getQuant * ratio;

        if (getQuant >= minGrosir) {
            getPrice = priceGrosir;
        }

        console.log("harga "+getPrice);
        console.log("getPriceDaily "+getPriceDaily);
        console.log("ratio "+ratioVal);
        console.log("quant "+getQuant);

        var getSumTot = parseInt(getQuant)*parseInt(getPrice);

        var getTotal = parseInt($("#payment").val());
        var getPercent = parseInt($("#percentage").val());
        
        var total = getTotal + getSumTot;
        var persen = getPercent / 100 * total;
        var result = total - (getPercent / 100 * total);

        var getRow = $("#tbMaterial tr").length;
        var createID = "add"+"-"+(getRow);
        var table = document.getElementById("mainMaterialTable");
        var tbodyRowCount = (getRow+1);

        var payload = '<tr id="'+createID+'" class="t-item">'+
                               '<td class="p-2 c-text-2 text-center d-none" id="'+tbodyRowCount+'-id">'+getId+'</td>'+
                               '<td class="p-2 c-text-2 text-center" id="'+tbodyRowCount+'-name">'+getMate+'</td>'+
                               '<td class="p-2 c-text-2 text-center" id="'+tbodyRowCount+'-quant"><input type="text" id="temp-quant-'+tbodyRowCount+'" class="text-center search-fill col-4" placeholder="Quantity..." value="'+getQuant+'"></td>'+
                               '<td class="p-2 c-text-2 text-center" id="'+tbodyRowCount+'-price">'+getPrice+'</td>'+
                               '<td class="p-2 c-text-2 text-center d-none" id="'+tbodyRowCount+'-total">'+getSumTot+'</td>'+
                               '<td class="p-2 c-text-2 text-center ">'+
                                   '<a onclick="deleteItem(\''+createID+'\',\'temp-quant-'+createID+'\')">'+
                                       '<button class="ml-2 basic-btn c-soft-background" >'+
                                           '<i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>'+
                                       '</button>'+
                                   '</a >'+
                               '</td>'+
                           '</tr>';
        $("#tbMaterial").append(payload);
        setBackMaterial(getMate, getQuant, getPrice, getSumTot, getId);

        $("#payment").val(total);
        $("#result").val(result);
        $("#editQuant").val("");
        $("#editRatio").val("");

        $("#materialCountVal").val(tbodyRowCount);
        //func hitung total harga & diskon
        totalQuant();
    }

    function setBackMaterial(material, quant, price, total, id) {
        var getAmount = $("#tbMaterial tr").length;
        var payload = '';
        
        for (let o = 0; o < getAmount; o++) {
            payload = '<tr id="back-'+(o+1)+'">'+
                            '<td><input name="idBack'+(o+1)+'" id="idBack'+(o+1)+'" type="text" value="'+id+'"></td>'+
                            '<td><input name="materialBack'+(o+1)+'" id="materialBack'+(o+1)+'" type="text" value="'+material+'"></td>'+
                            '<td><input name="quantBack'+(o+1)+'" id="quantBack'+(o+1)+'" type="text" value="'+quant+'"></td>'+
                            '<td><input name="priceBack'+(o+1)+'" id="priceBack'+(o+1)+'" type="text" value="'+price+'"></td>'+
                            '<td><input name="totalBack'+(o+1)+'" id="totalBack'+(o+1)+'" type="text" value="'+total+'"></td>'+
                        '</tr>';
        }
        $("#back-item-material").val(getAmount);
        $("#back-material").append(payload);
    }

    //func hitung total harga & diskon
    function totalQuant() {
        var total = 0;
        var getDeli = $("#sellInteract option:selected").text();
        var getOngkir = 0;
        var getDis = $("#discountForm").val();
        var splitDis = getDis.split("%");

        $("#tbMaterial").find("tr").each(function (index, element) {
            // element == this
            var quantId = "#temp-quant-"+(index+1);
            var totalId = "#"+(index+1)+"-price";
            var getQuant = $(quantId).val();
            var getPrice = $(totalId).text();

            var semi = parseInt(getQuant) * parseInt(getPrice);
            total += semi;

        });

        if (getDeli == "Delivery") {
            getOngkir = $("#deliveryPrice").val();
            console.log(getOngkir);
        }

        total = total + parseInt(getOngkir);
        var totalPoin = 0.1/100 * total;
        var gim =  "Rp "+total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        var poin =  totalPoin.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

        if (getMembership() == 0) {
            poin = "-";
        }
        
        $("#discountForm").val(poin);
        $("#back-member").val(totalPoin);
        $("#back-amount-material").val(total);
        $("#countTotal").val(gim);
    }

    function getMembership() {
        var buyerType = $("#stateBuy").val();
        var getFin;
        if (buyerType == "db") {
            var getValue = $("#buyerName option:selected").val();
            var splitVal = getValue.split("|");
            getFin = splitVal[0];
        }else{
            getFin = "0";
        }
        return getFin;
    }

    function getCustomerName() {
        var buyerType = $("#stateBuy").val();
        var getFin;
        if (buyerType == "db") {
            getFin = $("#buyerName option:selected").text();
        }else{
            getFin = $("#buyerName").val();
        }
        return getFin;
    }

    //func get total harga
    function getTotalPay() {
        var total = 0;
        var getDeli = $("#sellInteract option:selected").text();
        var getOngkir = 0;
        var getDis = $("#discountForm").val();

        $("#tbMaterial").find("tr").each(function (index, element) {
            // element == this
            var quantId = "#temp-quant-"+(index+1);
            var totalId = "#"+(index+1)+"-price";
            var getQuant = $(quantId).val();
            var getPrice = $(totalId).text();
            
            //console.log(getQuant+" "+getPrice);
            var semi = parseInt(getQuant) * parseInt(getPrice);
            total += semi;
        });

        if (getDeli == "Delivery") {
            getOngkir = $("#deliveryPrice").val();
        }
        //console.log("ongkir: "+getOngkir);
        var total1 = total + parseInt(getOngkir);
        //console.log("total: "+total1);
        total = total + parseInt(getOngkir);
        
        
        return total;

    }

    function editItem(id) {
        $("#editMaterial").modal("show");
        var idQuant = "#"+id+"-quant";
        var idName = "#"+id+"-name";
        var getAmount = $(idQuant).text();
        var getName = $(idName).text();
        console.log(getAmount);
        console.log(getName);
        
        $("#editQuant").val(getAmount);
        $("#editName").val(getName).change();

        $("#editMerkNew").click(function (e) { 
            e.preventDefault();
            var newQuant = $("#editQuant").val();
            var newName = $("#editName option:selected").text();

            $(idQuant).html(newQuant);
            $(idName).html(newName);
            $("#editMaterial").modal("hide");
            
        });
    }

    function deleteItem(id,val) {
        var splitId = id.split("-");
        var getBackId = "#back-"+splitId[1];
        var fag = "#"+id;
        $(getBackId).remove();
        $(fag).remove();
        totalQuant();
    }

    function showQuant(matName, eceran, reseller, grosir, id, satuan_bawah, satuan_atas,min_trans, ratio) {
        var setPrice = 0;
        var payload = '';
        var custType;
        var buyerType = $("#stateBuy").val();
        
        if (buyerType == "db") {
            custType = getCustomerType();
        }else{
            custType = "Retail";
        }

        console.log("cust type "+custType);

        if (custType == "Retail") {
            setPrice = eceran;
        }else{
            setPrice = reseller
        }

        let satuanOps = 
            "<option value='1'>"+satuan_bawah+" (satuan bawah)</option>"+
            "<option value='"+ratio+"'>"+satuan_atas+" (satuan atas)</option>";
        
        $("#satuan").html(satuanOps);

        $("#editId").val(id);
        $("#editGrosir").val(grosir);
        $("#editName").val(matName);
        $("#editMin").val(min_trans);
        $("#editPrice").val(setPrice);

        $("#modQuantMaterial").modal("show");
        $("#addNewMaterial").modal("hide");
    }

    function showModMate() {
        $("#modQuantMaterial").modal("hide");
        $("#addNewMaterial").modal("show");
    }

    function getCustomerType() {
        var getValue = $("#buyerName option:selected").val();
        var splitVal = getValue.split("|");
        return splitVal[1];
    }

    function getCustomerSaldo() {
        var getValue = $("#buyerName option:selected").val();
        var splitVal = getValue.split("|");
        return splitVal[2];
    }

</script>
