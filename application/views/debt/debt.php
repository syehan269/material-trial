    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-5">
            <input autocomplete="off" class="search-fill col-6 border-0" type="search" name="search" id="search" placeholder="Cari Pemasok">
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>
        
    <div class="d-inline-flex col-12 p-0">
        
        <div class="ml-auto d-inline-flex">
            <div class="d-inline-flex my-auto">
                <p class="my-auto c-text-2 mr-3 medium-weight">Tanggal</p>
                <input id ="date" type="date" class="my-auto c-text-2 search-fill" placeholder="Date" style="padding: 10px 12px">
            </div>
            <!-- <div class="d-inline-flex my-auto ml-3">
                <p class="my-auto mr-3 c-text-2 medium-weight">Status</p>
                <div class="dropdown c-text-2">
                    <select id ="status" class="c-dropdown" >
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="">All</option>
                        <option class="dropdown-item" value="Finished">Finished</option>
                        <option class="dropdown-item" value="Incomplete">Incomplete</option>
                    </select>
                </div>
            </div> -->
            <div class="d-flex my-auto ml-3">
                <button class="btn-filter c-color-primary" id="filter">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                </button>
            </div>
        </div>
    </div>

    <div class="mt-4 custom-card p-3">
        <table width="100%" id="main-table">
            <thead>
                <tr class="t-header col-12">
                    <td class="p-3 primary-title boldest-weight">No</td>
                    <td class="p-3 primary-title boldest-weight d-none">ID Transaksi</td>
                    <td class="p-3 primary-title boldest-weight">Nama Pemasok</td>
                    <td class="p-3 primary-title boldest-weight" >Tanggal Transaksi</td>
                    <td class="p-3 primary-title boldest-weight" >Term of Payment</td>
                    <td class="p-3 primary-title boldest-weight" >Jumlah Tagihan</td>
                    <td class="p-3 primary-title boldest-weight" >Jumlah Terbayar</td>
                    <td class="p-3 primary-title boldest-weight" >Utilitas</td>
                </tr>
            </thead>
            <tbody id="tb_debt">
                <?php
                    $i = 1;
                    foreach ($result as $item ) {
                ?>
                    <tr>
                        <td class="p-3 c-text-2"><?php echo $i++ ."." ?></td>
                        <td class="p-3 c-text-2 d-none"><?php echo $item->trans_in_id ?></td>
                        <td class="p-3 c-text-2"><?php echo $item->trans_in_supplier ?></td>
                        <td class="p-3 c-text-2">
                            <?php echo date_format(date_create($item->trans_in_insert_date), 'd/m/Y') ?>
                        </td>
                        <td class="p-3 c-text-2"><?php echo $item->trans_in_due_date ?></td>
                        <td class="p-3 c-text-2"><?php echo "Rp. ".number_format($item->trans_in_payment_amount ,0,".",".") ?></td>
                        <td class="p-3 c-text-2"><?php echo "Rp. ".number_format($item->trans_in_paid_amount ,0,".",".") ?></td>
                        <td>
                            <button class="ml-2 basic-btn c-color-primary" onclick="addCicilanField('<?php echo $item->trans_in_id ?>')">
                                <i class="bx bxs-pencil text-white " style="margin-top: 5px"></i>
                            </button>
                        </td>
                    </tr>
                <?php
                    }
                ?>
            </tbody>
            <tfoot>
                <tr class="t-header col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight text-center" colspan="5">
                        Total Tagihan
                    </td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" colspan="1">
                        Rp <?php echo number_format($paid,0,',','.') ?>
                    </td>
                    <td class="p-3 primary-title c-text-2 boldest-weight">
                        
                    </td>
                </tr>
                <tr class="t-header col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight text-center" colspan="5">
                        Total Belum Terbayar
                    </td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" colspan="1">
                        Rp <?php echo number_format($unpaid,0,',','.') ?>
                    </td>
                    <td class="p-3 primary-title c-text-2 boldest-weight">
                        
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class="modal fade" id="editCicilan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Edit Tagihan</p>
            </div>
            <form action="<?php echo base_url("index.php/c_debt/update_cicilan") ?>" method="post">
                <div class="modal-body c-main-background">
                    <div id="spaceEditSisa" class="">
                        <input class="search-fill col-12 border-0 d-none" type="number" name="cicilan-total" id="cicilan-total" placeholder="jumlah cicilan" value="0">
                        <input class="search-fill col-12 border-0 mt-3 d-none" type="text" name="id-transaksi" id="id-transaksi" placeholder="id transaksi">
                    </div>
                    <input id="amountCicilan" name="amountCicilan" type="text" placeholder="amountID" class="d-none col-12 c-text-2 mt-2 search-fill main-padding-l main-padding-r">
                    <input id="cicilanId" name="cicilanId" type="text" placeholder="amountID" class="d-none col-12 c-text-2 mt-2 search-fill main-padding-l main-padding-r mb-3">
                    <div class="col-12 pr-0 pl-3 pb-3" id="countPay">
                       
                    </div>
                </div>
                <div class="modal-footer c-main-background border-0">
                    <button type="button" id="add-cicilan" class="btn-add col-4 c-color-white primary-title c-border-primary medium-weight mr-auto">Tambah Cicilan</button>
                    <button type="button" class="btn-modal-negative mr-3 medium-weight" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn-modal-positive medium-weight">Konfirmasi</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var table = "";

        $("#search").on("input", function () {
            search(2, this.value);
        });

        $("#date").on("input change", function () {
            var getDate = this.value;
            var split1 = getDate.split("-");
            var finDate = split1[1]+"/"+split1[2]+"/"+split1[0];
            search(3, finDate);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#main-table').DataTable()
                .search('').columns()
                .search('').draw();
            $("#date").val("");
            $("#search").val("");
        });

        //reset view setiap hide modal edit cicilan
        $('#editCicilan').on('hidden.bs.modal', function (e) {
            $("#countPay").html("");
        })

        dataTable();

        function dataTable(){
            table = $('#main-table').DataTable({
                stateSave: true,
                "lengthChange": false,
                //"pageLength": 50,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

        $("#add-cicilan").click(function (e) { 
            e.preventDefault();
            let count = $("#cicilan-total").val();
            count = parseInt(count) + 1;
            payloadCicilan =  '<div id="cicilan-container-'+count+'">'+
                                '<p class="c-text-2 soft-title regular-weight mt-3">Cicilan ke-'+count+'</p>'+
                                '<div class="d-inline-flex col-12 px-0">'+
                                    '<input name="cicilan-'+count+'" autocomplete="off" type="number" id="cicilan-'+count+'" class="col-10 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Jumlah Pembayaran Cicilan ke-'+count+'">'+
                                    '<button id="btn-delete-'+count+'" type="button" onclick="deleteCicilan('+count+')" class="basic-btn c-color-primary my-auto ml-4" style="width: 30px; height: 30px;">'+
                                        '<i class="bx bxs-trash text-white " style="margin-top: 5px"></i>'+
                                    '</button>'+
                                '</div>'+
                                '<input name="tanggal-'+count+'" autocomplete="off" type="text" id="tanggal-'+count+'" class="col-12 c-text-2 d-none mt-2 search-fill main-padding-l main-padding-r" placeholder="Tanggal Cicilan ke-'+count+'">'+
                            '</div>';
            $("#cicilan-total").val(count);
            $("#countPay").append(payloadCicilan);
        });

    });

    function addCicilanField(id) {        

        $("#id-transaksi").val(id);

        $.ajax({
              type  : 'ajax',
              url   : '<?php echo base_url()?>index.php/c_debt/getCicilan/'+id,
              async : true,
              crossDomain: true,
              dataType : 'json',
              success : function(data){

                if (Array.isArray(data.data) && data.data.length) {
                    $("#countPay").html("");

                    for (let i = 0; i < data.data.length; i++) {
                        const cicilan = data.data[i];    
                        $("#cicilan-total").val(data.data.length);
                    
                        payloadCicilan =  '<div id="cicilan-container-'+(i+1)+'">'+
                                            '<p class="c-text-2 soft-title regular-weight mt-3">Cicilan ke-'+(i+1)+'</p>'+
                                            '<div class="d-inline-flex col-12 px-0">'+
                                                '<input name="cicilan-'+(i+1)+'" autocomplete="off" type="number" id="cicilan-'+(i+1)+'" class="col-10 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Jumlah Pembayaran Cicilan ke-'+(i+1)+'">'+
                                                '<button id="btn-delete-'+(i+1)+'" type="button" onclick="deleteCicilan('+(i+1)+')" class="basic-btn c-color-primary my-auto ml-4" style="width: 30px; height: 30px;">'+
                                                    '<i class="bx bxs-trash text-white " style="margin-top: 5px"></i>'+
                                                '</button>'+
                                            '</div>'+
                                            '<input name="tanggal-'+(i+1)+'" autocomplete="off" type="text" id="tanggal-'+(i+1)+'" class="col-12 c-text-2 mt-2 search-fill main-padding-l main-padding-r d-none" placeholder="Tanggal Cicilan ke-'+(i+1)+'">'+
                                        '</div>';
                        $("#countPay").append(payloadCicilan);
                        $("#cicilan-"+(i+1)).val(cicilan.utang_amount);
                        $("#tanggal-"+(i+1)).val(cicilan.utang_paid_date);

                    }
                }else{
                    console.log("data empty");
                    $("#cicilan-total").val(1);
                    
                    payloadCicilan =  '<div id="cicilan-container-1">'+
                                        '<p class="c-text-2 soft-title regular-weight mt-3">Cicilan ke-1</p>'+
                                        '<div class="d-inline-flex col-12 px-0">'+
                                            '<input name="cicilan-1" autocomplete="off" type="number" id="cicilan-1" class="col-10 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Jumlah Pembayaran Cicilan ke-1">'+
                                            '<button id="btn-delete-1" type="button" onclick="deleteCicilan(1)" class="basic-btn c-color-primary my-auto ml-4" style="width: 30px; height: 30px;">'+
                                                '<i class="bx bxs-trash text-white " style="margin-top: 5px"></i>'+
                                            '</button>'+
                                        '</div>'+
                                        '<input name="tanggal-1" autocomplete="off" type="text" id="tanggal-1" class="col-12 c-text-2 mt-2 search-fill main-padding-l main-padding-r d-none" placeholder="Tanggal Cicilan ke-1">'+
                                    '</div>';
                    $("#countPay").html(payloadCicilan);

                }
                
                showEdit();
              }   

        }); 

    }

    function deleteCicilan(id) {
        let count = $("#cicilan-total").val();
        count = parseInt(count)-1;
        $("#cicilan-container-"+id).remove();
        $("#cicilan-total").val(count);
    }

    function showEdit() {
        $("#editCicilan").modal("show");
    }

    function showPreview() {
        $("#prevIns").modal("show");
    }
</script>