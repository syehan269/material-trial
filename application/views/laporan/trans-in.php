    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-5">
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>
        
        <form class="d-inline-flex col-12 p-0" method="post" action="<?php echo base_url('index.php/laporan-masuk/search') ?>">
            <div class="d-inline-flex col-5 px-0 my-auto">
                <input name="nama-supplier" type="text" autocomplete="off" class="my-auto col-12 c-text-2 search-fill" placeholder="Nama Supplier" style="padding: 10px 12px">
            </div>
            <div class="ml-auto d-inline-flex">
                <div class="d-inline-flex my-auto">
                    <input name="tanggal-awal" type="date" class="my-auto c-text-2 search-fill" placeholder="Date" style="padding: 10px 12px">
                </div>
                <div class="d-inline-flex my-auto ml-3">
                    <p class="my-auto c-text-2 mr-3 medium-weight">Sampai</p>
                    <input name="tanggal-akhir" type="date" class="my-auto c-text-2 search-fill" placeholder="Date" style="padding: 10px 12px">
                </div>
                <div class="d-flex my-auto ml-3">
                    <button class="btn-filter c-color-primary" type="submit">
                        <i class='bx bx-search-alt-2 bx-sm align-middle' style='color:#ffffff' ></i>
                    </button>
                </div>
            </div>
        </form>

        <div class="mt-4 custom-card p-3">
            <table width="100%" id="income-table">
                <thead>
                    <tr class="t-header col-12">
                        <td class="p-3 primary-title c-text-2 boldest-weight">No</td>
                        <td class="p-3 primary-title c-text-2 boldest-weight" >Nama Pemasok</td>
                        <td class="p-3 primary-title c-text-2 boldest-weight" >Tipe Pembayaran</td>
                        <td class="p-3 primary-title c-text-2 boldest-weight" >Tanggal</td>
                        <td class="p-3 primary-title c-text-2 boldest-weight" >Jumlah Pembayaran</td>
                    </tr>
                </thead>
                <tbody id="tb_income">
                    <?php
                        $i = 1;
                        foreach ($result as $item ) {
                    ?>
                    <tr>
                        <td class="p-3 c-text-2"><?php echo $i++?></td>
                        <td class="p-3 c-text-2"><?php echo $item->trans_in_supplier?></td>
                        <?php if($item->trans_in_payment_type == "Cash"): ?>
                            <td class="p-3 c-text-2"><span class="cash-status"><?php echo $item->trans_in_payment_type ?></span></td>
                        <?php else: ?>
                            <td class="p-3 c-text-2"><span class="credit-status"><?php echo $item->trans_in_payment_type ?></span></td>
                        <?php endif; ?>
                        <td class="p-3 c-text-2">
                            <?php echo date_format(date_create($item->trans_in_insert_date), 'd/m/Y') ?>
                        </td>
                        <td class="p-3 c-text-2"><?php echo "Rp. ".number_format($item->trans_in_payment_amount ,0,".",".") ?></td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
                <tfoot>
                    <tr class="t-header col-12">
                        <td class="p-3 primary-title c-text-2 boldest-weight text-center" colspan="4">
                            Total
                        </td>
                        <td class="p-3 primary-title c-text-2 boldest-weight">
                            Rp <?php echo number_format($total,0,',','.') ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

<script>
    $(document).ready(function () {
        var table = "";
        //js select2 dropdown
        $('.dropdown-select2').select2();
        $("#search").on("input", function () {
            search(2, this.value);
        });

        $("#type-drop").on("change", function () {
            var type = $(this).find('option:selected').val();
            search(2, type);
        });

        $("#date").on("input change", function () {
            var getDate = this.value;
            var split1 = getDate.split("-");
            var finDate = split1[2]+"/"+split1[1]+"/"+split1[0];
            search(5, finDate);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#income-table').DataTable()
                .search('').columns()
                .search('').draw();
            $("#date").val("");
            $("#type-drop").val("");
            $("#search").val("");
        });

        //setTable();
        dataTable();

        function dataTable(){
            table = $('#income-table').DataTable({
                stateSave: true,
                "ordering": false,
                "lengthChange": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

    });
</script>
