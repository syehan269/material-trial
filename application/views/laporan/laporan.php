<div class="main-content col-10 ">
    <div class="d-inline-flex col-12 p-0 mb-5">
        <button id="btn-export" class="white-button c-color-primary text-white ml-2" >
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAA30lEQVRIS9WW3RGCQAyEdyvQDrQDKcFSsDLpREuRDuxgnTiHgyfchYN7MK9H8uVnM4GobKwcHz8ASWcAVwDHDLwjecklOAV4ADjkHMN7FjIFkCO4ZW5VmiUhRQCSlNR6IMUAS90DWQXwQFYDYoi1bzzDIkBKBH8HGMv1XdimFQS5NgDuAHZVAGHI+wA5La4gdkgNWJJttS3gx7Iqqg7wSFKStegGoFncohxAkg3ZghtkWxUBqCvTqeo8LXoOmnbchfiTnuTXJZw7md2CqzZAegAtSVu6eZkWZJ10qf5X8QLLKoQZduq01wAAAABJRU5ErkJggg=="/>
            <span class="my-auto">Export ke Excel </span>
        </button>
        <div class="dropdown ml-auto">
            <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                AM
            </button>
            <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
            </div>
        </div>
    </div>

    <div class="col-12 p-0 d-inline-flex">
        <form action="<?php echo base_url('index.php/c_laporan_keuangan/custom_date') ?>" method="post" class="d-inline-flex">
            <div class="d-inline-flex my-auto ml-2">
                <input name="date1" id="date1" type="date" class="my-auto c-text-2 search-fill" placeholder="Date" style="padding: 10px 12px">
            </div>
            <div class="d-inline-flex ml-2 my-auto">
                <p class="my-auto c-text-2 mr-2 medium-weight">Sampai</p>
                <input name="date2" id="date2" type="date" class="my-auto c-text-2 search-fill" placeholder="Date" style="padding: 10px 12px">
            </div>
            <div class="d-flex my-auto ml-3">
                <button class="btn-filter c-color-primary" type="submit" id="filter">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAA+0lEQVRIS+2U21HDMBBFz+2AdJAOIBXE6YRS6IR0AunA7gAqSDpYZj0SY4xtrbGdL/Qr6Z593RUbH22sz/0BZlYDjwszqyUdXONXBmZmC8Xb75Ja7VFAfjAXlgMsAoCdpNscgJk9ANdSBu/AEfBenKKQJP4GPAEXSdVYiTwKh3ijQ5CeeANUObDBMU0fQpAp8cEMcs0jkJL4JMAvpyAR8SJgCJIN1DHkj5r3py60KobGr2PIyXEOAVImrcOzgfqGGvPLP+C7ZKuWyMyegdduT1YDAB/APgk2knz3jJ7ZTU5Kn8CLpHNp284B+OLzRRgSzuAwoBTp4h78FfAFWLCiGWydLB8AAAAASUVORK5CYII="/>
                </button>
            </div>
        </form>
        <div class="ml-auto btn-group" role="group" aria-label="filter-second">
            <button type="button" id="btn-hari" class="btn btn-custom-primary c-text-2 text-white">Hari</button>
            <button type="button" id="btn-minggu" class="btn btn-custom-primary c-text-2 text-white">Minggu</button>
            <button type="button" id="btn-bulan" class="btn btn-custom-primary c-text-2 text-white">Bulan</button>
        </div>
        <?php if($stat == "custom"): ?>
        <div class="d-flex my-auto ml-3">
            <a href="<?php echo base_url('index.php/c_laporan_keuangan') ?>">
                <button class="btn-filter c-soft-background" style="border: solid 2px #5756B3; padding: 4px" id="filter">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAB+UlEQVRIS7WVT07bQBTGvzdNQxdFReoBSk9QOAHOuhmJLmJ717AvUnKC9gZYavc1myrjLIpqdV1zgnIDwgUqEGwCYl71TAwOcpwJgdl5PPN+733vzxCeeNET24czYHv759rKyvM9Y9o7izjlBOh0fm8ost8BbJhEO90pnJh7OOz86lqiPQLW7jynEQgZEe8PBjqri8gJwIQIoFeVhhiZheoPh++Pqv7PBciliUQxgHciUf6t2AO4B8YbBk6ZVasK4gQQiCT5RbMRDRLdLTwt9hj4OAviDKjTOfTTWCBgZGaoW+WzjwK4KeHGkchFCq1y4p0BQZB+BUPkuQThhzF6t+xp6KdfGPhMwH5ZRifAxPinKZkI38qQSSH8BWhkkvZb5z6Qg4GfngN4eS8P/0yiX5f3Aj9l+S4341QEVQdqABcm0auLAYJ0JImyrDbLNb2QRIQTY/R6pUS35UbUN6YdTXl3l2SAED8oyWGYemzxRxI1vrzaPDj4cOoyOaVMm83GscyruWXq+2lGwBaIYtfRXETOwGGSaK+20W7K7TrLhxtRPB5f9WdFUrwRYO4CfGb5mXd/HlX2wRQENAJzZKEOi8v5f1iJsgfw+izjEsnMRhMjRDbK5apZIguz6j14XEviYdFlgiclnLMIJ8TIoBAv/eC4VNFSL9qygP82C/gZd5AA6gAAAABJRU5ErkJggg=="/>
                </button>
            </a>
        </div>
        <?php endif; ?>
    </div>

    <div class="mt-4 custom-card p-3 ml-2">        
        <p class="c-text-4 mt-2 mb-0 c-text-primary medium-weight">Laporan Laba</p>
        <table width="100%" id="pengeluaran-table" class="data-table">
            <thead>
                <tr class="t-header border-0 col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight text-center">Deskripsi</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight text-center" >Jumlah</td>
                </tr>
            </thead>
            <tbody id="trans-table">
                <tr role="row">
                    <td class="p-3 c-text-2">Jumlah Laba Transaksi</td>
                    <td class="p-3 c-text-2 text-center"><?php echo "Rp ".number_format(intval($sum_out - $sum_in) ,0,".",".")?></td>
                </tr>
                <tr role="row" class="even">
                    <td class="p-3 c-text-2">Jumlah Pengeluaran</td>
                    <td class="p-3 c-text-2 text-center"><?php echo "Rp ".number_format($totalPengeluaran[0]->pengeluaran_amount ,0,".",".")?></td>
                </tr>
            </tbody>
            <tfoot >
                <tr class="c-table-footer">
                    <td class="p-3 c-text-2 text-center font-weight-bold">Total laba</td>
                    <td class="p-3 c-text-2 font-weight-bold text-center"><?php echo "Rp ".number_format(intval($sum_out-$sum_in-$totalPengeluaran[0]->pengeluaran_amount) ,0,".",".")?></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="mt-4 custom-card p-3 ml-2">        
        <p class="c-text-4 mt-2 mb-0 c-text-primary medium-weight">Laporan Transaksi</p>
        <table width="100%" id="pengeluaran-table" class="data-table">
            <thead>
                <tr class="t-header border-0 col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight text-center">Deskripsi</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight text-center" >Jumlah</td>
                </tr>
            </thead>
            <tbody id="trans-table">
                <tr role="row">
                    <td class="p-3 c-text-2">Jumlah Transaksi Keluar</td>
                    <td class="p-3 c-text-2 text-center"><?php echo "Rp ".number_format($sum_out ,0,".",".")?></td>
                </tr>
                <tr role="row" class="even">
                    <td class="p-3 c-text-2">Jumlah Transaksi Masuk</td>
                    <td class="p-3 c-text-2 text-center"><?php echo "Rp ".number_format($sum_in ,0,".",".")?></td>
                </tr>
            </tbody>
            <tfoot >
                <tr class="c-table-footer">
                    <td class="p-3 c-text-2 text-center font-weight-bold">Total Transaksi</td>
                    <td class="p-3 c-text-2 font-weight-bold text-center"><?php echo "Rp ".number_format(intval($sum_out-$sum_in) ,0,".",".")?></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="mt-4 custom-card p-3 ml-2">        
        <p class="c-text-4 mt-2 mb-0 c-text-primary medium-weight">Laporan Pengeluaran</p>
        <table width="100%" id="pengeluaran-table" class="data-table">
            <thead>
                <tr class="t-header border-0 col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight text-center">Deskripsi</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight text-center" >Jumlah</td>
                </tr>
            </thead>
            <tbody id="trans-table">
                <?php foreach ($pengeluaran as $key ) {?>
                    <tr role="row">
                        <td class="p-3 c-text-2"><?php echo $key->pengeluaran_nama ?></td>
                        <td class="p-3 c-text-2 text-center"><?php echo "Rp ".number_format($key->pengeluaran_amount ,0,".",".")?></td>
                    </tr>
                <?php } ?>
            </tbody>
            <tfoot class="c-table-footer">
                <tr>
                    <td class="p-3 c-text-2 text-center font-weight-bold">Total Pengeluaran</td>
                    <td class="p-3 c-text-2 font-weight-bold text-center"><?php echo "Rp ".number_format($totalPengeluaran[0]->pengeluaran_amount ,0,".",".")?></td>
                </tr>
            </tfoot>
        </table>
    </div>

</div>
<script>
    $(document).ready(function () {
        moment.locale('id');
        dataTable();
        let dat1 = "k";
        let dat2 = "K";

        $("#btn-hari").click(function (e) { 
            e.preventDefault();
            let date1 = moment().format('YYYY-MM-DD');
            let date =  moment().subtract(1, 'days').format('YYYY-MM-DD');
            dat2 = date1;
            dat1 = date; 
            location.replace('<?php echo base_url('index.php/c_laporan_keuangan/static_report/')?>'+date1+'/'+date)
        });

        $("#btn-minggu").click(function (e) { 
            e.preventDefault();
            console.log(dat2+" "+dat1);
            let date1 = moment().format('YYYY-MM-DD');
            let date = moment().subtract(7, 'days').format('YYYY-MM-DD');
            dat2 = date1;
            dat1 = date;
            location.replace('<?php echo base_url('index.php/c_laporan_keuangan/static_report/')?>'+date1+'/'+date)
        });

        $("#btn-bulan").click(function (e) { 
            e.preventDefault();
            let date1 = moment().format('YYYY-MM-DD');
            let date = moment().subtract(1, 'months').format('YYYY-MM-DD');
            dat2 = date1;
            dat1 = date;
            location.replace('<?php echo base_url('index.php/c_laporan_keuangan/static_report/')?>'+date1+'/'+date)
        });

        $("#btn-export").click(function (e) { 
            e.preventDefault();
            let url = '<?php echo base_url('index.php/Export/exTransIn') ?>';
            let ul = location.pathname.split("/");
            //console.log(ul[5]+" "+ul[6]);
            location.replace(url+'/'+ul[5]+'/'+ul[6]);
        });

        function dataTable(){
            table = $('.data-table').DataTable({
                "lengthChange": false,
                "paging":   false,
                "ordering": false,
                "info":     false
            });
        }
    });
</script>