    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-5">
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>
        
        <div class="col-12 px-0 d-flex justify-content-right">
            <div class="btn-group ml-auto mr-1" role="group" aria-label="filter-second">
                <button type="button" id="btn-hari" class="btn btn-custom-primary c-text-2 text-white">Hari</button>
                <button type="button" id="btn-minggu" class="btn btn-custom-primary c-text-2 text-white">Minggu</button>
                <button type="button" id="btn-bulan" class="btn btn-custom-primary c-text-2 text-white">Bulan</button>
            </div>
        </div>

        <div class="mt-4 custom-card p-3 ml-4">
            <table width="100%" id="income-table">
                <thead>
                    <tr class="t-header col-12">
                        <td class="p-3 primary-title c-text-2 boldest-weight">No</td>
                        <td class="p-3 primary-title c-text-2 boldest-weight" >Tipe</td>
                        <td class="p-3 primary-title c-text-2 boldest-weight" >Nama Pemasok</td>
                        <td class="p-3 primary-title c-text-2 boldest-weight" >Tipe Pembayaran</td>
                        <td class="p-3 primary-title c-text-2 boldest-weight" >Tanggal</td>
                        <td class="p-3 primary-title c-text-2 boldest-weight" >Jumlah Pembayaran</td>
                    </tr>
                </thead>
                <tbody id="tb_income">
                    <?php
                        $i = 1;
                        foreach ($resultIn as $item ) {
                    ?>
                    <tr>
                        <td class="p-3 c-text-2"><?php echo $i++?></td>
                        <td class="p-3 c-text-2"><span class="text-secondary  cash-status">Pembelian</span></td>
                        <td class="p-3 c-text-2"><?php echo $item->trans_in_supplier?></td>
                        <?php if($item->trans_in_payment_type == "Cash"): ?>
                            <td class="p-3 c-text-2"><span class="cash-status"><?php echo $item->trans_in_payment_type ?></span></td>
                        <?php else: ?>
                            <td class="p-3 c-text-2"><span class="credit-status"><?php echo $item->trans_in_payment_type ?></span></td>
                        <?php endif; ?>
                        <td class="p-3 c-text-2">
                            <?php echo date_format(date_create($item->trans_in_insert_date), 'd/m/Y') ?>
                        </td>
                        <td class="p-3 c-text-2"><?php echo "Rp. ".number_format($item->trans_in_payment_amount ,0,".",".") ?></td>
                    </tr>
                    <?php
                        }
                    ?>
                    <?php
                        foreach ($resultOut as $item ) {
                    ?>
                    <tr>
                        <td class="p-3 c-text-2"><?php echo $i++?></td>
                        <td class="p-3 c-text-2"><span class="text-success cash-status">Penjualan</span></td>
                        <td class="p-3 c-text-2"><?php echo $item->trans_out_customer_name?></td>
                        <?php if($item->trans_out_payment_type == "Cash"): ?>
                            <td class="p-3 c-text-2"><span class="cash-status"><?php echo $item->trans_out_payment_type ?></span></td>
                        <?php else: ?>
                            <td class="p-3 c-text-2"><span class="credit-status"><?php echo $item->trans_out_payment_type ?></span></td>
                        <?php endif; ?>
                        <td class="p-3 c-text-2">
                            <?php echo date_format(date_create($item->trans_out_insert_date), 'd/m/Y') ?>
                        </td>
                        <td class="p-3 c-text-2"><?php echo "Rp. ".number_format($item->trans_out_payment_amount ,0,".",".") ?></td>
                    </tr>
                    <?php
                        }
                    ?>
                    <?php
                        foreach ($resultExp as $item ) {
                    ?>
                    <tr>
                        <td class="p-3 c-text-2"><?php echo $i++?></td>
                        <td class="p-3 c-text-2"><span class="text-danger cash-status">Pengeluaran</span></td>
                        <td class="p-3 c-text-2"><?php echo $item->pengeluaran_nama?></td>
                        <td class="p-3 c-text-2">-</td>
                        <td class="p-3 c-text-2">
                            -
                        </td>
                        <td class="p-3 c-text-2"><?php echo "Rp. ".number_format($item->pengeluaran_amount ,0,".",".") ?></td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
                <tfoot>
                    <tr class="t-header col-12">
                        <td class="p-3 primary-title c-text-2 boldest-weight text-center" colspan="4" rowspan="3">
                            Total Pembayaran
                        </td>
                        <td class="p-3 primary-title c-text-2 boldest-weight" colspan="1">
                            Pembelian
                        </td>
                        <td class="p-3 primary-title c-text-2 boldest-weight">
                            Rp <?php echo number_format($sumIn,0,',','.') ?>
                        </td>
                    </tr>
                    <tr class="t-header col-12">
                        <td class="p-3 primary-title c-text-2 boldest-weight" colspan="1">
                            Penjualan
                        </td>
                        <td class="p-3 primary-title c-text-2 boldest-weight">
                            Rp <?php echo number_format($sumOut,0,',','.') ?>
                        </td>
                    </tr>
                    <tr class="t-header col-12">
                        <td class="p-3 primary-title c-text-2 boldest-weight" colspan="1">
                            Pengeluaran
                        </td>
                        <td class="p-3 primary-title c-text-2 boldest-weight">
                            Rp <?php echo number_format($sumExp,0,',','.') ?>
                        </td>
                    </tr>
                    <tr class="t-header col-12">
                    
                        <td class="p-3 primary-title c-text-2 boldest-weight text-center" colspan="4">
                            Total Laba
                        </td>
                        <td class="p-3 primary-title c-text-2 boldest-weight" colspan="1">
                            
                        </td>
                        <td class="p-3 primary-title c-text-2 boldest-weight">
                            Rp <?php echo number_format($laba,0,',','.') ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

<script>
    $(document).ready(function () {
        let table = "";
        moment.locale('id');

        //js select2 dropdown
        $('.dropdown-select2').select2();
        $("#search").on("input", function () {
            search(2, this.value);
        });

        $("#type-drop").on("change", function () {
            let type = $(this).find('option:selected').val();
            search(2, type);
        });

        $("#date").on("input change", function () {
            let getDate = this.value;
            let split1 = getDate.split("-");
            let finDate = split1[2]+"/"+split1[1]+"/"+split1[0];
            search(5, finDate);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#income-table').DataTable()
                .search('').columns()
                .search('').draw();
            $("#date").val("");
            $("#type-drop").val("");
            $("#search").val("");
        });

        $("#btn-hari").click(function (e) { 
            e.preventDefault();
            let akhir = moment().format('YYYY-MM-DD');
            let awal =  moment().subtract(1, 'days').format('YYYY-MM-DD');
            location.replace('<?php echo base_url('index.php/laporan-laba/search/')?>'+awal+'/'+akhir)
        });

        $("#btn-minggu").click(function (e) { 
            e.preventDefault();
            let akhir = moment().format('YYYY-MM-DD');
            let awal = moment().subtract(7, 'days').format('YYYY-MM-DD');
            location.replace('<?php echo base_url('index.php/laporan-laba/search/')?>'+awal+'/'+akhir)
        });

        $("#btn-bulan").click(function (e) { 
            e.preventDefault();
            let akhir = moment().format('YYYY-MM-DD');
            let awal = moment().subtract(1, 'months').format('YYYY-MM-DD');
            location.replace('<?php echo base_url('index.php/laporan-laba/search/')?>'+awal+'/'+akhir)
        });

        //setTable();
        dataTable();

        function dataTable(){
            table = $('#income-table').DataTable({
                stateSave: true,
                "ordering": false,
                "lengthChange": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

    });
</script>
