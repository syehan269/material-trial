<div class="main-content col-10">
    <div class="d-inline-flex col-12 p-0 mb-5">
        <input class="search-fill col-6 border-0" type="search  " name="search" id="search" placeholder="Cari Data (umum)">
        <div class="dropdown ml-auto">
            <button tabindex="-1" class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                AM
            </button>
            <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
            </div>
        </div>
    </div>
        
    <div class="d-inline-flex col-12 p-0">
        <?php if($this->session->userdata("user_level") == "kasir"): ?>
            <a target="_blank" href="<?php echo base_url() ?>index.php/c_income/input">
                <button class="c-text-2 my-auto btn-add c-color-primary text-white medium-weight">
                    Tambah Transaksi Baru
                </button>
            </a>
        <?php else:?>
        <?php endif;?>
        &ensp;
        <a href="<?php echo base_url() ?>index.php/c_income">
            <button class="c-text-2 col-12 my-auto btn-add c-color-primary text-white medium-weight">
                List Transaksi
            </button>
        </a>
        &ensp;
        <a>
            <button class="c-text-2 col-12 my-auto btn-add c-color-default text-white medium-weight">
                Laporan Supplier
            </button>
        </a>
        <div class="ml-auto d-inline-flex">
            <div class="d-inline-flex my-auto">
                <p class="my-auto c-text-2 mr-3 medium-weight">Tanggal</p>
                <input id="date" type="date" class="my-auto c-text-2 search-fill" placeholder="Date" style="padding: 10px 12px">
            </div>
            <div class="d-inline-flex my-auto ml-3 c-text-2">
                <p class="my-auto mr-3 c-text-2 medium-weight">Tipe</p>
                <div class="dropdown c-text-2">
                    <select class="c-dropdown" id="type-drop">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="">Semua</option>
                        <option class="dropdown-item" value="Cash">Tunai</option>
                        <option class="dropdown-item" value="Credit">Kredit</option>
                    </select>
                </div>
            </div>
            <div class="d-flex my-auto ml-3">
                <button class="btn-filter c-color-primary" id="filter">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                </button>
            </div>
        </div>
    </div>

    <div class="mt-4 custom-card p-3">
        <table width="100%" id="income-table">
            <thead>
                <tr class="t-header col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight">No</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Pemasok</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Tipe Pembayaran</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Jumlah Pembayaran</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Detail</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight d-none" >Jumlah Terbayar</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight d-none" >Quantitas</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Tanggal</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Utilitas</td>
                </tr>
            </thead>
            <tbody id="tb_income">
                <?php
                    $i = 1;
                    foreach ($result as $item ) {
                ?>
                <tr>
                    <td class="p-3 c-text-2"><?php echo $i++?></td>
                    <td class="p-3 c-text-2"><?php echo $item->trans_in_supplier?></td>
                    <?php if($item->trans_in_payment_type == "Cash"): ?>
                    <td class="p-3 c-text-2"><span class="cash-status"><?php echo $item->trans_in_payment_type ?></span></td>
                    <?php else: ?>
                    <td class="p-3 c-text-2"><span class="credit-status"><?php echo $item->trans_in_payment_type ?></span></td>
                    <?php endif; ?>
                    <td class="p-3 c-text-2"><?php echo "Rp. ".number_format($item->trans_in_payment_amount ,0,".",".") ?></td>
                    <td class="p-3 c-text-2">
                    <?php
                        foreach ($id_material as $material ) {
                            if($item->trans_in_id == $material->detail_in_trans_in_id){
                                echo $material->detail_in_material_name.', ';
                            }else{}
                        }
                    ?>
                    </td>
                    <?php if($item->trans_in_paid_amount > $item->trans_in_payment_amount): ?>
                    <td class="p-3 c-text-2 d-none"><?php echo "Rp. ".number_format($item->trans_in_payment_amount ,0,".",".") ?></td>
                    <?php else: ?>
                    <td class="p-3 c-text-2 d-none"><?php echo "Rp. ".number_format($item->trans_in_paid_amount ,0,".",".") ?></td>
                    <?php endif; ?>
                    <td class="p-3 c-text-2 d-none"><?php echo "-"?></td>
                    <td class="p-3 c-text-2">
                        <?php echo date_format(date_create($item->trans_in_insert_date), 'd/m/Y') ?>
                    </td>
                    <td>
                        <a href="#" onclick="setPreviewData('<?php echo $item->trans_in_id?>','<?php echo $item->trans_in_supplier?>','<?php echo $item->trans_in_payment_type?>','<?php echo $item->trans_in_payment_amount?>','<?php echo $item->trans_in_additional_info?>','<?php echo $item->trans_in_insert_date?>')">
                            <button class="ml-2 basic-btn c-color-primary" >
                                <i class="bx bx-show text-white" style="margin-top: 5px"></i>
                            </button>
                        </a >
                    </td>
                </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>

</div>
<div class="modal fade" id="prevIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Detail Trasaksi Masuk</p>
            </div>
            <div class="modal-body c-main-background px-4">
                <div class="col-12 p-0 ">
                        <p class="c-text-3 soft-title regular-weight">Nama Supplier</p>
                        <input disabled id="prvName" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                    </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Tipe Pembayaran</p>
                    <input disabled id="prvTrans" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Tanggal </p>
                    <input disabled id="prvDate" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Material Yang Dibeli</p>
                    <div class=" col-12 custom-card p-3">
                        <table class="col-12 p-3" width="100%">
                            <thead class="t-header primary-title">
                                <tr>
                                    <th class="p-3 c-text-3 boldest-weight text-center d-none">No.</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Material</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Kuantitas</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Harga(1)</th>
                                </tr>
                            </thead>
                            <tbody id="tbPrv">
                                
                            </tbody>
                            <tfoot class="t-header primary-title" id="footMat">
                                
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Informasi Transaksi</p>
                    <textarea disabled id="prvInfo" class="search-fill c-card c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b" rows="4" style="min-height: 150px; width: 100%"></textarea>
                </div>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var table = "";
        //js select2 dropdown
        $('.dropdown-select2').select2();
        $("#search").on("input", function () {
            search_spec(this.value);
        });

        $("#type-drop").on("change", function () {
            var type = $(this).find('option:selected').val();
            search(2, type);
        });

        $("#date").on("input change", function () {
            var getDate = this.value;
            var split1 = getDate.split("-");
            var finDate = split1[1]+"/"+split1[2]+"/"+split1[0];
            search(6, finDate);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#income-table').DataTable()
                .search('').columns()
                .search('').draw();
            $("#date").val("");
            $("#type-drop").val("");
            $("#search").val("");
        });

        //setTable();
        dataTable();

        function dataTable(){
            table = $('#income-table').DataTable({
                stateSave: true,
                "lengthChange": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

        function search_spec(getIn) {
            table
                .search(getIn)
                .draw();
        }


    });

    function setPreviewData(id, supplier, method, payment, info, date) {
        var split1 = date.split(" ");
        var split2 = split1[0].split("-");
        var finDate = split2[2]+"-"+split2[1]+"-"+split2[0];

        $("#prvName").val(supplier);
        $("#prvTrans").val(method);
        $("#prvInfo").val(info);
        $("#prvDate").val(finDate);

        $.ajax({
              type  : 'ajax',
              url   : '<?php echo base_url()?>index.php/c_income/get_detail',
              async : false,
              dataType : 'json',
              success : function(data){
                payload = '';
                var tod = 0;

                for(i=0; i<data.data_detail.length; i++){
                    
                    if(data.data_detail[i].detail_in_trans_in_id == id){
                        var no = 1;
                        var context = data.data_detail[i];  
                        var getName = context.detail_in_material_name;  
                        var getAmount = context.detail_in_material_amount;  
                        var getPrice = context.detail_in_material_price;
                        tod+=parseInt(getAmount);
                        
                        payload+= '<tr class="t-item">'+
                                    '<td class="p-3 text-center d-none">'+(no)+'</td>'+
                                    '<td class="p-3 text-center">'+getName+'</td>'+
                                    '<td class="p-3 text-center">'+getAmount+'</td>'+
                                    '<td class="p-3 text-center"> Rp '+getPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</td>'+
                                '</tr>';
                        
                    }else{
                        payload+="";
                    }
                }

                payloadMatFoot = '<tr>'+
                            '<td colspan="1" class="p-3 c-text-3 boldest-weight text-center">Total</td>'+
                            '<td class="p-3 c-text-3 boldest-weight text-center"> '+tod+' item</td>'+
                            '<td class="p-3 c-text-3 boldest-weight text-center"> Rp '+payment.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</td>'+
                        '</tr>';

                $("#tbPrv").html(payload);
                $("#footMat").html(payloadMatFoot);
                show_preview();
                
              }
        }); 
    }

    function show_preview() {
        $("#prevIn").modal("show");
    }
    function delete_item(name, id){
        var getStatus = confirm("Hapus Transaksi oleh "+name+" ?");
        if (getStatus == true) {
            location.href = "<?php echo base_url('index.php/c_income/delete/') ?>"+id;
        }
    }
</script>
