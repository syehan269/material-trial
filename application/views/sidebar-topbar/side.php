<div class="bungkus col-2">
    <nav id="side" class="pl-4 body-nav">
        <div class="sidebar-header d-inline-flex col-12 ml-3 ">
            <h4 class="my-auto c-text-primary">Material.</h4>
            <span class="logo my-auto"><p class="m-1 text-center medium-weight ">M</p></span>
        </div>

        <ul class="list-unstyled component">
            <?php if($site == "dashboard"): ?>
            <li class="d-inline-flex mt-3 subitem1">
                <i class="bx bxs-dashboard bx-md" style='color:#5756B3;'></i>
                <a tabindex="-1" href="<?php echo base_url() ?>index.php/Welcome" class="primary-title my-auto medium-weight">Dashboard</a>
            </li>
            <?php else: ?>            
            <li class="d-inline-flex mt-3 subitem1">
                <i class="bx bxs-dashboard bx-md"></i>
                <a tabindex="-1" href="<?php echo base_url() ?>index.php/Welcome" class="soft-title my-auto medium-weight">Dashboard</a>
            </li>
            <?php endif; ?>
            <?php if($site == "transaction_in"): ?>
            <li class=" mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageSubmenu" data-toggle="collapse" aria-expanded="true" class= "dropdown-toggle medium-weight pb-0 c-text-2" style="display: flex;">
                <i class='bx bxs-dollar-circle  my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Transaksi</p>
                </a>
                <ul class="show list-unstyled" id="pageSubmenu">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-dollar-circle my-auto bx-md-2' style='color:#5756B3; margin-left:20px;'  ></i>
                        <a tabindex="-1" class=" c-text-2 primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_income">Pembelian</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-dollar-circle my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_outcome">Penjualan</a>
                    </li>
                </ul>
            </li>
            <?php elseif($site == "transaction_out"): ?>
            <li class=" mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageSubmenu" data-toggle="collapse" aria-expanded="true" class= "dropdown-toggle medium-weight pb-0 c-text-2" style="display: flex;">
                <i class='bx bxs-dollar-circle  my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Transaksi</p>
                </a>
                <ul class="show list-unstyled" id="pageSubmenu">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-dollar-circle my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class=" c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_income">Pembelian</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-dollar-circle my-auto bx-md-2' style='color:#5756B3; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2  primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_outcome">Penjualan</a>
                    </li>
                </ul>
            </li>
            <?php else: ?>
            <li class=" mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class= "dropdown-toggle medium-weight soft-title c-text-2 pb-0" style="display: flex;">
                <i class='bx bxs-dollar-circle  my-auto bx-md ' style='color:#9999bd; margin-left:-10px;'></i><p class="medium-weight" style="display: initial; margin-bottom:0;">Transaksi</p>
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-dollar-circle my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_income">Pembelian</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-dollar-circle my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_outcome">Penjualan</a>
                    </li>
                </ul>
            </li>
            <?php endif; ?>
            <?php if($site == "utang"): ?>
            <li class=" mt-1" style="display:contents;">
                <a tabindex="-1" href="#pagePerhutang" data-toggle="collapse" aria-expanded="true" class= "dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-wallet-alt  my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Perhutangan</p>
                </a>
                <ul class="show list-unstyled" id="pagePerhutang">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#5756B3; margin-left:20px;'  ></i>
                        <a tabindex="-1" class=" c-text-2 primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_debt">Utang</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_installment">Piutang</a>
                    </li>
                </ul>
            </li>
            <?php elseif($site == "piutang"): ?>
            <li class=" mt-1" style="display:contents;">
                <a tabindex="-1" href="#pagePerhutang" data-toggle="collapse" aria-expanded="true" class= "dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-wallet-alt  my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Perhutangan</p>
                </a>
                <ul class="show list-unstyled" id="pagePerhutang">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class=" c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_debt">Utang</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#5756B3; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2  primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_installment">Piutang</a>
                    </li>
                </ul>
            </li>
            <?php else: ?>
            <li class=" mt-1" style="display:contents;">
                <a tabindex="-1" href="#pagePerhutang" data-toggle="collapse" aria-expanded="false" class= "dropdown-toggle medium-weight soft-title c-text-2" style="display: flex;">
                <i class='bx bxs-wallet-alt  my-auto bx-md ' style='color:#9999bd; margin-left:-10px;'></i><p class="medium-weight" style="display: initial; margin-bottom:0;">Perhutangan</p>
                </a>
                <ul class="collapse list-unstyled" id="pagePerhutang">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_debt">Utang</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_installment">Piutang</a>
                    </li>
                </ul>
            </li>
            <?php endif; ?>
            <?php if($site == "membership"): ?>
            <li class=" mt-1" style="display:contents;">
                <a tabindex="-1" href="#pageMember" data-toggle="collapse" aria-expanded="true" class= "dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-medal  my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Membership</p>
                </a>
                <ul class="show list-unstyled" id="pageMember">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-medal my-auto bx-md-2' style='color:#5756B3; margin-left:20px;'  ></i>
                        <a tabindex="-1" class=" c-text-2 primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_membership">Data Membership</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-medal my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_trans_poin">Transaksi Poin</a>
                    </li>
                </ul>
            </li>
            <?php elseif($site == "trans-poin"):?>
            <li class=" mt-1" style="display:contents;">
                <a tabindex="-1" href="#pageMember" data-toggle="collapse" aria-expanded="true" class= "dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-medal  my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Membership</p>
                </a>
                <ul class="show list-unstyled" id="pageMember">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class=" c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_membership">Data Membership</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#5756B3; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2  primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_trans_poin">Transaksi Poin</a>
                    </li>
                </ul>
            </li>
            <?php else: ?>            
            <li class=" mt-1" style="display:contents;">
                <a tabindex="-1" href="#pageMember" data-toggle="collapse" aria-expanded="false" class= "dropdown-toggle medium-weight soft-title c-text-2" style="display: flex;">
                <i class='bx bxs-medal  my-auto bx-md ' style='color:#9999bd; margin-left:-10px;'></i><p class="medium-weight" style="display: initial; margin-bottom:0;">Membership</p>
                </a>
                <ul class="collapse list-unstyled" id="pageMember">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_membership">Data Membership</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_trans_poin">Transaksi Poin</a>
                    </li>
                </ul>
            </li>
            <?php endif; ?>
            <?php if($this->session->userdata("user_level") == "admin"): ?>
            <hr class="mb-0">
            <?php if($site == "laporan_trans_in"): ?>
            <li class=" mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageLaporan" data-toggle="collapse" aria-expanded="true" class= "dropdown-toggle medium-weight pb-0 c-text-2" style="display: flex;">
                <i class='bx bxs-file-blank  my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Laporan</p>
                </a>
                <ul class="show list-unstyled" id="pageLaporan">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-file-blank my-auto bx-md-2' style='color:#5756B3; margin-left:20px;'  ></i>
                        <a tabindex="-1" class=" c-text-2 primary-title c-text-2" href="<?php echo base_url('index.php/laporan-masuk') ?>">Pembelian</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-file-blank my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url('index.php/laporan-keluar') ?>">Penjualan</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-file-blank my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url('index.php/laporan-laba') ?>">Laba</a>
                    </li>
                </ul>
            </li>
            <?php elseif($site == "laporan_trans_out"): ?>
            <li class=" mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageLaporan" data-toggle="collapse" aria-expanded="true" class= "dropdown-toggle medium-weight pb-0 c-text-2" style="display: flex;">
                <i class='bx bxs-file-blank  my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Laporan</p>
                </a>
                <ul class="show list-unstyled" id="pageLaporan">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-file-blank my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class=" c-text-2 soft-title c-text-2" href="<?php echo base_url('index.php/laporan-masuk') ?>">Pembelian</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-file-blank my-auto bx-md-2' style='color:#5756B3; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2  primary-title c-text-2" href="<?php echo base_url('index.php/laporan-keluar') ?>">Penjualan</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-file-blank my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url('index.php/laporan-laba') ?>">Laba</a>
                    </li>
                </ul>
            </li>
            <?php elseif($site == "laporan_pengeluaran"): ?>
            <li class=" mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageLaporan" data-toggle="collapse" aria-expanded="true" class= "dropdown-toggle medium-weight pb-0 c-text-2" style="display: flex;">
                <i class='bx bxs-file-blank  my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Laporan</p>
                </a>
                <ul class="show list-unstyled" id="pageLaporan">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-file-blank my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class=" c-text-2 soft-title c-text-2" href="<?php echo base_url('index.php/laporan-masuk') ?>">Pembelian</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-file-blank my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url('index.php/laporan-keluar') ?>">Penjualan</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-file-blank my-auto bx-md-2' style='color:#5756B3; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2  primary-title c-text-2" href="<?php echo base_url('index.php/laporan-laba') ?>">Laba</a>
                    </li>
                </ul>
            </li>
            <?php else: ?>
            <li class=" mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageLaporan" data-toggle="collapse" aria-expanded="false" class= "dropdown-toggle medium-weight soft-title c-text-2 pb-0" style="display: flex;">
                <i class='bx bxs-file-blank  my-auto bx-md ' style='color:#9999bd; margin-left:-10px;'></i><p class="medium-weight" style="display: initial; margin-bottom:0;">Laporan</p>
                </a>
                <ul class="collapse list-unstyled" id="pageLaporan">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-file-blank my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url('index.php/laporan-masuk') ?>">Pembelian</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-file-blank my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url('index.php/laporan-keluar') ?>">Penjualan</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-file-blank my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="c-text-2 soft-title c-text-2" href="<?php echo base_url('index.php/laporan-laba') ?>">Laba</a>
                    </li>
                </ul>
            </li>
            <?php endif; ?>
            <hr class="mb-0">
            <?php endif; ?>
            <?php if($site == "customer"): ?>
            <li class="active mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageSubmenu2" data-toggle="collapse" aria-expanded="true" class= "soft-title dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-pie-chart-alt-2 my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Data</p>
                </a>
                <ul class="show list-unstyled" id="pageSubmenu2">
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 primary-title' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_customer">Pelanggan</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_supplier">Pemasok</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_material">Material</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_satuan">Satuan material</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 d-none' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2 d-none" href="<?php echo base_url() ?>index.php/c_tipe">Tipe Pelanggan</a>
                    </li>
                    <?php if($this->session->userdata("user_level") == "admin"): ?>
                        <li class="active" style="display:flex;">
                            <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 soft-title' style=' margin-left:20px;'  ></i>
                            <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_pengeluaran">Pengeluaran</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php elseif($site == "supplier"): ?>
            <li class="active mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageSubmenu2" data-toggle="collapse" aria-expanded="true" class= "soft-title dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-pie-chart-alt-2 my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Data</p>
                </a>
                <ul class="show list-unstyled" id="pageSubmenu2">
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_customer">Pelanggan</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 primary-title' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_supplier">Pemasok</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 soft-title' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_material">Material</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_satuan">Satuan material</a>
                    </li>   
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 d-none' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2 d-none" href="<?php echo base_url() ?>index.php/c_tipe">Tipe Pelanggan</a>
                    </li>
                    <?php if($this->session->userdata("user_level") == "admin"): ?>
                        <li class="active" style="display:flex;">
                            <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 soft-title' style=' margin-left:20px;'  ></i>
                            <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_pengeluaran">Pengeluaran</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php elseif($site == "material"): ?>
            <li class="active mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageSubmenu2" data-toggle="collapse" aria-expanded="true" class= "soft-title dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-pie-chart-alt-2 my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Data</p>
                </a>
                <ul class="show list-unstyled" id="pageSubmenu2">
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_customer">Pelanggan</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_supplier">Pemasok</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 primary-title' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_material">Material</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_satuan">Satuan material</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 d-none' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2 d-none" href="<?php echo base_url() ?>index.php/c_tipe">Tipe Pelanggan</a>
                    </li>
                    <?php if($this->session->userdata("user_level") == "admin"): ?>
                        <li class="active" style="display:flex;">
                            <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 soft-title' style=' margin-left:20px;'  ></i>
                            <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_pengeluaran">Pengeluaran</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php elseif($site == "merk"): ?>
            <li class="active mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageSubmenu2" data-toggle="collapse" aria-expanded="true" class= "soft-title dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-pie-chart-alt-2 my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Data</p>
                </a>
                <ul class="show list-unstyled" id="pageSubmenu2">
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_customer">Pelanggan</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_supplier">Pemasok</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_material">Material</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_satuan">Satuan material</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 d-none' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2 d-none" href="<?php echo base_url() ?>index.php/c_tipe">Tipe Pelanggan</a>
                    </li>
                    <?php if($this->session->userdata("user_level") == "admin"): ?>
                        <li class="active" style="display:flex;">
                            <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 soft-title' style=' margin-left:20px;'  ></i>
                            <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_pengeluaran">Pengeluaran</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php elseif($site == "satuan"): ?>
            <li class="active mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageSubmenu2" data-toggle="collapse" aria-expanded="true" class= "soft-title dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-pie-chart-alt-2 my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Data</p>
                </a>
                <ul class="show list-unstyled" id="pageSubmenu2">
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_customer">Pelanggan</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_supplier">Pemasok</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_material">Material</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 primary-title' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_satuan">Satuan material</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 d-none' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2 d-none" href="<?php echo base_url() ?>index.php/c_tipe">Tipe Pelanggan</a>
                    </li>
                    <?php if($this->session->userdata("user_level") == "admin"): ?>
                        <li class="active" style="display:flex;">
                            <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 soft-title' style=' margin-left:20px;'  ></i>
                            <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_pengeluaran">Pengeluaran</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php elseif($site == "tipe"): ?>
            <li class="active mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageSubmenu2" data-toggle="collapse" aria-expanded="true" class= "soft-title dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-pie-chart-alt-2 my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Data</p>
                </a>
                <ul class="show list-unstyled" id="pageSubmenu2">
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_customer">Pelanggan</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_supplier">Pemasok</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_material">Material</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 soft-title' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_satuan">Satuan material</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 primary-title d-none' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="primary-title c-text-2 d-none" href="<?php echo base_url() ?>index.php/c_tipe">Tipe Pelanggan</a>
                    </li>
                    <?php if($this->session->userdata("user_level") == "admin"): ?>
                        <li class="active" style="display:flex;">
                            <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 soft-title' style=' margin-left:20px;'  ></i>
                            <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_pengeluaran">Pengeluaran</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php elseif($site == 'pengeluaran'): ?>
            <li class="active mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageSubmenu2" data-toggle="collapse" aria-expanded="false" class= "soft-title dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-pie-chart-alt-2 my-auto bx-md soft-title' style=' margin-left:-10px;'></i><p class="medium-weight soft-title" style="display: initial; margin-bottom:0;">Data</p>
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenu2">
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_customer">Pelanggan</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_supplier">Pemasok</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_material">Material</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_satuan">Satuan material</a>
                    </li>   
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 soft-title d-none' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2 d-none" href="<?php echo base_url() ?>index.php/c_tipe">Tipe Pelanggan</a>
                    </li>
                    <?php if($this->session->userdata("user_level") == "admin"): ?>
                        <li class="active" style="display:flex;">
                            <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 primary-title' style=' margin-left:20px;'  ></i>
                            <a tabindex="-1" class="primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_pengeluaran">Pengeluaran</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php else: ?>
            <li class="active mt-3" style="display:contents;">
                <a tabindex="-1" href="#pageSubmenu2" data-toggle="collapse" aria-expanded="false" class= "soft-title dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-pie-chart-alt-2 my-auto bx-md soft-title' style=' margin-left:-10px;'></i><p class="medium-weight soft-title" style="display: initial; margin-bottom:0;">Data</p>
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenu2">
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_customer">Pelanggan</a>
                    </li>
                    <li class="active " style="display:flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_supplier">Pemasok</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_material">Material</a>
                    </li>
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_satuan">Satuan material</a>
                    </li>   
                    <li class="active" style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 soft-title d-none' style=' margin-left:20px;'  ></i>
                        <a tabindex="-1" class="soft-title c-text-2 d-none" href="<?php echo base_url() ?>index.php/c_tipe">Tipe Pelanggan</a>
                    </li>
                    <?php if($this->session->userdata("user_level") == "admin"): ?>
                        <li class="active" style="display:flex;">
                            <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 soft-title' style=' margin-left:20px;'  ></i>
                            <a tabindex="-1" class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_pengeluaran">Pengeluaran</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>
        </ul>

    </nav>
</div>