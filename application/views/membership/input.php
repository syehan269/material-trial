<div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4">Add Pemasok</p>
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <div class="col-12 d-inline-flex p-0">
            <form action="" method="get" class="col-12 p-0">
                <div class="flex-column col-12 main-padding-l pr-0">
                    <div class="col-12 p-0">
                        <p class="c-text-2 soft-title medium-weight">Nama Member</p>
                        <input id="name" class="col-12 c-text-2 search-fill main-padding-l main-padding-r">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight">Total Poin</p>
                        <input id="poin" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" value="0">
                    </div>                    
                    <a href="#" id="btn_send">
                        <button type="button" class="btn-add col-12 text-white c-color-primary c-color-primary mt-4 c-text-2">
                            Tambah Member
                        </button>
                    </a>
                </div>
            </form>
        </div>
    </div>