<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_profile extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
            $this->load->model('Main_model');
            date_default_timezone_set('Asia/Jakarta');
        }

        public function index(){
            $send['site'] = "profile";
            $header['title'] = "Profile";

            //get data
            $this->db->select("*");
            $this->db->from("tb_rekening");
            $this->db->where("is_deleted_rekening","0");
            $send['result'] = $this->db->get()->result();

            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('data/profile/profil');
            $this->load->view('header-footer/footer');
        }

        public function input_rekening(){
            $this->form_validation->set_rules('nama-rekening','Nama Bank','required');
            $this->form_validation->set_rules('nomor-rekening','Nomor Rekening','required');
            $id = 'RKN-'.now('Asia/Jakarta');

            if ($this->form_validation->run() == true) {
                $data = [
                    'id_rekening' => $id,
                    'bank_rekening' => $this->input->post('nama-rekening'),
                    'nomor_rekening' => $this->input->post('nomor-rekening'),
                    'deskripsi_rekening' => $this->input->post('desc-rekening'),
                    'insert_date_rekening' => date("Y-m-d h:i:s"),
                    'is_deleted_rekening' => '0'
                ];
                $this->db->insert('tb_rekening', $data);
                redirect('index.php/c_profile');
            }else {
                $this->index();
                //redirect('index.php/c_satuan');
            }
        }

        public function edit_rekening(){
            $id = $this->input->post('id');
            $bank_rekening = $this->input->post('bank_rekening');
            $nomor_rekening = $this->input->post('nomor_rekening');
            $desc_rekening = $this->input->post('desc_rekening');
            $data = $this->Main_model->update_data('tb_rekening', "bank_rekening = '$bank_rekening', nomor_rekening = '$nomor_rekening', deskripsi_rekening = '$desc_rekening' WHERE id_rekening = '$id'");
            echo json_encode($data);
        }

        public function delete_rekening($id){
            
            $this->db->set("is_deleted_rekening","1");
            $this->db->where("id_rekening",$id);
            $this->db->update("tb_rekening");

            redirect('index.php/c_profile/');
        }

    }
    

?>