<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_merk extends CI_Controller
    {
        public function index(){
            $send['site'] = "merk";
            $header['title'] = "Merk";
            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('merk/merk');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $send['site'] = "merk";
            $header['title'] = "Tambah Merk";
            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('merk/input');
            $this->load->view('header-footer/footer');
        }

        public function edit(){
            $send['id'] = $this->input->get('parameter1');
            $send['site'] = "merk";
            $header['title'] = "Edit Merk";
            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('merk/edit');
            $this->load->view('header-footer/footer');
        }

    }
    

?>