<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LaporanLaba extends CI_Controller
{
    
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Report_laba_model');
		$this->load->model('Report_In_model');
		$this->load->model('Report_Out_model');
	}

	public function index()
	{
		$header['title'] = 'Laporan Laba';
		$data['site'] = 'laporan_pengeluaran';

		$data['resultIn'] = $this->Report_In_model->getAll();
		$data['resultOut'] = $this->Report_Out_model->getAll();
		$data['resultExp'] = $this->Report_laba_model->getAll();

		$data['sumOut'] = $this->Report_Out_model->sumAll();
		$data['sumIn'] = $this->Report_In_model->sumAll();
		$data['sumExp'] = $this->Report_laba_model->sumAll();

		$data['laba'] = $data['sumOut'] - ($data['sumExp'] + $data['sumIn']);

		$this->load->view('header-footer/header', $header);
		$this->load->view('sidebar-topbar/side', $data);
		$this->load->view('laporan/laba');
		$this->load->view('header-footer/footer');
	}

	public function search(){
		$header['title'] = 'Laporan Material Masuk';
		$data['site'] = 'laporan_pengeluaran';

		$awal = $this->uri->segment(3);
		$akhir = $this->uri->segment(4);
		
		$data['resultOut'] = $this->Report_Out_model->getByDateRange($awal, $akhir);
		$data['resultIn'] = $this->Report_In_model->getByDateRange($awal, $akhir);
		$data['resultExp'] = $this->Report_laba_model->getAll();

		$data['sumIn'] = $this->Report_In_model->sumByDateRange($awal, $akhir);
		$data['sumOut'] = $this->Report_Out_model->sumByDateRange($awal, $akhir);
		$data['sumExp'] = $this->Report_laba_model->sumAll();

		$data['laba'] = $data['sumOut'] - ($data['sumExp'] + $data['sumIn']);

		$this->load->view('header-footer/header', $header);
		$this->load->view('sidebar-topbar/side', $data);
		$this->load->view('laporan/laba');
		$this->load->view('header-footer/footer');
	}

}


/* End of file LaporanPengeluaran.php */
/* Location: ./application/controllers/LaporanPengeluaran.php */