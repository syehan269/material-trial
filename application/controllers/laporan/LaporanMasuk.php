<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LaporanMasuk extends CI_Controller
{
    
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Report_In_model');
		$this->load->model('Trans_Out_model');
	}

	public function index()
	{
		$header['title'] = 'Laporan Material Masuk';
		$data['site'] = 'laporan_trans_in';

		$data['total'] = $this->Report_In_model->sumAll();
		$data['result'] = $this->Report_In_model->getAll();
		$data['tod'] = $this->Trans_Out_model->getItem("semen");

		$this->load->view('header-footer/header', $header);
		$this->load->view('sidebar-topbar/side', $data);
		//$this->load->view('laporan/trans-in');
		$this->load->view('laporan/kok');
		$this->load->view('header-footer/footer');
	}

	public function search(){
		$header['title'] = 'Laporan Material Masuk';
		$data['site'] = 'laporan_trans_in';
		$data['total'] = 0;

		$supplier = $this->input->post('nama-supplier');
		$awal = $this->input->post('tanggal-awal');
		$akhir = $this->input->post('tanggal-akhir');

		if (strlen($supplier) > 0 && strlen($awal) == 0 && strlen($akhir) == 0) {
			$data['result'] = $this->Report_In_model->getBySupplier($supplier);
			$data['total'] = $this->Report_In_model->sumBySupplier($supplier);
		}else if(strlen($supplier) == 0 && strlen($awal) > 0 && strlen($akhir) > 0){
			$data['result'] = $this->Report_In_model->getByDateRange($awal, $akhir);
			$data['total'] = $this->Report_In_model->sumByDateRange($awal, $akhir);
		}else if(strlen($supplier) > 0 && strlen($awal) > 0 && strlen($akhir) > 0){
			$data['result'] = $this->Report_In_model->getByAll($supplier, $awal, $akhir);
			$data['total'] = $this->Report_In_model->sumByAll($supplier, $awal, $akhir);
		} else {
			redirect('index.php/laporan-masuk', 'refresh');
		}

		$this->load->view('header-footer/header', $header);
		$this->load->view('sidebar-topbar/side', $data);
		$this->load->view('laporan/trans-in');
		$this->load->view('header-footer/footer');
	}

}


/* End of file LaporanMasuk.php */
/* Location: ./application/controllers/LaporanMasuk.php */