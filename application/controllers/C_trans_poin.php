<?php
defined('BASEPATH') or exit('No direct script access allowed');
// Don't forget include/define REST_Controller path

class c_trans_poin extends CI_Controller
{
    
  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $send['site'] = "trans-poin";
    $header['title'] = "Poin";

    $this->db->select("*");
    $this->db->where("trans_poin_deleted_at","0");
    $this->db->from("tb_trans_poin");
    $send['transPoin'] = $this->db->get()->result();

    $this->load->view('header-footer/header', $header);
    $this->load->view('sidebar-topbar/side', $send);
    $this->load->view('membership/trans-poin');
    $this->load->view('header-footer/footer');
    //$this->load->view('function');

  }

}


/* End of file C_trans_poin.php */
/* Location: ./application/controllers/C_trans_poin.php */