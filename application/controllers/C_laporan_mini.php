<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_laporan_mini extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
            $this->load->model('Main_model');
            date_default_timezone_set('Asia/Jakarta');
        }

        public function index(){
            $send['site'] = "mini_report";
            $header['title'] = "Transaksi Hari Ini";

            $send['result'] = $this->Main_model->get_data("tb_trans_out", "WHERE CAST(trans_out_insert_date AS DATE) = CAST('".date("Y-m-d")."' AS DATE) AND trans_out_delivery_type = 'Delivery' AND trans_out_is_delete = '0'");

            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('laporan/today');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function bank_transfer(){
            $send['site'] = "mini_report";
            $header['title'] = "Transaksi ke Rekening";

            $this->db->select("*");
            $this->db->from("tb_rekening");
            $this->db->where("is_deleted_rekening","0");
            $send['rekening'] = $this->db->get()->result();

            $send['result'] = $this->Main_model->get_data("tb_trans_out", "WHERE trans_out_payment_type = 'ATM' AND trans_out_is_delete = '0'");

            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('laporan/bank');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function sent(){
            $id= $this->input->post('sent-id');

            $this->Main_model->update_data("tb_trans_out", "trans_out_sent_status = '1', trans_out_sent_date = '".date("Y-m-d h:i:s")."' WHERE trans_out_id = '".$id."'");

            redirect('index.php/c_laporan_mini/');
        }

        public function reverse(){
            $id= $this->input->post('reverse-id');

            $this->Main_model->update_data("tb_trans_out", "trans_out_sent_status = '0', trans_out_sent_date = '".date("Y-m-d h:i:s")."' WHERE trans_out_id = '".$id."'");

            redirect('index.php/c_laporan_mini/');
        }

        function get_data(){
            $material_data = $this->Main_model->get_data("tb_material", "WHERE material_is_delete = '0'");
            $customer_data = $this->Main_model->get_data("tb_customer", "WHERE customer_is_delete = '0'");
            $dataArray = array(
                'material_data' => $material_data,
                'customer_data' => $customer_data
            );
            echo json_encode($dataArray);
        }

    }
    
?>