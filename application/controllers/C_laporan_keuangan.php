<?php
defined('BASEPATH') or exit('No direct script access allowed');
// Don't forget include/define REST_Controller path

class c_laporan_keuangan extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $send['site'] = "";
        $send['stat'] = "";
        $header['title'] = "Laporan";

        $this->db->select("*");
        $this->db->where("is_deleted","0");
        $this->db->from("tb_pengeluaran");
        $send["pengeluaran"] = $this->db->get()->result();

        $this->db->where('is_deleted',0);
        $this->db->select_sum('pengeluaran_amount');
        $send['totalPengeluaran'] = $this->db->get('tb_pengeluaran')->result();
        
        $this->db->where("trans_in_is_delete","0");
		$this->db->where("trans_in_complete","1");
		$this->db->select_sum('trans_in_payment_amount');
        $query = $this->db->get('tb_trans_in')->result();
        
        $this->db->where("trans_out_is_delete","0");
		$this->db->where("trans_out_complete","1");
		$this->db->select_sum('trans_out_payment_amount');
        $query1 = $this->db->get('tb_trans_out')->result();
        
        foreach ($query as $key ) {
            $send['sum_in'] = $key->trans_in_payment_amount;
        }
        
        foreach ($query1 as $key ) {
            $send['sum_out'] = $key->trans_out_payment_amount;   
        }

        $this->load->view('header-footer/header', $header);
        $this->load->view('sidebar-topbar/side', $send);
        $this->load->view('laporan/laporan');
        $this->load->view('header-footer/footer');
        $this->load->view('function');
    }

    public function custom_date(){
        $send['site'] = "";
        $send['stat'] = "custom";
        $header['title'] = "Laporan";

        $date1 = $this->input->post("date1");
        $date2 = $this->input->post("date2");

        $this->db->select("*");
        $this->db->where("is_deleted","0");
        $this->db->from("tb_pengeluaran");
        $send["pengeluaran"] = $this->db->get()->result();

        $this->db->where('is_deleted',0);
        $this->db->select_sum('pengeluaran_amount');
        $send['totalPengeluaran'] = $this->db->get('tb_pengeluaran')->result();
        
        $this->db->where("trans_in_is_delete","0");
		$this->db->where("trans_in_complete",'1');
        $this->db->where("trans_in_insert_date Between '$date1' AND '$date2'");
		$this->db->select_sum('trans_in_payment_amount');
        $query = $this->db->get('tb_trans_in')->result();
        
        $this->db->where("trans_out_is_delete","0");
		$this->db->where("trans_out_complete","1");
        $this->db->where("trans_out_insert_date Between '$date1' AND '$date2'");
		$this->db->select_sum('trans_out_payment_amount');
        $query1 = $this->db->get('tb_trans_out')->result();
        
        foreach ($query as $key ) {
            $send['sum_in'] = $key->trans_in_payment_amount;
        }
        
        foreach ($query1 as $key ) {
            $send['sum_out'] = $key->trans_out_payment_amount;   
        }

        $this->load->view('header-footer/header', $header);
        $this->load->view('sidebar-topbar/side', $send);
        $this->load->view('laporan/laporan');
        $this->load->view('header-footer/footer');
        $this->load->view('function');

    }

    public function static_report(){
        $send['site'] = "";
        $send['stat'] = "custom";
        $header['title'] = "Laporan";

        $date2 = $this->uri->segment(3);
        $date1 = $this->uri->segment(4);

        $this->db->select("*");
        $this->db->where("is_deleted","0");
        $this->db->from("tb_pengeluaran");
        $send["pengeluaran"] = $this->db->get()->result();

        $this->db->where('is_deleted',0);
        $this->db->select_sum('pengeluaran_amount');
        $send['totalPengeluaran'] = $this->db->get('tb_pengeluaran')->result();
        
        $this->db->where("trans_in_is_delete","0");
		$this->db->where("trans_in_complete",'1');
        $this->db->where("trans_in_insert_date Between '$date1' AND '$date2'");
		$this->db->select_sum('trans_in_payment_amount');
        $query = $this->db->get('tb_trans_in')->result();
        
        $this->db->where("trans_out_is_delete","0");
		$this->db->where("trans_out_complete","1");
        $this->db->where("trans_out_insert_date Between '$date1' AND '$date2'");
		$this->db->select_sum('trans_out_payment_amount');
        $query1 = $this->db->get('tb_trans_out')->result();
        
        foreach ($query as $key ) {
            $send['sum_in'] = $key->trans_in_payment_amount;
        }
        
        foreach ($query1 as $key ) {
            $send['sum_out'] = $key->trans_out_payment_amount;   
        }

        $this->load->view('header-footer/header', $header);
        $this->load->view('sidebar-topbar/side', $send);
        $this->load->view('laporan/laporan');
        $this->load->view('header-footer/footer');
        $this->load->view('function');

    }

}


/* End of file C_laporan_keuangan.php */
/* Location: ./application/controllers/C_laporan_keuangan.php */