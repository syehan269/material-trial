<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_membership extends CI_Controller
    {
        public function index(){
            $send['site'] = "membership";
            $header['title'] = "Membership";

            $this->db->select("member_id");
            $this->db->from("tb_membership");
            $this->db->order_by("member_id","DESC");
            $this->db->limit(1);
            $send['data'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->where("customer_is_delete","0");
            $this->db->from("tb_customer");
            $send["customer"] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->where("is_delete","0");
            $this->db->from("tb_membership");
            $send["membership"] = $this->db->get()->result();

            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('membership/membership');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $send['site'] = "membership";

            $this->form_validation->set_rules('name','nama member','required');
            $id = 'MBR-'.now('Asia/Jakarta');
            if ($this->form_validation->run() == true) {
                $data = [
                    'member_name' => $this->input->post('name'),
                    'member_id' => $id,
                    'member_poin' => $this->input->post('poin'),
                    'created_at' => date("Y-m-d h:i:s"),
                    'is_delete' => '0'
                ];
                $this->db->insert('tb_membership',$data);
                redirect('index.php/c_membership');
            }else {
                redirect('index.php/c_membership');
            }
        }

        public function penarikan(){
            $send['site'] = "membership";
            $rawMember = explode("|",$this->input->post("member-penarikan"));
            $nama = $this->input->post("nama-penarikan");
            $poin = $this->input->post("poin-penarikan");
            $memberK = explode(" ",$this->input->post("poin-member-penarikan"));

            $this->form_validation->set_rules('member-penarikan','Member','required',
                array(
                    'required' => '{field} tidak boleh kosong.'
                )
            );
            $this->form_validation->set_rules('nama-penarikan','Nama kegiatan','required',
                array(
                    'required' => '{field} tidak boleh kosong.'
                )
            );
            $this->form_validation->set_rules('poin-penarikan','Poin','required|less_than_equal_to['.$memberK[0].']',
                array(
                    'required' => '{field} tidak boleh kosong.',
                    'less_than_equal_to' => 'Poin member harus lebih dari poin penarikan.'
                )
            );

            if ($this->form_validation->run()) {
                $member = $rawMember[0];
                $poinMember = $rawMember[1];
                $memberId = $rawMember[2];
                $id = 'TPN-'.now('Asia/Jakarta');

                $this->db->where('member_id',$memberId);
                $this->db->update('tb_membership',array(
                    'member_poin' => $poinMember-$poin
                ));

                $this->db->insert('tb_trans_poin',array(
                    'trans_poin_id' => $id,
                    'trans_poin_member' => $member,
                    'trans_poin_kegiatan' => $nama,
                    'trans_poin_jumlah' => $poin,
                    'trans_poin_created' => date("Y-m-d h:i:s")
                ));

                redirect('index.php/c_membership');
            } else {
                //redirect('index.php/c_membership');
                $this->index();
            }
            
        }

        public function update_poin($id){
            $send['site'] = "membership";

            $this->form_validation->set_rules('newAmount','Poin member','required');
            if ($this->form_validation->run() == true) {
                $data =[          
                    'member_poin' => $this->input->post('newAmount'),
                ];
                $this->db->where("member_id",$id);
                $this->db->update("tb_membership",$data);
                redirect('index.php/c_membership');
            }else {
                redirect('index.php/c_membership');
            }
        }

        public function edit(){
            $send['id'] = $this->input->get('parameter1');
            $send['site'] = "membership";
            $header['title'] = "Edit Membership";
            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('membership/edit');
            $this->load->view('header-footer/footer');
        }

        public function delete($id){
            $this->db->set("is_delete","1");
            $this->db->where("member_id",$id);
            $this->db->update("tb_membership");

            redirect('index.php/c_membership/');
        }

    }

?>