<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_income extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
            $this->load->model('Main_model');
            date_default_timezone_set('Asia/Jakarta');
        }

        function rupiah($angka){
	
            $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
            return $hasil_rupiah;
         
        }

        public function index(){
            $data['site'] = "transaction_in";
            $header['title'] = "Material Masuk";
            $this->db->select("*");
            $this->db->from("tb_trans_in");
            $this->db->where("trans_in_is_delete","0");
            $data['result'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_trans_in");
            $this->db->join("tb_detail_trans_in", "tb_trans_in.trans_in_id = tb_detail_trans_in.detail_in_trans_in_id","inner");
            $data['hasil'] = $this->db->get()->result();

            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $data);
            $this->load->view('transaction/income/income');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $data['site'] = "transaction_in";
            $header['title'] = "Tambah Material Masuk";

            $this->db->select("*");
            $this->db->from("tb_material");
            $this->db->where("material_is_delete","0");
            $data['material'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_satuan");
            $this->db->where("satuan_is_delete","0");
            $data['satuan'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_supplier");
            $this->db->where("supplier_is_delete","0");
            $data['supplier'] = $this->db->get()->result();

            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $data);
            $this->load->view('transaction/income/input');
            $this->load->view('header-footer/footer');
        }

        public function input_all(){
            $getMethod = $this->input->post("payment");
            $getSupplier = $this->input->post("detail_in_supplier_id");
            $getAmount = $this->input->post("materialAmount");
            $getInfo = $this->input->post("info");
            $valId = 'TIN-'.now('Asia/Jakarta');
            $getPaid = 0;
            $getComp = 1;
            $getMatAmount = $this->input->post("materialItem");
            $materialBundle = array();

            $saldo = $this->input->post('saldo-cicilan');
            $tanggalAkhir = $this->input->post('tanggal-cicilan');
            $textCicilan = $this->input->post('text-cicilan');
            
            $this->form_validation->set_rules('detail_in_supplier_id','Supplier','required',
                array(
                    'required' => '%s harus diisi'
                )
            );

            $this->form_validation->set_rules('materialAmount','Material','required',
                array(
                    'required' => '%s harus diisi'
                )
            );

            if ($this->form_validation->run()) {
                if($getMethod == "Cash"){
                    $getPaid = $this->input->post("paymentPaid");
                }

                if ($getMethod == "Credit" ) {
                    $getPaid = $saldo;
                    $getComp = 0;
                    
                    if (strlen($saldo)) {
                        $utang = [
                            'utang_id' => 'UTA-'.now('Asia/Jakarta'),
                            'utang_trans_in_id' => $valId,
                            'utang_amount' => $saldo,
                            'utang_paid' => '0',
                            'utang_information' => 'Cicilan ke-1'
                        ];
                        //$this->db->insert("tb_utang",$utang);
                    }
                    
                }

                for ($i=0; $i < $getMatAmount; $i++) { 
                    $id = $i + 1;
                    $matId = "materialBack".$id;
                    $idId = "idBack".$id;
                    $quantId = "quantBack".$id;
                    $totalId = "totalBack".$id;
                    $priceId = "priceBack".$id;
                    
                    $valMat = $this->input->post($matId);
                    $valMatId = $this->input->post($idId);
                    $valPri = $this->input->post($priceId);
                    $valTot = $this->input->post($totalId);
                    $valQua = $this->input->post($quantId);

                    array_push($materialBundle, array(
                        'detail_in_trans_in_id' => $valId,
                        'detail_in_material_id' => $valMatId,
                        'detail_in_material_name' => $valMat,
                        'detail_in_material_price' => $valPri,
                        'detail_in_material_amount' => $valQua
                    ));

                    //get material stock
                    $quant = "";
                    $this->db->select("material_stock");
                    $this->db->from("tb_material");
                    $this->db->where("material_id",$valMatId);
                    $quant = $this->db->get()->result();
                    $finQuant = $quant[0]->material_stock + $valQua;
                    //update stock
                    $this->db->set("material_stock",$finQuant);
                    $this->db->where("material_id",$valMatId);
                    $this->db->update("tb_material");

                    //echo "tod ".$id." ".$quant[0]->material_stock.": ".$finQuant." ";
                }

                $data = [
                    'trans_in_id' => $valId,
                    'trans_in_payment_amount' => $getAmount,
                    'trans_in_paid_amount' => $getPaid,
                    'trans_in_due_date' => $textCicilan,
                    //'trans_in_deadline_text' => $textCicilan,
                    'trans_in_additional_info' => $getInfo,
                    'trans_in_supplier' => $getSupplier,
                    'trans_in_payment_type' => $getMethod,
                    'trans_in_insert_date' => date("Y-m-d H:i:s"),
                    'trans_in_complete' => $getComp,
                    'trans_in_is_delete' => '0'
                ];

                $this->db->insert('tb_trans_in', $data);
                //$this->db->insert_batch("tb_detail_trans_in", $materialBundle);
                redirect('index.php/c_income/input');
            }else {
                $this->input();
            }
            
        }

        public function edit($id){
            $data['site'] = "transaction_in";
            $data['id'] = $id;

            $this->db->select("*");
            $this->db->from("tb_trans_in");
            $this->db->where("trans_in_id",$id);
            $data['data'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_detail_trans_in");
            $this->db->where("detail_in_trans_in_id",$id);
            $data['materialBundle'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_utang");
            $this->db->where("utang_trans_in_id",$id);
            $data['cicilanBundle'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_material");
            $this->db->where("material_is_delete","0");
            $data['material'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_supplier");
            $this->db->where("supplier_is_delete","0");
            $data['supplier'] = $this->db->get()->result();

            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $data);
            $this->load->view('transaction/income/edit');
            $this->load->view('header-footer/footer');
        }

        public function update_all(){
            $getMethod = $this->input->post("payment");
            $getSupplier = $this->input->post("name");
            $getAmount = $this->input->post("materialAmount");
            $getInfo = $this->input->post("info");
            $valId = $this->input->post("id-item");
            $getPaid = 0;
            $getMatAmount = $this->input->post("materialItem");
            $materialBundle = array();
            
            if($getMethod == "Cash"){
                $getPaid = $this->input->post("paymentPaid");
            }

            if ($getMethod == "Credit") {
                $getAmount1 = $this->input->post("cicilanAmount");
                $data = array();
                for ($i=0; $i < $getAmount1; $i++) {
                    $id = $i + 1;
                    $fin = "cicilan".$id;
                    $cicilan = "finalBack".$id;
                    $val = $this->input->post($fin);
                    $cicilanVal = $this->input->post($cicilan);

                    array_push($data, array(
                        'utang_due_date' => $val,
                        'utang_amount' => $cicilanVal,
                        'utang_trans_in_id' => $valId,
                        'utang_information' => "Cicilan ke-".$id
                    ));
                }
                //$this->db->insert_batch("tb_utang", $data);
            }

            for ($i=0; $i < $getMatAmount; $i++) { 
                $id = $i + 1;
                $matId = "materialBack".$id;
                $quantId = "quantBack".$id;
                $totalId = "totalBack".$id;
                $priceId = "priceBack".$id;
                
                $valMat = $this->input->post($matId);
                $valPri = $this->input->post($priceId);
                $valTot = $this->input->post($totalId);
                $valQua = $this->input->post($quantId);

                array_push($materialBundle, array(
                    'detail_in_trans_in_id' => $valId,
                    'detail_in_material_name' => $valMat,
                    'detail_in_material_price' => $valPri,
                    'detail_in_material_amount' => $valQua
                ));
            }

            $data = [
                'trans_in_payment_amount' => $getAmount,
                'trans_in_paid_amount' => $getPaid,
                'trans_in_additional_info' => $getInfo,
                'trans_in_supplier' => $getSupplier,
                'trans_in_payment_type' => $getMethod,
                'trans_in_last_update_date' => date("Y-m-d h:i:s"),
            ];

            $this->db->where("trans_in_id",$id);
            $this->db->update("tb_trans_in",$data);
            //$this->db->update_batch("tb_detail_trans_in", $materialBundle);
            redirect('index.php/c_income');
        }

        public function get_detail(){
            $detail = $this->Main_model->get_data('tb_detail_trans_in'," ");
            $dataArray = array(
                'data_detail' => $detail
            );
            echo json_encode($dataArray);
        }

        public function delete($id){
            $this->db->set("trans_in_is_delete","1");
            $this->db->where("trans_in_id",$id);
            $this->db->update("tb_trans_in");

            redirect('index.php/c_income/');
        }

    }
?>