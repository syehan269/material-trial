<?php
defined('BASEPATH') or exit('No direct script access allowed');
// Don't forget include/define REST_Controller path
require('./plugin/vendor/autoload.php');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Export extends CI_Controller
{
    
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Export_model');
  }

  public function index()
  {
    //
  }

  public function exTransIn(){

    $date1 = $this->uri->segment(4);
    $date2 = $this->uri->segment(3);

    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $allOut = $this->Export_model->getTransIn($date1,$date2)->result();
      $total = $this->Export_model->countTransIn($date1,$date2)->result();
      $paid = $this->Export_model->countTransInPaid($date1,$date2)->result();
      $cash = $this->Export_model->countTransInCash($date1,$date2)->result();
      $credit = $this->Export_model->countTransInCredit($date1,$date2)->result();
    } else {
      $date1 = null;
      $date2 = null;

      $allOut = $this->Export_model->getTransIn($date1,$date2)->result();
      $total = $this->Export_model->countTransIn($date1,$date2)->result();
      $paid = $this->Export_model->countTransInPaid($date1,$date2)->result();
      $cash = $this->Export_model->countTransInCash($date1,$date2)->result();
      $credit = $this->Export_model->countTransInCredit($date1,$date2)->result();
    }
    
    $sumTotal = $total[0]->trans_in_payment_amount;
    $sumPaid = $paid[0]->paid;
    $sumCash = $cash[0]->cash;
    $sumCredit = $credit[0]->credit;

    $sheet = new Spreadsheet;
    // Create a new worksheet called "My Data"
    $workSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($sheet, 'Masuk');
    // Attach the "My Data" worksheet as the first worksheet in the Spreadsheet object
    $sheet->addSheet($workSheet, 0);

    //$sheet->setActiveSheetIndex(0)
    //      ->setCellValue('A1',$date1)
    //      ->setCellValue('B1',$date2);

    $sheet->setActiveSheetIndex(0)
          ->setCellValue('B4','No')
          ->setCellValue('C4','Nama Supplier')
          ->setCellValue('D4','Jumlah Pembayaran')
          ->setCellValue('D5','Cash')
          ->setCellValue('E5','Kredit')
          ->setCellValue('F4','Jumlah Terbayar')
          ->setCellValue('G4','Jumlah Kembalian')
          ->setCellValue('H4','Status Transaksi')
          ->setCellValue('I4','Tanggal')
          ->setCellValue('J4','Info Tambahan');

    $baris = 6;
    $nomor = 1;

    foreach ($allOut as $item ) {
      $status = "";
      $kembalian = $item->trans_in_paid_amount - $item->trans_in_payment_amount;

      if ($kembalian < 1) {
        $kembalian = 0;
      }
      
      if ($item->trans_in_complete == 1) {
        $status = "Lunas";
      }else{
        $status = "Belum Lunas";
      }

      $sheet->setActiveSheetIndex(0)
            ->setCellValue('B'.$baris, $nomor)
            ->setCellValue('C'.$baris, $item->trans_in_supplier)
            ->setCellValue('F'.$baris, 'Rp '.number_format($item->trans_in_paid_amount ,0,".","."))
            ->setCellValue('G'.$baris, 'Rp '.number_format($kembalian ,0,".","."))
            ->setCellValue('H'.$baris, $status)
            ->setCellValue('I'.$baris, date('j F Y', strtotime($item->trans_in_insert_date)))
            ->setCellValue('J'.$baris, $item->trans_in_additional_info);
      
      if ($item->trans_in_payment_type == "Cash") {
        $sheet->setActiveSheetIndex(0)
              ->setCellValue('D'.$baris, 'Rp '.number_format($item->trans_in_payment_amount ,0,".","."));
      } else {
        $sheet->setActiveSheetIndex(0)
              ->setCellValue('E'.$baris, 'Rp '.number_format($item->trans_in_payment_amount ,0,".","."));
      }

      $baris++;
      $nomor++;
    }

    $styleArray = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
        ],
        'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            ],
        ],
    ];

    $styleContent = [
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'borders' => [
            'outline' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            ],
        ],
    ];

    $styleTitle = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
        ],
    ];

    //khusus title
    $sheet->getActiveSheet()->mergeCells('E2:G2');
    
    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $sheet->setActiveSheetIndex(0)
          ->setCellValue('E2','Tabel Laporan Transaksi Material Masuk Tanggal '.$date1.' sampai '.$date2);
    } else {
      $sheet->setActiveSheetIndex(0)
          ->setCellValue('E2','Tabel Laporan Transaksi Material Masuk');
    }
    

    $sheet->setActiveSheetIndex(0)
          ->setCellValue('C'.strval($baris+1),"Total")
          ->setCellValue('E'.strval($baris+1), 'Rp '.number_format($sumCredit ,0,".","."))
          ->setCellValue('D'.strval($baris+1), 'Rp '.number_format($sumCash ,0,".","."))
          ->setCellValue('D'.strval($baris+2), 'Rp '.number_format($sumTotal ,0,".","."));
    
    $sheet->getActiveSheet()->getStyle('B4:J4')->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('FFFF00');

    $sheet->getActiveSheet()->getStyle('B5:J5')->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('FFFF00');

    $sheet->getActiveSheet()->getStyle('B'.strval($baris+1).':J'.strval($baris+1))->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('FFFF00');

    $sheet->getActiveSheet()->getStyle('B'.strval($baris+2).':J'.strval($baris+2))->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('FFFF00');

    $sheet->getActiveSheet()->getStyle('E2')->applyFromArray($styleTitle);
    $sheet->getActiveSheet()->getStyle('C'.strval($baris+1))->applyFromArray($styleTitle);
    $sheet->getActiveSheet()->getStyle('B4:J4')->applyFromArray($styleArray);
    $sheet->getActiveSheet()->getStyle('B5:J5')->applyFromArray($styleArray);
    $sheet->getActiveSheet()->getStyle('D5:E5')->applyFromArray($styleArray);

    //border khusus footer
    $sheet->getActiveSheet()->getStyle('B'.strval($baris+1).':J'.strval($baris+1))->applyFromArray($styleArray);
    $sheet->getActiveSheet()->getStyle('B'.strval($baris+2).':J'.strval($baris+2))->applyFromArray($styleArray);

    //khusus header tabel
    $sheet->getActiveSheet()->mergeCells('D4:E4');
    $sheet->getActiveSheet()->mergeCells('B4:B5');
    $sheet->getActiveSheet()->mergeCells('C4:C5');
    $sheet->getActiveSheet()->mergeCells('F4:F5');
    $sheet->getActiveSheet()->mergeCells('G4:G5');
    $sheet->getActiveSheet()->mergeCells('H4:H5');
    $sheet->getActiveSheet()->mergeCells('I4:I5');
    $sheet->getActiveSheet()->mergeCells('J4:J5');

    $sheet->getActiveSheet()->mergeCells('D4:E4');
    $sheet->getActiveSheet()->mergeCells('B'.strval($baris+1).':B'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('C'.strval($baris+1).':C'.strval($baris+2));
    //$sheet->getActiveSheet()->mergeCells('D'.strval($baris+1).':D'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('D'.strval($baris+2).':E'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('F'.strval($baris+1).':F'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('G'.strval($baris+1).':G'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('H'.strval($baris+1).':H'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('I'.strval($baris+1).':I'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('J'.strval($baris+1).':J'.strval($baris+2));

    //khusus footer
    $sheet->getActiveSheet()->getStyle('B'.strval($baris+1).':J'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('B'.strval($baris+2).':J'.strval($baris+2))->applyFromArray($styleContent);

    $sheet->getActiveSheet()->getStyle('B6:B'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('C6:C'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('D6:D'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('E6:E'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('F6:F'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('G6:G'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('H6:H'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('I6:I'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('J6:J'.strval($baris+1))->applyFromArray($styleContent);

    $sheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $sheet->getActiveSheet()->getColumnDimension('C')->setWidth(28);
    $sheet->getActiveSheet()->getColumnDimension('D')->setWidth(25);
    $sheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);
    $sheet->getActiveSheet()->getColumnDimension('F')->setWidth(25);
    $sheet->getActiveSheet()->getColumnDimension('G')->setWidth(25);
    $sheet->getActiveSheet()->getColumnDimension('H')->setWidth(15);
    $sheet->getActiveSheet()->getColumnDimension('I')->setWidth(20);
    $sheet->getActiveSheet()->getColumnDimension('J')->setWidth(30);

    $this->exTransOut($sheet,$styleArray,$styleContent,$styleTitle);
    $this->exPengeluaran($sheet,$styleArray,$styleContent,$styleTitle);
    $this->setFileName($sheet);
  }

  //new workspace out
  public function exTransOut($sheet,$styleArray,$styleContent,$styleTitle){
    $date1 = $this->uri->segment(4);
    $date2 = $this->uri->segment(3);

    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $allOut = $this->Export_model->getTransOut($date1,$date2)->result();
      $total = $this->Export_model->countTransOut($date1,$date2)->result();
      $paid = $this->Export_model->countTransOutPaid($date1,$date2)->result();
      $cash = $this->Export_model->countTransOutCash($date1,$date2)->result();
      $credit = $this->Export_model->countTransOutCredit($date1,$date2)->result();
    } else {
      $date1 = null;
      $date2 = null;
      $allOut = $this->Export_model->getTransOut($date1,$date2)->result();
      $total = $this->Export_model->countTransOut($date1,$date2)->result();
      $paid = $this->Export_model->countTransOutPaid($date1,$date2)->result();
      $cash = $this->Export_model->countTransOutCash($date1,$date2)->result();
      $credit = $this->Export_model->countTransOutCredit($date1,$date2)->result();
    }
    
    $sumTotal = $total[0]->trans_out_payment_amount;
    $sumPaid = $paid[0]->paid;
    $sumCash = $cash[0]->cash;
    $sumCredit = $credit[0]->credit;

    // Create a new worksheet called "My Data"
    $workSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($sheet, 'Keluar');
    // Attach the "My Data" worksheet as the first worksheet in the Spreadsheet object
    $sheet->addSheet($workSheet, 1);

    $sheet->setActiveSheetIndex(1)
          ->setCellValue('B4','No')
          ->setCellValue('C4','Nama Customer')
          ->setCellValue('D4','Jumlah Pembayaran')
          ->setCellValue('D5','Cash')
          ->setCellValue('E5','Kredit')
          ->setCellValue('F4','Jumlah Terbayar')
          ->setCellValue('G4','Jumlah Kembalian')
          ->setCellValue('H4','Status Transaksi')
          ->setCellValue('I4','Tanggal')
          ->setCellValue('J4','Info Tambahan');

    $baris = 6;
    $nomor = 1;

    foreach ($allOut as $item ) {
      $status = "";
      $kembalian = $item->trans_out_paid_amount - $item->trans_out_payment_amount;

      if ($kembalian < 1) {
        $kembalian = 0;
      }
      
      if ($item->trans_out_complete == 1) {
        $status = "Lunas";
      }else{
        $status = "Belum Lunas";
      }

      $sheet->setActiveSheetIndex(1)
            ->setCellValue('B'.$baris, $nomor)
            ->setCellValue('C'.$baris, $item->trans_out_customer_name)
            ->setCellValue('F'.$baris, 'Rp '.number_format($item->trans_out_paid_amount ,0,".","."))
            ->setCellValue('G'.$baris, 'Rp '.number_format($kembalian ,0,".","."))
            ->setCellValue('H'.$baris, $status)
            ->setCellValue('I'.$baris, date('j F Y', strtotime($item->trans_out_insert_date)))
            ->setCellValue('J'.$baris, $item->trans_out_additional_info);
      
      if ($item->trans_out_payment_type == "Cash") {
        $sheet->setActiveSheetIndex(1)
              ->setCellValue('D'.$baris, 'Rp '.number_format($item->trans_out_payment_amount ,0,".","."));
      } else {
        $sheet->setActiveSheetIndex(1)
              ->setCellValue('E'.$baris, 'Rp '.number_format($item->trans_out_payment_amount ,0,".","."));
      }

      $baris++;
      $nomor++;
    }

    //khusus title
    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $sheet->setActiveSheetIndex(1)
          ->setCellValue('E2','Tabel Laporan Transaksi Material Keluar Tanggal '.$date1.' sampai '.$date2);
    } else {
      $sheet->setActiveSheetIndex(1)
          ->setCellValue('E2','Tabel Laporan Transaksi Material Keluar');
    }

    $sheet->setActiveSheetIndex(1)
          ->setCellValue('C'.strval($baris+1),"Total")
          ->setCellValue('E'.strval($baris+1), 'Rp '.number_format($sumCredit ,0,".","."))
          ->setCellValue('D'.strval($baris+1), 'Rp '.number_format($sumCash ,0,".","."))
          ->setCellValue('D'.strval($baris+2), 'Rp '.number_format($sumCash + $sumCredit ,0,".","."));
    
    $sheet->getActiveSheet()->getStyle('B4:J4')->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('FFFF00');

    $sheet->getActiveSheet()->getStyle('B5:J5')->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('FFFF00');

    $sheet->getActiveSheet()->getStyle('B'.strval($baris+1).':J'.strval($baris+1))->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('FFFF00');

    $sheet->getActiveSheet()->getStyle('B'.strval($baris+2).':J'.strval($baris+2))->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('FFFF00');

    $sheet->getActiveSheet()->getStyle('E2')->applyFromArray($styleTitle);
    $sheet->getActiveSheet()->getStyle('C'.strval($baris+1))->applyFromArray($styleTitle);
    $sheet->getActiveSheet()->getStyle('B4:J4')->applyFromArray($styleArray);
    $sheet->getActiveSheet()->getStyle('B5:J5')->applyFromArray($styleArray);
    $sheet->getActiveSheet()->getStyle('D5:E5')->applyFromArray($styleArray);

    //border khusus footer
    $sheet->getActiveSheet()->getStyle('B'.strval($baris+1).':J'.strval($baris+1))->applyFromArray($styleArray);
    $sheet->getActiveSheet()->getStyle('B'.strval($baris+2).':J'.strval($baris+2))->applyFromArray($styleArray);

    //khusus header tabel
    $sheet->getActiveSheet()->mergeCells('D4:E4');
    $sheet->getActiveSheet()->mergeCells('B4:B5');
    $sheet->getActiveSheet()->mergeCells('C4:C5');
    $sheet->getActiveSheet()->mergeCells('F4:F5');
    $sheet->getActiveSheet()->mergeCells('G4:G5');
    $sheet->getActiveSheet()->mergeCells('H4:H5');
    $sheet->getActiveSheet()->mergeCells('I4:I5');
    $sheet->getActiveSheet()->mergeCells('J4:J5');

    $sheet->getActiveSheet()->mergeCells('D4:E4');
    $sheet->getActiveSheet()->mergeCells('B'.strval($baris+1).':B'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('C'.strval($baris+1).':C'.strval($baris+2));
    //$sheet->getActiveSheet()->mergeCells('D'.strval($baris+1).':D'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('D'.strval($baris+2).':E'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('F'.strval($baris+1).':F'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('G'.strval($baris+1).':G'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('H'.strval($baris+1).':H'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('I'.strval($baris+1).':I'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('J'.strval($baris+1).':J'.strval($baris+2));

    //khusus footer
    $sheet->getActiveSheet()->getStyle('B'.strval($baris+1).':J'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('B'.strval($baris+2).':J'.strval($baris+2))->applyFromArray($styleContent);

    $sheet->getActiveSheet()->getStyle('B6:B'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('C6:C'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('D6:D'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('E6:E'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('F6:F'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('G6:G'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('H6:H'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('I6:I'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('J6:J'.strval($baris+1))->applyFromArray($styleContent);

    $sheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $sheet->getActiveSheet()->getColumnDimension('C')->setWidth(28);
    $sheet->getActiveSheet()->getColumnDimension('D')->setWidth(25);
    $sheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);
    $sheet->getActiveSheet()->getColumnDimension('F')->setWidth(25);
    $sheet->getActiveSheet()->getColumnDimension('G')->setWidth(25);
    $sheet->getActiveSheet()->getColumnDimension('H')->setWidth(15);
    $sheet->getActiveSheet()->getColumnDimension('I')->setWidth(20);
    $sheet->getActiveSheet()->getColumnDimension('J')->setWidth(30);

    //$this->setFileName($sheet);
  }

  //new workspace out
  public function exPengeluaran($sheet,$styleArray,$styleContent,$styleTitle){

    $allOut = $this->Export_model->getPengeluaran()->result();
    $total = $this->Export_model->countPengeluaran()->result();
    
    $sumTotal = $total[0]->pengeluaran_amount;

    // Create a new worksheet called "My Data"
    $workSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($sheet, 'Pengeluaran');
    // Attach the "My Data" worksheet as the first worksheet in the Spreadsheet object
    $sheet->addSheet($workSheet, 2);

    $sheet->setActiveSheetIndex(2)
          ->setCellValue('B4','No')
          ->setCellValue('C4','Nama Pengeluaran')
          ->setCellValue('D4','Jumlah Pembayaran');

    $baris = 6;
    $nomor = 1;

    foreach ($allOut as $item ) {
      $status = "";

      $sheet->setActiveSheetIndex(2)
            ->setCellValue('B'.$baris, $nomor)
            ->setCellValue('C'.$baris, $item->pengeluaran_nama)
            ->setCellValue('D'.$baris, 'Rp '.number_format($item->pengeluaran_amount ,0,".","."));

      $baris++;
      $nomor++;
    }

    //khusus title
    $sheet->getActiveSheet()->mergeCells('C2:D2');
    $sheet->setActiveSheetIndex(2)
          ->setCellValue('C2','Tabel Laporan Pengeluaran');

    $sheet->setActiveSheetIndex(2)
          ->setCellValue('C'.strval($baris+1),"Total")
          ->setCellValue('D'.strval($baris+1), 'Rp '.number_format($sumTotal ,0,".","."));
    
    $sheet->getActiveSheet()->getStyle('B4:D4')->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('FFFF00');

    $sheet->getActiveSheet()->getStyle('B5:D5')->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('FFFF00');

    $sheet->getActiveSheet()->getStyle('B'.strval($baris+1).':D'.strval($baris+1))->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('FFFF00');

    $sheet->getActiveSheet()->getStyle('B'.strval($baris+2).':D'.strval($baris+2))->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('FFFF00');

    //style title
    $sheet->getActiveSheet()->getStyle('C2')->applyFromArray($styleTitle);
    //
    $sheet->getActiveSheet()->getStyle('C'.strval($baris+1))->applyFromArray($styleTitle);
    $sheet->getActiveSheet()->getStyle('B4:D4')->applyFromArray($styleArray);
    $sheet->getActiveSheet()->getStyle('B5:D5')->applyFromArray($styleArray);

    //border khusus footer
    $sheet->getActiveSheet()->getStyle('B'.strval($baris+1).':D'.strval($baris+1))->applyFromArray($styleArray);
    $sheet->getActiveSheet()->getStyle('B'.strval($baris+2).':D'.strval($baris+2))->applyFromArray($styleArray);

    //khusus header tabel
    $sheet->getActiveSheet()->mergeCells('B4:B5');
    $sheet->getActiveSheet()->mergeCells('C4:C5');
    $sheet->getActiveSheet()->mergeCells('D4:D5');

    $sheet->getActiveSheet()->mergeCells('B'.strval($baris+1).':B'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('C'.strval($baris+1).':C'.strval($baris+2));
    $sheet->getActiveSheet()->mergeCells('D'.strval($baris+1).':D'.strval($baris+2));

    //khusus footer
    $sheet->getActiveSheet()->getStyle('B'.strval($baris+1).':D'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('B'.strval($baris+2).':D'.strval($baris+2))->applyFromArray($styleContent);

    $sheet->getActiveSheet()->getStyle('B6:B'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('C6:C'.strval($baris+1))->applyFromArray($styleContent);
    $sheet->getActiveSheet()->getStyle('D6:D'.strval($baris+1))->applyFromArray($styleContent);

    $sheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $sheet->getActiveSheet()->getColumnDimension('C')->setWidth(38);
    $sheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);

    //$this->setFileName($sheet);
  }

  public function setFileName($sheet){
    //file name setting
    $writer = new Xlsx($sheet);
    $filename = 'Laporan-'.now('Asia/Jakarta');

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
    header('Cache-Control: max-age=0');
    $writer->save('php://output');
  }

}


/* End of file Export.php */
/* Location: ./application/controllers/Export.php */