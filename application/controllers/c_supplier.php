<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_supplier extends CI_Controller
    {
        public function index(){
            $send['site'] = "supplier";
            $header['title'] = "Supplier";
            $this->db->select("*");
            $this->db->from("tb_supplier");
            $this->db->where("supplier_is_delete","0");
            $send['result'] = $this->db->get()->result();

            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('data/supplier/supplier');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $send['site'] = "supplier";
            $header['title'] = "Tambah Supplier";

            $this->form_validation->set_error_delimiters('<div class="text-credit col-4 p-0 m-0">', '</div>');
            $this->form_validation->set_rules('name','Nama supplier','required',
                array(
                    'required' => '%s harus diisi'
                )
            );

            $id = 'SUP-'.now('Asia/Jakarta');
            
            if ($this->form_validation->run() == true) {
                $data = [
                    'supplier_id' => $id,
                    'supplier_name' => $this->input->post('name'),
                    'supplier_address' => $this->input->post('alamat'),
                    'supplier_telphone' => $this->input->post('telephone'),
                    'supplier_insert_date' => date("Y-m-d h:i:s"),
                    'supplier_is_delete' => '0'
                ];
                $this->db->insert("tb_supplier",$data);
                redirect('index.php/c_supplier/');

            }else {
                $this->load->view('header-footer/header', $header);
                $this->load->view('sidebar-topbar/side', $send);
                $this->load->view('data/supplier/input');
                $this->load->view('header-footer/footer');
            }
            
        }

        public function edit($id){
            $send['site'] = "supplier";
            $header['title'] = "Edit Supplier";
            $this->db->select("supplier_id");
            $this->db->from("tb_supplier");
            $this->db->order_by("supplier_id","DESC");
            $this->db->limit(1);
            $send['data'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_supplier");
            $this->db->where("supplier_id", $id);
            $send['stored'] = $this->db->get()->result();

            $this->form_validation->set_rules('nama', 'Nama supplier', 'required');
            if ($this->form_validation->run() == true) {
                
                $data = [
                    'supplier_name' => $this->input->post('nama'),
                    'supplier_address' => $this->input->post('alamat'),
                    'supplier_telphone' => $this->input->post('telephone'),
                    'supplier_last_update_date' => date("Y-m-d h:i:s"),
                ];
                $this->db->where("supplier_id", $id);
                $this->db->update("tb_supplier", $data);
                redirect('index.php/c_supplier/');

            }else {
                $this->load->view('header-footer/header', $header);
                $this->load->view('sidebar-topbar/side',$send);
                $this->load->view('data/supplier/edit');
                $this->load->view('header-footer/footer');    
            }

        }

        public function delete($id){
            $this->db->set("supplier_is_delete","1");
            $this->db->where("supplier_id",$id);
            $this->db->update("tb_supplier");

            redirect('index.php/c_supplier/');
        }
    }

?>