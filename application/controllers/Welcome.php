<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


	public function __construct()
	{
		parent::__construct();

		$this->load->model('Main_model');
	}

	public function index()
	{
		$data['site'] = "dashboard";
		$header['title'] = "Dashboard";

		$data['count_trans_not_sent'] = $this->Main_model->transTakTerkirim();
		$data['count_trans_in'] = $this->Main_model->totalTransIn();
		$data['count_trans_out'] = $this->Main_model->totalTransOut();
		$data['minimum'] = $this->Main_model->get_data('tb_material', 'WHERE material_is_delete = \'0\' AND material_stock <= material_min_stock');
		$data['keluar'] = $this->Main_model->transOutBulanIni();
		$data['masuk'] = $this->Main_model->transInBulanIni();

		$e = $this->Main_model->pendapatan();
		$data['pendapatan'] = json_encode($e);

		$this->load->view('header-footer/header', $header);
		$this->load->view('sidebar-topbar/side', $data);
		$this->load->view('dashboard/dashboard2');
		$this->load->view('header-footer/footer');
	}

	public function login()
	{	
		$header['title'] = "Login";
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true) {
			$data['username'] = $this->input->post('username', true);
			$data['password'] = $this->input->post('password', true);

			if ($data['username'] == 'admin' && $data['password'] == 'admin') {
				$session_data = array(
					'name' => $data['username'],
					'user_level' => 'admin'
				);
				$this->session->set_userdata($session_data);
				redirect('index.php/Welcome');
			}else if ($data['username'] == 'kasir' && $data['password'] == 'kasir') {
				$session_data = array(
					'name' => $data['username'],
					'user_level' => 'kasir'
				);
				$this->session->set_userdata($session_data);
				redirect('index.php/Welcome');
			}else {
				
			}

		}else {
			
		}

		$this->load->view('header-footer/header', $header);
		$this->load->view('login');
	}

	public function logout()
	{
		$session_data = array(
			'user_level' => null
		);
		redirect(base_url("index.php/Welcome/login"));  
	}
}
