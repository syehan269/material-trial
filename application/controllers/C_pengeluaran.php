<?php
defined('BASEPATH') or exit('No direct script access allowed');
// Don't forget include/define REST_Controller path

class c_pengeluaran extends CI_Controller
{
    
    public function __construct(){
        parent::__construct();
    }

    public function index(){

        $send['site'] = "pengeluaran";
        $header['title'] = "Pengeluaran";

        $this->db->select("*");
        $this->db->where("is_deleted","0");
        $this->db->from("tb_pengeluaran");
        $send["pengeluaran"] = $this->db->get()->result();

        $this->load->view('header-footer/header', $header);
        $this->load->view('sidebar-topbar/side', $send);
        $this->load->view('data/pengeluaran/pengeluaran');
        $this->load->view('header-footer/footer');
        $this->load->view('function');
    }

    public function input(){
        $send['site'] = 'pengeluaran';
        $getName = $this->input->post('addName');
        $getAmount = $this->input->post('addAmount');
        $id = 'EXP-'.now('Asia/Jakarta');

        $data = [
            'pengeluaran_id' => $id,
            'pengeluaran_nama' => $getName,
            'pengeluaran_amount' => $getAmount,
            'created_at' => date("Y-m-d h:i:s")
        ];

        $this->db->insert('tb_pengeluaran', $data);
        redirect('index.php/c_pengeluaran');

    }

    public function edit($id){
        $getName = $this->input->post('editName');
        $getAmount = $this->input->post('editAmount');
        $data = [
            'pengeluaran_nama' => $getName,
            'pengeluaran_amount' => $getAmount
        ];

        $this->db->where('pengeluaran_id',$id);
        $this->db->update('tb_pengeluaran',$data);
        redirect('index.php/c_pengeluaran');
    }

    public function delete($id){
        $this->db->set("is_deleted","1");
        $this->db->where("pengeluaran_id",$id);
        $this->db->update("tb_pengeluaran");

        redirect('index.php/c_pengeluaran/');
    }

}


/* End of file C_pengeluaran.php */
/* Location: ./application/controllers/C_pengeluaran.php */