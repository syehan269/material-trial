<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_satuan extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
            $this->load->model('Main_model');
            date_default_timezone_set('Asia/Jakarta');
        }

        public function index(){
            $send['site'] = "satuan";
            $header['title'] = "Satuan";

            //get data
            $this->db->select("*");
            $this->db->from("tb_satuan");
            $this->db->where("satuan_is_delete","0");
            $send['result'] = $this->db->get()->result();

            //input data
            $this->db->select("satuan_id");
            $this->db->from("tb_satuan");
            $this->db->order_by("satuan_id","DESC");
            $this->db->limit(1);
            $send['data'] = $this->db->get()->result();

            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('data/satuan/satuan');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $send['site'] = "satuan";

            $this->form_validation->set_rules('namaAtas','Nama Satuan Atas','required');
            $this->form_validation->set_rules('namaBawah','Nama Satuan Bawah','required');
            $id = 'STN-'.now('Asia/Jakarta');

            if ($this->form_validation->run() == true) {
                $data = [
                    'satuan_id' => $id,
                    'satuan_atas' => $this->input->post('namaAtas'),
                    'satuan_bawah' => $this->input->post('namaBawah'),
                    'created_at' => date("Y-m-d h:i:s"),
                    'satuan_is_delete' => '0'
                ];
                $this->db->insert('tb_satuan', $data);
                redirect('index.php/c_satuan');
            }else {
                $this->index();
                //redirect('index.php/c_satuan');
            }
        }

        public function edit(){
            $id = $this->input->post('id');
            $satuan_atas = $this->input->post('satuan_atas');
            $satuan_bawah = $this->input->post('satuan_bawah');
            $data = $this->Main_model->update_data('tb_satuan', "satuan_atas = '$satuan_atas', satuan_bawah = '$satuan_bawah' WHERE satuan_id = '$id'");
            echo json_encode($data);
        }

        public function delete($id){
            
            $this->db->set("satuan_is_delete","1");
            $this->db->where("satuan_id",$id);
            $this->db->update("tb_satuan");

            redirect('index.php/c_satuan/');
        }

    }
    

?>