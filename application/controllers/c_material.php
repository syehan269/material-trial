<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_material extends CI_Controller
    {
        public function index(){
            $send['site'] = "material";
            $header['title'] = "Material";
            $this->db->select("*");
            $this->db->from("tb_material");
            $this->db->where("material_is_delete","0");
            $send['result'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_satuan");
            $this->db->where("satuan_is_delete","0");
            $send['satuan'] = $this->db->get()->result();

            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('data/material/material');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $send['site'] = "material";
            $header['title'] = "Tambah Material";

            $this->db->select("*");
            $this->db->from("tb_satuan");
            $this->db->where("satuan_is_delete","0");
            $send['satuan'] = $this->db->get()->result();
            
            $this->form_validation->set_error_delimiters('<div class="text-credit col-4 p-0 my-1">', '</div>');
            $this->form_validation->set_rules('name','Nama supplier','required',
                array(
                    'required' => '%s harus diisi'
                )
            );
            /*$this->form_validation->set_rules('satuan','Satuan material','required',
                array(
                    'required' => '%s harus diisi'
                )
            );*/
            $this->form_validation->set_rules('hBeli','Harga beli','required',
                array(
                    'required' => '%s harus diisi'
                )
            );
            $this->form_validation->set_rules('hReseller','Harga reseller','required',
                array(
                    'required' => '%s harus diisi'
                )
            );
            $this->form_validation->set_rules('hEcer','Harga ecer','required',
                array(
                    'required' => '%s harus diisi'
                )
            );
            $this->form_validation->set_rules('hGrosir','Harga grosir','required',
                array(
                    'required' => '%s harus diisi'
                )
            );
            $this->form_validation->set_rules('minStock','Minimal stock','required',
                array(
                    'required' => '%s harus diisi'
                )
            );
            $this->form_validation->set_rules('minTrans','Minimal transaksi','required',
                array(
                    'required' => '%s harus diisi'
                )
            );
            $this->form_validation->set_rules('sAtas','Satuan atas','required',
                array(
                    'required' => '%s harus diisi'
                )
            );
            $this->form_validation->set_rules('sBawah','Satuan bawah','required',
                array(
                    'required' => '%s harus diisi'
                )
            );
            $this->form_validation->set_rules('isiRatio','Ratio Satuan','required',
                array(
                    'required' => '%s harus diisi'
                )
            );

            $getSat = $this->input->post('satuan');
            $getRaw = explode('|', $getSat);
            $id = 'MTR-'.now('Asia/Jakarta');

            if ($this->form_validation->run() == true) {
                $data = [
                    'material_id' => $id,
                    'material_name' => $this->input->post('name'),
                    // 'material_satuan' => $getRaw[1],
                    // 'material_satuan_amount' => $getRaw[0],
                    'material_harga_beli' => $this->input->post('hBeli'),
                    'material_harga_reseller' => $this->input->post('hReseller'),
                    'material_harga_agen' => $this->input->post('hEcer'),
                    'material_harga_grosir' => $this->input->post('hGrosir'),
                    'material_min_stock' => $this->input->post('minStock'),
                    'material_min_trans' => $this->input->post('minTrans'),
                    'material_sat_atas' => $this->input->post('sAtas'),
                    'material_sat_bawah' => $this->input->post('sBawah'),
                    'material_sat_ratio' => $this->input->post('isiRatio'),
                    'material_last_update_date' => date("Y-m-d h:i:s"),
                    'material_is_delete' => '0'
                ];
                $this->db->insert("tb_material", $data);
                redirect('index.php/c_material');
            }else {
                //get data
                $this->db->select("*");
                $this->db->from("tb_satuan");
                $this->db->where("satuan_is_delete","0");
                $send['pale'] = $this->db->get()->result();

                $this->load->view('header-footer/header', $header);
                $this->load->view('sidebar-topbar/side', $send);
                $this->load->view('data/material/input');
                $this->load->view('header-footer/footer');   
            }
        }

        public function edit($id){
            $send['id'] = $this->input->get('id');
            $send['site'] = "material";
            $header['title'] = "Edit Material";
            $this->db->select("material_id");
            $this->db->from("tb_material");
            $this->db->order_by("material_id","DESC");
            $this->db->limit(1);
            $send['data'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_material");
            $this->db->where("material_id", $id);
            $send['stored'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_satuan");
            $this->db->where("satuan_is_delete","0");
            $send['satuan'] = $this->db->get()->result();

            //get data
            $this->db->select("*");
            $this->db->from("tb_satuan");
            $this->db->where("satuan_is_delete","0");
            $send['pale'] = $this->db->get()->result();

            $this->form_validation->set_rules('name','Nama material','required');
            //$this->form_validation->set_rules('satuan','Nama supplier','required');
            $this->form_validation->set_rules('hBeli','Harga beli material','required');
            $this->form_validation->set_rules('hReseller','Harga reseller material','required');
            $this->form_validation->set_rules('hEcer','Harga ecer material','required');
            $this->form_validation->set_rules('hGrosir','Harga grosir material','required');
            $this->form_validation->set_rules('minStock','Minimal stock material','required');
            $this->form_validation->set_rules('minTrans','Minimal transaksi material','required');
            $this->form_validation->set_rules('sAtas','Satuan atas material','required');
            $this->form_validation->set_rules('sBawah','Satuan bawah material','required');
            
            if ($this->form_validation->run() == true) {
                $data = [
                    'material_name' => $this->input->post('name'),
                    //'material_satuan' => $this->input->post('satuan'),
                    'material_harga_beli' => $this->input->post('hBeli'),
                    'material_harga_reseller' => $this->input->post('hReseller'),
                    'material_harga_agen' => $this->input->post('hEcer'),
                    'material_harga_grosir' => $this->input->post('hGrosir'),
                    'material_min_stock' => $this->input->post('minStock'),
                    'material_min_trans' => $this->input->post('minTrans'),
                    'material_sat_atas' => $this->input->post('sAtas'),
                    'material_sat_bawah' => $this->input->post('sBawah'),
                    'material_last_update_date' => date("Y-m-d h:i:s"),
                ];
                $this->db->where("material_id",$id);
                $this->db->update("tb_material",$data);
                redirect('index.php/c_material');
            }else {
                $this->load->view('header-footer/header', $header);
                $this->load->view('sidebar-topbar/side', $send);
                $this->load->view('data/material/edit');
                $this->load->view('header-footer/footer');
            }

        }

        public function update_stock(){
            $getId = $this->input->post("newId");
            $getStok = $this->input->post("newAmount");
            $getRatio = $this->input->post("newRatio");

            $this->db->set("material_stock",$getStok);
            $this->db->set("material_sat_ratio",$getRatio);
            $this->db->where("material_id",$getId);
            $this->db->update("tb_material");

            redirect("index.php/c_material");
        }

        public function delete($id){
            $this->db->set("material_is_delete","1");
            $this->db->where("material_id",$id);
            $this->db->update("tb_material");

            redirect('index.php/c_material/');
        }

    }
    

?>