<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_installment extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
            $this->load->model('Main_model');
            date_default_timezone_set('Asia/Jakarta');
        }

        public function index(){
            $send['site'] = "piutang";
            $header['title'] = "Hutang Pelanggan";

            $data['piutang'] = $this->Main_model->get_data('tb_trans_out', "WHERE trans_out_payment_type = 'Credit' AND trans_out_is_delete = '0'");
            
            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('installment/installment', $data);
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function getCicilan($id){
            $this->db->select('*');
            $this->db->from('tb_credit');
            $this->db->where('credit_trans_out_id',$id);
            $data['data'] = $this->db->get()->result();
            echo json_encode($data);
        }

        public function update_cicilan(){
            $cicilanTotal = $this->input->post("cicilan-total");
            $transId = $this->input->post("id-transaksi");
            $isCompleted = 0;
            $uangCicilan = 0;
            
            $cicilan = [];

            for ($i=0; $i < $cicilanTotal; $i++) { 

                $fieldId = "cicilan-".intval($i+1);
                $tanggalId = "tanggal-".intval($i+1);
                $pembayaran = $this->input->post($fieldId);
                $tanggal = $this->input->post($tanggalId);
                
                $uangCicilan = $uangCicilan + $pembayaran;

                if ($tanggal == null) {
                    $tanggal = date('y-m-d H:i:s');
                }

                array_push($cicilan, [
                    'credit_trans_out_id' => $transId,
                    'credit_amount' => $pembayaran,
                    'credit_paid_date' => $tanggal,
                    'credit_additional_info' => 'Cicilan ke-'.intval($i+1)
                ]);
            }

            $this->db->where('credit_trans_out_id',$transId);
            $this->db->delete('tb_credit');

            $this->db->insert_batch('tb_credit',$cicilan);

            $this->db->select("trans_out_payment_amount");
            $this->db->from("tb_trans_out");
            $this->db->where('trans_out_id',$transId);
            $res = $this->db->get()->result();

            $tagihan = $res[0]->trans_out_payment_amount;

            if ($uangCicilan == $tagihan) {
                $isCompleted = 1;
            }

            $data = [
                'trans_out_complete' => $isCompleted,
                'trans_out_paid_amount' => $uangCicilan
            ];
            $this->db->where("trans_out_id",$transId);
            $this->db->update("tb_trans_out",$data);

            redirect('index.php/c_installment/');

        }

    }
    

?>