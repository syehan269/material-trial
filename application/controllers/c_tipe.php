<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_tipe extends CI_Controller
    {
        public function index(){
            $data['site'] = "tipe";
            $header['title'] = "Tipe";
            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $data);
            $this->load->view('data/tipe/tipe');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $data['site'] = "tipe";
            $header['title'] = "Tambah Tipe";
            $send['id'] = $this->input->get('id');
            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $data);
            $this->load->view('data/tipe/input');
            $this->load->view('header-footer/footer');
        }

        public function edit(){
            $send['id'] = $this->input->get('id');
            $send['site'] = "tipe";
            $header['title'] = "Edit Tipe";
            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('data/tipe/edit');
            $this->load->view('header-footer/footer');
        }

    }

?>