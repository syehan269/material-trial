<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_customer extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
            $this->load->model('Main_model');
            date_default_timezone_set('Asia/Jakarta');
        }

        public function index(){
            $data['site'] = "customer";
            $header['title'] = "Pelanggan";
            $this->db->select("*");
            $this->db->from("tb_customer");
            $this->db->where("customer_is_delete","0");
            $data['result'] = $this->db->get()->result();

            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $data);
            $this->load->view('data/customer/customer');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        function get_data_by_id(){
            $id = $this->input->get('id');
            $customer_data = $this->Main_model->get_customer_by_code($id);
            $dataArray = array(
                'customer_data' => $customer_data
            );
            echo json_encode($dataArray);
        }

        public function input(){
            $data['site'] = "customer";
            $header['title'] = "Tambah Pelanggan";
            $this->db->select("customer_id");
            $this->db->from("tb_customer");
            $this->db->order_by("customer_id","DESC");
            $this->db->limit(1);
            $data['data'] = $this->db->get()->result();
            $id = 'CUS-'.now('Asia/Jakarta');
            $name = $this->input->post('name');

            $this->form_validation->set_error_delimiters('<div class="text-credit col-4 p-0 m-0">', '</div>');
            $this->form_validation->set_rules('name','Nama pelanggan','required|is_unique[tb_customer.customer_name]',
                array(
                    'required' => 'Field pelanggan tidak boleh kosong',
                    'is_unique' => '%s duplikat'
                )
            );
            $this->form_validation->set_rules('type','Tipe pelanggan','required',
                array(
                    'required' => 'Field tipe pelanggan tidak boleh kosong'
                )
            );

            if ($this->form_validation->run()) {
                $data = [
                    'customer_id' => $id,
                    'customer_name' => $this->input->post('name'),
                    'customer_address' => $this->input->post('alamat'),
                    'customer_type' => $this->input->post('type'),
                    'customer_telephone' => $this->input->post('telephone'),
                    'customer_saldo' => $this->input->post('saldo'),
                    'customer_additional_info' => $this->input->post('info'),
                    'customer_insert_date' => date("Y-m-d h:i:s"),
                    'customer_is_delete' => '0'
                ];
                $this->db->insert("tb_customer",$data);
                redirect('index.php/c_customer/');
            }else {
                $this->load->view('header-footer/header', $header);
                $this->load->view('sidebar-topbar/side', $data);
                $this->load->view('data/customer/input');
                $this->load->view('header-footer/footer');                
            }

        }

        public function update_saldo($id){
            $send['site'] = "customer";

            $this->form_validation->set_rules('newAmount','Saldo','required');
            if ($this->form_validation->run() == true) {
                $data =[          
                    'customer_saldo' => $this->input->post('newAmount'),
                ];
                $this->db->where("customer_id",$id);
                $this->db->update("tb_customer",$data);
                redirect('index.php/c_customer');
            }else {
                redirect('index.php/c_customer');
            }
        }

        public function edit($id){
            $send['id'] = $this->input->get('id');
            $send['site'] = "customer";
            $header['title'] = "Edit Pelanggan";
            $this->db->select("customer_id");
            $this->db->from("tb_customer");
            $this->db->order_by("customer_id","DESC");
            $this->db->limit(1);
            $send['data'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_customer");
            $this->db->where("customer_id", $id);
            $send['stored'] = $this->db->get()->result();

            $this->form_validation->set_rules('name', 'Nama customer', 'required');
            $this->form_validation->set_rules('type', 'Type supplier', 'required');
            if ($this->form_validation->run() == true) {
                $data = [
                    'customer_name' => $this->input->post('name'),
                    'customer_address' => $this->input->post('address'),
                    'customer_type' => $this->input->post('type'),
                    'customer_telephone' => $this->input->post('telephone'),
                    'customer_additional_info' => $this->input->post('info'),
                    'customer_last_update_date' => date("Y-m-d h:i:s"),
                    'customer_is_delete' => '0'
                ];
                $this->db->where("customer_id", $id);
                $this->db->update("tb_customer", $data);
                redirect('index.php/c_customer/');
            }else {
                $this->load->view('header-footer/header', $header);
                $this->load->view('sidebar-topbar/side', $send);
                $this->load->view('data/customer/edit');
                $this->load->view('header-footer/footer');
            }

        }

        public function delete($id){
            $this->db->set("customer_is_delete","1");
            $this->db->where("customer_id",$id);
            $this->db->update("tb_customer");
            
            redirect('index.php/c_customer/');
        }

    }

?>