<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_laporan_supplier extends CI_Controller
{
    
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Report_In_model');
		$this->load->model('Trans_Out_model');
	}

	public function index()
	{
		$header['title'] = 'Laporan Supplier';
		$data['site'] = "transaction_in";

		$this->db->select("*");
		$this->db->from("tb_trans_in");
		$this->db->where("trans_in_is_delete","0");
		$data['result'] = $this->db->get()->result();

		$this->db->select("*");
		$this->db->from("tb_detail_trans_in");
		$data['id_material'] = $this->db->get()->result();

		$this->db->select("*");
		$this->db->from("tb_material");
		$this->db->where("material_is_delete","0");
		$data['material'] = $this->db->get()->result();

		$this->load->view('header-footer/header', $header);
		$this->load->view('sidebar-topbar/side', $data);
		$this->load->view('laporan/supplier');
		$this->load->view('header-footer/footer');
		$this->load->view('function');
	}

}
