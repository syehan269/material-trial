<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_debt extends CI_Controller
    {

        public function __construct()
        {
            parent::__construct();
            $this->load->database();
            $this->load->model('Main_model');
            date_default_timezone_set('Asia/Jakarta');
        }


        public function index(){
            $send['site'] = "utang";
            $header['title'] = "Hutang Instansi";
            $this->db->select("*");
            $this->db->from("tb_trans_in");
            $this->db->where("trans_in_is_delete","0");
            $this->db->where("trans_in_payment_type","Credit");
            $send['result'] = $this->db->get()->result();

            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('debt/debt');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $send['site'] = "utang";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('debt/input');
            $this->load->view('header-footer/footer');
        }

        public function getCicilan($id){
            $this->db->select('*');
            $this->db->from('tb_utang');
            $this->db->where('utang_trans_in_id',$id);
            $data['data'] = $this->db->get()->result();
            echo json_encode($data);
        }

        public function update_cicilan(){
            $cicilanTotal = $this->input->post("cicilan-total");
            $transId = $this->input->post("id-transaksi");
            $isCompleted = 0;
            $uangCicilan = 0;
            
            $cicilan = [];

            for ($i=0; $i < $cicilanTotal; $i++) { 

                $fieldId = "cicilan-".intval($i+1);
                $tanggalId = "tanggal-".intval($i+1);
                $pembayaran = $this->input->post($fieldId);
                $tanggal = $this->input->post($tanggalId);
                
                $uangCicilan = $uangCicilan + $pembayaran;

                if ($tanggal == null) {
                    $tanggal = date('y-m-d H:i:s');
                }

                array_push($cicilan, [
                    'utang_trans_in_id' => $transId,
                    'utang_amount' => $pembayaran,
                    'utang_paid_date' => $tanggal,
                    'utang_information' => 'Cicilan ke-'.intval($i+1)
                ]);
            }

            $this->db->where('utang_trans_in_id',$transId);
            $this->db->delete('tb_utang');

            $this->db->insert_batch('tb_utang',$cicilan);

            $this->db->select("trans_in_payment_amount");
            $this->db->from("tb_trans_in");
            $this->db->where('trans_in_id',$transId);
            $res = $this->db->get()->result();

            $tagihan = $res[0]->trans_in_payment_amount;

            if ($uangCicilan == $tagihan) {
                $isCompleted = 1;
            }

            $data = [
                'trans_in_complete' => $isCompleted,
                'trans_in_paid_amount' => $uangCicilan
            ];

            $this->db->where("trans_in_id",$transId);
            $this->db->update("tb_trans_in",$data);
            
            redirect('index.php/c_debt/');
        }

    }
    
?>