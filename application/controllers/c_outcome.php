<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_outcome extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
            $this->load->model('Main_model');
            date_default_timezone_set('Asia/Jakarta');
        }

        public function index(){
            $send['site'] = "transaction_out";
            $header['title'] = "Data Penjualan";
            $this->db->select("*");
            $this->db->from("tb_trans_out");
            $this->db->where("trans_out_is_delete","0");
            $send['result'] = $this->db->get()->result();

            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('transaction/outcome/outcome');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        function get_data(){
            $material_data = $this->Main_model->get_data("tb_material", "WHERE material_is_delete = '0'");
            $customer_data = $this->Main_model->get_data("tb_customer", "WHERE customer_is_delete = '0'");
            $dataArray = array(
                'material_data' => $material_data,
                'customer_data' => $customer_data
            );
            echo json_encode($dataArray);
        }

        public function input(){
            $send['site'] = "transaction_out";
            $header['title'] = "Kasir Penjualan";

            $this->db->select("*");
            $this->db->from("tb_rekening");
            $this->db->where("is_deleted_rekening","0");
            $send['rekening'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_material");
            $this->db->where("material_is_delete","0");
            $send['material'] = $this->db->get()->result();

            $this->db->select("*");
            $this->db->from("tb_customer");
            $this->db->where("customer_is_delete","0");
            $send['customer'] = $this->db->get()->result();

            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('transaction/outcome/input2');
            $this->load->view('header-footer/footer');
        }

        public function input_all(){

            $valId = 'TOU-'.now('Asia/Jakarta');
            $getInfo = $this->input->post("info-item");
            $getName = $this->input->post("backCustomer");
            $getMethod = $this->input->post("method-item");
            $getSales = $this->input->post("sales-item");
            $getState = $this->input->post("state-item");

            $getBank = '';
            $getRekening = '';
            $getPaidAmount = 0;
            $getComp = 1;

            $getAlamat = $this->input->post("backAlamat");
            $getSaldo = $this->input->post("backSaldo");
            $getPoin = $this->input->post("backMember");
            $getCustId = $this->input->post("backCustomerId");

            $getTotalAmount = $this->input->post("materialAmount");
            $getMatAmount = $this->input->post("materialItem");

            $tanggalDeadline = $this->input->post("text-cicilan");
            $getAmount1 = 0;

            $materialBundle = array();
            
            if ($getMethod == "Cash") {
                $getPaidAmount = $this->input->post("paid-item");
            }else if($getMethod == "Credit") {
                $getPaidAmount = $this->input->post("paid-item");
                $getComp = 0;
            }elseif ($getMethod == "ATM") {
                $getBank = $this->input->post("bank-item");
                $getRekening = $this->input->post("rekening-no-item");
            }else {
                $getPaidAmount = $getTotalAmount;
            }

            //input credit
            if ( $getMethod == "Credit") {
                
                $pembayaranAwal = $this->input->post('saldo-cicilan');
                if (strlen($pembayaranAwal) > 0) {
                    $this->db->insert('tb_credit',[
                        'credit_trans_out_id' => $valId,
                        'credit_amount' => $pembayaranAwal,
                        'credit_additional_info' => 'Cicilan ke-1',
                        'credit_paid_date' => date('y-m-d H:i:s'),
                        'credit_insert_date' => date('y-m-d H:i:s')
                    ]);
                }

            }

            //update saldo
            if ($getSaldo > 0) {
                $quant = "";
                $this->db->select("customer_saldo");
                $this->db->from("tb_customer");
                $this->db->where("customer_id",$getCustId);
                $quant = $this->db->get()->result();
                $finQuant = $quant[0]->customer_saldo - $getTotalAmount;
                
                $this->db->set("customer_saldo",$finQuant);
                $this->db->where("customer_id",$getCustId);
                $this->db->update("tb_customer");
            }

            //update member poin
            if ($getPoin != "-") {
                $quant = "";
                $this->db->select("member_poin");
                $this->db->from("tb_membership");
                $this->db->where("member_customer_id",$getCustId);
                $quant = $this->db->get()->result();
                $finQuant = $quant[0]->member_poin + $getPoin;
                
                $this->db->set("member_poin",$finQuant);
                $this->db->where("member_customer_id",$getCustId);
                $this->db->update("tb_membership");
            }

            for ($i=0; $i < $getMatAmount; $i++) {
                $id = $i + 1;
                $matId = "materialBack".$id;
                $idId = "idBack".$id;
                $quantId = "quantBack".$id;
                $totalId = "totalBack".$id;
                $priceId = "priceBack".$id;
                
                $valMat = $this->input->post($matId);
                $valMatId = $this->input->post($idId);
                $valPri = $this->input->post($priceId);
                $valTot = $this->input->post($totalId);
                $valQua = $this->input->post($quantId);

                array_push($materialBundle, array(
                    'detail_out_trans_out_id' => $valId,
                    'detail_out_material_id' => $valMatId,
                    'detail_out_material_name' => $valMat,
                    'detail_out_material_price' => $valPri,
                    'detail_out_material_amount' => $valQua
                ));
                
                //get material stock
                $quant = "";
                $this->db->select("material_stock");
                $this->db->from("tb_material");
                $this->db->where("material_id",$valMatId);
                $quant = $this->db->get()->result();

                //update stock
                $finQuant = $quant[0]->material_stock - $valQua;
                
                $this->db->set("material_stock",$finQuant);
                $this->db->where("material_id",$valMatId);
                $this->db->update("tb_material");
            }

            $data = [
                'trans_out_id' => $valId,
                'trans_out_customer_id' =>$getCustId,
                'trans_out_customer_name' => $getName,
                'trans_out_payment_amount' => $getTotalAmount,
                'trans_out_paid_amount' => $getPaidAmount,
                'trans_out_payment_type' => $getMethod,
                'trans_out_delivery_type' => $getSales,
                'trans_out_complete' => $getComp,
                'trans_out_bank' => $getBank,
                'trans_out_rekening' => $getRekening,
                'trans_out_delivery_address' => $getAlamat,
                'trans_out_due_date' => $tanggalDeadline,
                'trans_out_sent_status' => '0',
                'trans_out_sent_date' => '',
                'trans_out_additional_info' => $getInfo,
                'trans_out_insert_date' => date("Y-m-d h:i:s"),
                'trans_out_is_delete' => '0',
            ];

            $this->db->insert('tb_trans_out', $data);
            $this->db->insert_batch("tb_detail_trans_out", $materialBundle);

            redirect('index.php/c_outcome/input');
        }

        public function edit(){
            $send['site'] = "transaction_out";
            $header['title'] = "Edit Penjualan";
            $send['id'] = $this->input->get('getId');
            $this->load->view('header-footer/header', $header);
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('transaction/outcome/edit');
            $this->load->view('header-footer/footer');
        }

        public function store(){
            $id = 'TOU-'.now('Asia/Jakarta');
            $diantar = $this->input->post('diantar');
            $status = $this->input->post('type-pembeli');
            $type = $this->input->post('tipe-pembeli');
            $saldo = $this->input->post('saldo-pembeli');
            $metode = $this->input->post('metode-pembayaran');
            $termPayment = "";
            $terbayar = $this->input->post('uang-terbayar');
            $isComplete = 1;
            $alamat = "";
            $ongkir = 0;
            $pembelian = "Takeaway";
            $bank = "";
            $rekening = "";
            $poin = $this->input->post('poin');
            $rawTotal = $this->input->post('total-pembayaran');
            $membership = "";
            $materialBundle = [];
            $matCount = $this->input->post('jumlah-material');

            $raw = explode(" ",$rawTotal);
            $total = str_replace('.','',$raw[1]);

            if ($status == "db") {
                $rawPembeli = $this->input->post('pembeli');
                $raw = explode("|",$rawPembeli);
                $pembeli = $raw[5];
                $custId = $raw[4];
                $membership = $raw[0];
            } else {
                $custId = "";
                $pembeli = $this->input->post('pembeli-field');
            }
            
            if ($metode == "Credit") {
                $isComplete = 0;
                $termPayment = $this->input->post('term-payment');

                if ($terbayar >= 0) {
                    $this->db->insert('tb_credit',[
                        'credit_trans_out_id' => $id,
                        'credit_amount' => $terbayar,
                        'credit_additional_info' => "Cicilan ke-1",
                        'credit_insert_date' => date('y-m-d h:i:s')
                    ]);
                }

            }elseif ($metode == "ATM") {
                $rawRekening = $this->input->post('rekening');
                $raw = explode(" - ",$rawRekening);
                $rekening = $raw[1];
                $bank = $raw[0];

            }elseif ($metode == "Saldo"){
                if ($saldo >= $total) {
                    $this->db->select('customer_saldo');
                    $this->db->from('tb_customer');
                    $this->db->where('customer_id',$custId);
                    $saldoCust = $this->db->get()->result();

                    $saldo = $saldo - $total;
                    $this->db->set('customer_saldo',$saldo);
                    $this->db->where('customer_id',$custId);
                    $this->db->update('tb_customer');
                }

            }

            if ($diantar == 1) {
                $pembelian = "Delivery";
                $alamat = $this->input->post('alamat-pengiriman');
                $ongkir = $this->input->post('ongkos-pengiriman');
                $total = $total + $ongkir;
            }

            if ($status == "db" && $membership == 1) {
                $this->db->select('member_poin');
                $this->db->from('tb_membership');
                $this->db->where('member_customer_id',$custId);
                $poinMember = $this->db->get()->result();

                $poin = $poin+$poinMember[0]->member_poin;
                $this->db->set('member_poin',$poin);
                $this->db->where('member_customer_id',$custId);
                $this->db->update('tb_membership');
            }

            for ($i=0; $i < $matCount; $i++) { 
                $con = $i+1;

                $matId = $this->input->post('id-'.$con);
                $nama = $this->input->post('nama-'.$con);
                $jumlah = $this->input->post('jumlah-'.$con);
                $retail = $this->input->post('Retail-'.$con);
                $reseller = $this->input->post('Reseller-'.$con);
                $grosir = $this->input->post('Grosir-'.$con);
                $min = $this->input->post('min-'.$con);
                
                if ($type == "Reseller") {
                    if (intval($jumlah) >= intval($min)) {
                        $price = $grosir;
                    }
                    $price = $reseller;
                } else {
                    if (intval($jumlah) >= intval($min)) {
                        $price = $grosir;
                    }
                    $price = $retail;
                }
                
                array_push($materialBundle, [
                    'detail_out_trans_out_id' => $id,
                    'detail_out_material_id' => $matId,
                    'detail_out_material_name' => $nama,
                    'detail_out_material_price' => $price,
                    'detail_out_material_amount' => $jumlah
                ]);

                //get material stock
                $quant = "";
                $this->db->select("material_stock");
                $this->db->from("tb_material");
                $this->db->where("material_id",$matId);
                $quant = $this->db->get()->result();

                //update stock
                $finJumlah = $quant[0]->material_stock - $jumlah;
                
                $this->db->set("material_stock",$finJumlah);
                $this->db->where("material_id",$matId);
                $this->db->update("tb_material");
            }

            $this->db->insert('tb_trans_out',[
                'trans_out_id' => $id,
                'trans_out_customer_id' => $custId,
                'trans_out_customer_name' => $pembeli,
                'trans_out_payment_amount' => $total,
                'trans_out_paid_amount' => $terbayar+$ongkir,
                'trans_out_payment_type' => $metode,
                'trans_out_delivery_type' => $pembelian,
                'trans_out_complete' => $isComplete,
                'trans_out_bank' => $bank,
                'trans_out_rekening' => $rekening,
                'trans_out_delivery_address' => $alamat,
                'trans_out_due_date' => $termPayment,
                'trans_out_sent_date' => date('y-m-d h:i:s'),
                'trans_out_insert_date' => date('y-m-d h:i:s'),
            ]);

            $this->db->insert_batch("tb_detail_trans_out", $materialBundle);
            redirect('index.php/c_outcome/input');
            
        }

        public function get_detail(){
            $detail = $this->Main_model->get_data('tb_detail_trans_out'," ");
            $dataArray = array(
                'data_detail' => $detail
            );
            echo json_encode($dataArray);
        }

        public function delete($id){
            $this->db->set("trans_out_is_delete","1");
            $this->db->where("trans_out_id",$id);
            $this->db->update("tb_trans_out");
            redirect('index.php/c_outcome/');
        }

    }
    
?>