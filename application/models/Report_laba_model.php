<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Report_laba_model extends CI_Model {

  // ------------------------------------------------------------------------

	public function __construct()
	{
		parent::__construct();
		
	}

  // ------------------------------------------------------------------------
	public function getAll()
	{
		$this->db->select("*");
		$this->db->from('tb_pengeluaran');
		$this->db->where("is_deleted",0);

		return $this->db->get()->result();
	}

	public function sumAll(){
		$this->db->select_sum('pengeluaran_amount','total');
		$this->db->where('is_deleted',0);

		return $this->db->get('tb_pengeluaran')->result()[0]->total;
	}

  // ------------------------------------------------------------------------

}

/* End of file Report_laba_model.php */
/* Location: ./application/models/Report_laba_model.php */