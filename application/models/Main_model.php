<?php
class Main_model extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}
    
    function query_uni($query_input){
        $hasil = $this->db->query("$query_input");
        return $hasil->result();
    }

    public function get_data($table,$condition){
        $hasil = $this->db->query("SELECT * FROM $table $condition");
        return $hasil->result();
    }

    public function get_data_row($table,$condition){
        $hasil = $this->db->query("SELECT * FROM $table $condition");
        return $hasil;
    }

    public function get_data_rows($table,$condition){
        $hasil = $this->db->query("SELECT * FROM $table $condition");
        return $hasil->num_rows();
    }

    public function get_customer_by_code($id){
        $hsl = $this->db->query("SELECT * FROM tb_customer WHERE customer_id = 'CUST-000002'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                 $hasil = array(
                    'customer_id' 	            => $data->customer_id,
                    'customer_name' 	        => $data->customer_name,
                    'customer_address' 	        => $data->customer_address,
                    'customer_type' 	        => $data->customer_type,
                    'customer_telephone'        => $data->customer_telephone,
                    'customer_saldo'            => $data->customer_saldo,
                    'customer_additional_info'  => $data->customer_additional_info,
                    'customer_insert_date'      => $data->customer_insert_date,
                    'customer_last_update_date' => $data->customer_last_update_date
                    );
            }
        }
        return $hasil;
    }
    
	function save_data($table,$values){
		$hasil=$this->db->query("INSERT INTO $table VALUES($values)");
		return $hasil;
	}
    
    function update_data($table,$data){
        $hasil=$this->db->query("UPDATE $table SET $data");
        return $hasil;
    }
    
    function delete_data($tabel,$condition){
        $hasil=$this->db->query("DELETE FROM $tabel $condition");
        return $hasil;
    }

    function pendapatan(){

        $this->db->where("trans_out_is_delete","0");
		$this->db->where("trans_out_complete","1");
		$this->db->select_sum('trans_out_paid_amount','terbayar');
		$this->db->select('YEAR(trans_out_insert_date) as tahun');
		$this->db->select('MONTH(trans_out_insert_date) as bulan');
		$this->db->group_by('bulan, tahun');
		$this->db->order_by('tahun, bulan', 'desc');
		$this->db->limit('12');

		return $this->db->get('tb_trans_out')->result();
    }

    function transTakTerkirim(){
        $this->db->from("tb_trans_out");
		$this->db->where("trans_out_is_delete","0");
        $this->db->where("trans_out_sent_status","0");
        $this->db->where("trans_out_delivery_type",'Delivery');

        return $this->db->count_all_results();
    }

    function totalTransIn(){
        $this->db->from("tb_trans_in");
		$this->db->where("trans_in_is_delete","0");
        $this->db->where("trans_in_complete","1");
        
		return $this->db->count_all_results();
    }

    function totalTransOut(){
        $this->db->from("tb_trans_out");
		$this->db->where("trans_out_is_delete","0");
        $this->db->where("trans_out_complete","1");
        
		return $this->db->count_all_results();
    }

    function transOutBulanIni(){

        $tahun = date('yy');
        $bulan = date('m');

        $this->db->where('trans_out_is_delete','0');
        $this->db->where('trans_out_complete','1');
        $this->db->where('YEAR(trans_out_insert_date)',$tahun);
        $this->db->where('MONTH(trans_out_insert_date)',$bulan);
        $this->db->select_sum('trans_out_paid_amount','keluar');

        return $this->db->get('tb_trans_out')->result();

    }

    function transInBulanIni(){

        $tahun = date('yy');
        $bulan = date('m');

        $this->db->where('trans_in_is_delete','0');
        $this->db->where('trans_in_complete','1');
        $this->db->where('YEAR(trans_in_insert_date)',$tahun);
        $this->db->where('MONTH(trans_in_insert_date)',$bulan);
        $this->db->select_sum('trans_in_paid_amount','masuk');

        return $this->db->get('tb_trans_in')->result();

    }
  
}