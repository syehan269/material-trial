<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export_model extends CI_Model {

  // ------------------------------------------------------------------------

  public function __construct()
  {
    parent::__construct();
  }

  // ------------------------------------------------------------------------


  // ------------------------------------------------------------------------
  public function getTransIn($date1, $date2)
  {

    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $this->db->select("*");
      $this->db->from('tb_trans_in');
      $this->db->where("trans_in_is_delete","0");
      $this->db->where("trans_in_insert_date Between '$date1' AND '$date2'");
      //$this->db->where("trans_in_complete","1");  
    } else {
      $this->db->select("*");
      $this->db->from('tb_trans_in');
      $this->db->where("trans_in_is_delete","0");
    }
    
    return $this->db->get();
  }

  public function countTransIn($date1, $date2){

    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $this->db->where("trans_in_is_delete","0");
      $this->db->where("trans_in_complete","1");
      $this->db->where("trans_in_insert_date Between '$date1' AND '$date2'");
      $this->db->select_sum('trans_in_payment_amount');      
    } else {
      $this->db->where("trans_in_is_delete","0");
      $this->db->where("trans_in_complete","1");
      $this->db->select_sum('trans_in_payment_amount');
    }

    return $this->db->get('tb_trans_in');
  }

  public function countTransInPaid($date1,$date2){

    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $this->db->where("trans_in_is_delete","0");
      $this->db->where("trans_in_complete","1");
      $this->db->where("trans_in_insert_date Between '$date1' AND '$date2'");
      $this->db->select_sum('trans_in_paid_amount','paid');
    } else {
      $this->db->where("trans_in_is_delete","0");
      $this->db->where("trans_in_complete","1");
      $this->db->select_sum('trans_in_paid_amount','paid');
    }
    
    return $this->db->get('tb_trans_in');
  }

  public function countTransInCash($date1,$date2){

    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $this->db->where("trans_in_is_delete","0");
      $this->db->where("trans_in_complete","1");
      $this->db->where("trans_in_payment_type","Cash");
      $this->db->where("trans_in_insert_date Between '$date1' AND '$date2'");
      $this->db->select_sum('trans_in_payment_amount','cash');
    } else {
      $this->db->where("trans_in_is_delete","0");
      $this->db->where("trans_in_complete","1");
      $this->db->where("trans_in_payment_type","Cash");
      $this->db->select_sum('trans_in_payment_amount','cash');
    }

    return $this->db->get('tb_trans_in');
  }

  public function countTransInCredit($date1,$date2){

    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $this->db->where("trans_in_is_delete","0");
      $this->db->where("trans_in_complete","1");
      $this->db->where("trans_in_payment_type","Credit");
      $this->db->where("trans_in_insert_date Between '$date1' AND '$date2'");
      $this->db->select_sum('trans_in_payment_amount','credit');
    } else {
      $this->db->where("trans_in_is_delete","0");
      $this->db->where("trans_in_complete","1");
      $this->db->where("trans_in_payment_type","Credit");
      $this->db->select_sum('trans_in_payment_amount','credit');
    }

    return $this->db->get('tb_trans_in');
  }

  //trans out
  public function getTransOut($date1,$date2)
  {

    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $this->db->select("*");
      $this->db->from('tb_trans_out');
      $this->db->where("trans_out_is_delete","0");
      $this->db->where("trans_out_insert_date Between '$date1' AND '$date2'");
      //$this->db->where("trans_in_complete","1");
    } else {
      $this->db->select("*");
      $this->db->from('tb_trans_out');
      $this->db->where("trans_out_is_delete","0");
      //$this->db->where("trans_in_complete","1");
    }
    
    return $this->db->get();
  }

  public function countTransOut($date1,$date2){

    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $this->db->where("trans_out_is_delete","0");
      $this->db->where("trans_out_complete","1");
      $this->db->where("trans_out_insert_date Between '$date1' AND '$date2'");
      $this->db->select_sum('trans_out_payment_amount');
    } else {
      $this->db->where("trans_out_is_delete","0");
      $this->db->where("trans_out_complete","1");
      $this->db->select_sum('trans_out_payment_amount');
    }
    
    return $this->db->get('tb_trans_out');
  }

  public function countTransOutPaid($date1,$date2){

    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $this->db->where("trans_out_is_delete","0");
      $this->db->where("trans_out_complete","1");
      $this->db->where("trans_out_insert_date Between '$date1' AND '$date2'");
      $this->db->select_sum('trans_out_paid_amount','paid');
    } else {
      $this->db->where("trans_out_is_delete","0");
      $this->db->where("trans_out_complete","1");
      $this->db->select_sum('trans_out_paid_amount','paid');
    }

    return $this->db->get('tb_trans_out');
  }

  public function countTransOutCash($date1,$date2){

    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $this->db->where("trans_out_is_delete","0");
      $this->db->where("trans_out_complete","1");
      $this->db->where("trans_out_payment_type","Cash");
      $this->db->where("trans_out_insert_date Between '$date1' AND '$date2'");
      $this->db->select_sum('trans_out_payment_amount','cash');
    } else {
      $this->db->where("trans_out_is_delete","0");
      $this->db->where("trans_out_complete","1");
      $this->db->where("trans_out_payment_type","Cash");
      $this->db->select_sum('trans_out_payment_amount','cash');
    }

    return $this->db->get('tb_trans_out');
  }

  public function countTransOutCredit($date1,$date2){

    if (strlen($date1) > 1 && strlen($date2) > 1 && $date1 != "undefined" && $date2 != "undefined") {
      $this->db->where("trans_out_is_delete","0");
      $this->db->where("trans_out_complete","1");
      $this->db->where("trans_out_payment_type","Credit");
      $this->db->where("trans_out_insert_date Between '$date1' AND '$date2'");
      $this->db->select_sum('trans_out_payment_amount','credit');
    } else {
      $this->db->where("trans_out_is_delete","0");
      $this->db->where("trans_out_complete","1");
      $this->db->where("trans_out_payment_type","Credit");
      $this->db->select_sum('trans_out_payment_amount','credit');
    }

    return $this->db->get('tb_trans_out');
  }

  //pengeluaran
  public function getPengeluaran()
  {
    $this->db->select("*");
    $this->db->from('tb_pengeluaran');
    $this->db->where("is_deleted","0");    
    return $this->db->get();
  }

  public function countPengeluaran(){
    $this->db->where("is_deleted","0");
		$this->db->select_sum('pengeluaran_amount');
    return $this->db->get('tb_pengeluaran');
  }

  // ------------------------------------------------------------------------

}

/* End of file Export_model_model.php */
/* Location: ./application/models/Export_model_model.php */