<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_In_model extends CI_Model {

	// ------------------------------------------------------------------------

	public function __construct()
	{
		parent::__construct();
	}

	// ------------------------------------------------------------------------


	// ------------------------------------------------------------------------
	public function getAll(){
		$this->db->select('*');
		$this->db->from('tb_trans_in');
		$this->db->where('trans_in_is_delete','0');
		$this->db->where('trans_in_complete','1');

		return $this->db->get()->result();
	}

	public function getBySupplier($supplier){
		$this->db->select('*');
		$this->db->from('tb_trans_in');
		$this->db->where('trans_in_is_delete','0');
		$this->db->where('trans_in_complete','1');
		$this->db->like('trans_in_supplier',$supplier);

		return $this->db->get()->result();
	}

	public function getByDateRange($awal, $akhir){
		$this->db->select('*');
		$this->db->from('tb_trans_in');
		$this->db->where('trans_in_is_delete','0');
		$this->db->where('trans_in_complete','1');
		$this->db->where("trans_in_insert_date Between '$awal' AND '$akhir'");

		return $this->db->get()->result();
	}

	public function getByAll($supplier, $awal, $akhir){
		$this->db->select('*');
		$this->db->from('tb_trans_in');
		$this->db->where('trans_in_is_delete','0');
		$this->db->where('trans_in_complete','1');
		$this->db->where('trans_in_supplier',$supplier);
		$this->db->where("trans_in_insert_date Between '$awal' AND '$akhir'");

		return $this->db->get()->result();
	}

	public function sumAll(){
		$this->db->select_sum('trans_in_payment_amount','total');
		$this->db->where('trans_in_is_delete','0');
		$this->db->where('trans_in_complete','1');

		return $this->db->get('tb_trans_in')->result()[0]->total;
	}

	public function sumBySupplier($supplier){
		$this->db->select_sum('trans_in_payment_amount','total');
		$this->db->where('trans_in_is_delete','0');
		$this->db->where('trans_in_complete','1');
		$this->db->like('trans_in_supplier',$supplier);

		return $this->db->get('tb_trans_in')->result()[0]->total;
	}

	public function sumByDateRange($awal, $akhir){
		$this->db->select_sum('trans_in_payment_amount','total');
		$this->db->where('trans_in_is_delete','0');
		$this->db->where('trans_in_complete','1');
		$this->db->where("trans_in_insert_date Between '$awal' AND '$akhir'");

		return $this->db->get('tb_trans_in')->result()[0]->total;
	}

	public function sumByAll($supplier, $awal, $akhir){
		$this->db->select_sum('trans_in_payment_amount','total');
		$this->db->where('trans_in_is_delete','0');
		$this->db->where('trans_in_complete','1');
		$this->db->where('trans_in_supplier',$supplier);
		$this->db->where("trans_in_insert_date Between '$awal' AND '$akhir'");

		return $this->db->get('tb_trans_in')->result()[0]->total;
	}

  	// ------------------------------------------------------------------------

}

/* End of file Report_model.php */
/* Location: ./application/models/Report_model.php */