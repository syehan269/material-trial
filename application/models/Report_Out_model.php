<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_Out_model extends CI_Model {

  // ------------------------------------------------------------------------

	public function __construct()
	{
		parent::__construct();
	}

	public function getAll(){
		$this->db->select('*');
		$this->db->from('tb_trans_out');
		$this->db->where('trans_out_is_delete','0');
		$this->db->where('trans_out_complete','1');

		return $this->db->get()->result();
	}

	public function getByCustomer($customer){
		$this->db->select('*');
		$this->db->from('tb_trans_out');
		$this->db->where('trans_out_is_delete','0');
		$this->db->where('trans_out_complete','1');
		$this->db->like('trans_out_customer_name',$customer);

		return $this->db->get()->result();
	}

	public function getByDateRange($awal, $akhir){
		$this->db->select('*');
		$this->db->from('tb_trans_out');
		$this->db->where('trans_out_is_delete','0');
		$this->db->where('trans_out_complete','1');
		$this->db->where("trans_out_insert_date Between '$awal' AND '$akhir'");

		return $this->db->get()->result();
	}

	public function getByAll($customer, $awal, $akhir){
		$this->db->select('*');
		$this->db->from('tb_trans_out');
		$this->db->where('trans_out_is_delete','0');
		$this->db->where('trans_out_complete','1');
		$this->db->where('trans_out_customer_name',$customer);
		$this->db->where("trans_out_insert_date Between '$awal' AND '$akhir'");

		return $this->db->get()->result();
	}

	public function sumAll(){
		$this->db->select_sum('trans_out_payment_amount','total');
		$this->db->where('trans_out_is_delete','0');
		$this->db->where('trans_out_complete','1');

		return $this->db->get('tb_trans_out')->result()[0]->total;
	}

	public function sumByCustomer($customer){
		$this->db->select_sum('trans_out_payment_amount','total');
		$this->db->where('trans_out_is_delete','0');
		$this->db->where('trans_out_complete','1');
		$this->db->like('trans_out_customer_name',$customer);

		return $this->db->get('tb_trans_out')->result()[0]->total;
	}

	public function sumByDateRange($awal, $akhir){
		$this->db->select_sum('trans_out_payment_amount','total');
		$this->db->where('trans_out_is_delete','0');
		$this->db->where('trans_out_complete','1');
		$this->db->where("trans_out_insert_date Between '$awal' AND '$akhir'");

		return $this->db->get('tb_trans_out')->result()[0]->total;
	}

	public function sumByAll($customer, $awal, $akhir){
		$this->db->select_sum('trans_out_payment_amount','total');
		$this->db->where('trans_out_is_delete','0');
		$this->db->where('trans_out_complete','1');
		$this->db->where('trans_out_customer_name',$customer);
		$this->db->where("trans_out_insert_date Between '$awal' AND '$akhir'");

		return $this->db->get('tb_trans_out')->result()[0]->total;
	}

}

/* End of file Report_Out_model.php */
/* Location: ./application/models/Report_Out_model.php */