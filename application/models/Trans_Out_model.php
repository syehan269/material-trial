<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trans_Out_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getItem($material)
	{
		$this->db->select('detail_in_trans_in_id');
		$this->db->from('tb_detail_trans_in');
		$this->db->like('detail_in_material_name',$material);
		$this->db->distinct('detail_in_trans_in_id');
		$trans_id = $this->db->get()->result();

		foreach ($trans_id as $item) {
			$trans[] = $item->detail_in_trans_in_id;
		}

		$this->db->select('*');
		$this->db->from('tb_trans_in');
		$this->db->where_in('trans_in_id',$trans);
		$data = $this->db->get()->result();

		return $data;
	}

}

/* End of file Trans_Out_model.php */
/* Location: ./application/models/Trans_Out_model.php */