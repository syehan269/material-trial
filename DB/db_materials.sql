/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1_3306
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : db_materials

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-08-11 01:10:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_credit
-- ----------------------------
DROP TABLE IF EXISTS `tb_credit`;
CREATE TABLE `tb_credit` (
  `credit_id` int(11) NOT NULL AUTO_INCREMENT,
  `credit_trans_out_id` varchar(20) DEFAULT NULL,
  `credit_amount` int(11) DEFAULT NULL,
  `credit_paid` enum('0','1') DEFAULT NULL,
  `credit_due_date` datetime DEFAULT NULL,
  `credit_additional_info` text,
  `credit_insert_date` datetime DEFAULT NULL,
  `credit_last_update_date` datetime DEFAULT NULL,
  `credit_is_delete` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`credit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_credit
-- ----------------------------
INSERT INTO `tb_credit` VALUES ('1', 'TRSOUT-000002', '10000', '0', '2020-07-16 00:00:00', 'Payment 1', '2020-06-22 12:08:11', '2020-06-22 12:08:11', '0');
INSERT INTO `tb_credit` VALUES ('2', 'TRSOUT-000006', '10000', '0', '2020-07-22 00:00:00', 'Payment 1', '2020-06-26 06:57:23', '2020-06-26 06:57:23', '0');
INSERT INTO `tb_credit` VALUES ('3', 'TRSOUT-000009', '10000', '0', '2020-07-27 00:00:00', 'Payment 1', '2020-06-26 06:59:50', '2020-06-26 06:59:50', '0');
INSERT INTO `tb_credit` VALUES ('4', 'TRSOUT-000011', '10000', '0', '2020-07-22 00:00:00', 'Payment 1', '2020-06-27 03:01:21', '2020-06-27 03:01:21', '0');
INSERT INTO `tb_credit` VALUES ('5', 'TRSOUT-000011', '10000', '0', '2020-07-29 00:00:00', 'Payment 2', '2020-06-27 03:01:21', '2020-06-27 03:01:21', '0');
INSERT INTO `tb_credit` VALUES ('6', 'TRSOUT-000015', '127400', '0', '2020-07-29 00:00:00', 'Payment 1', '2020-06-27 03:12:14', '2020-06-27 03:12:14', '0');
INSERT INTO `tb_credit` VALUES ('7', 'TRSOUT-000015', '127400', '0', '2020-07-30 00:00:00', 'Payment 2', '2020-06-27 03:12:14', '2020-06-27 03:12:14', '0');
INSERT INTO `tb_credit` VALUES ('8', 'TRSOUT-000015', '127400', '0', '2020-08-01 00:00:00', 'Payment 3', '2020-06-27 03:12:14', '2020-06-27 03:12:14', '0');
INSERT INTO `tb_credit` VALUES ('9', 'TRSOUT-000019', '227850', '0', '2020-07-30 00:00:00', 'Payment 1', '2020-06-27 03:33:03', '2020-06-27 03:33:03', '0');
INSERT INTO `tb_credit` VALUES ('10', 'TRSOUT-000019', '227850', '0', '2020-08-06 00:00:00', 'Payment 2', '2020-06-27 03:33:03', '2020-06-27 03:33:03', '0');
INSERT INTO `tb_credit` VALUES ('11', 'TRSOUT-000021', '176400', '0', '2020-08-06 00:00:00', 'Payment 1', '2020-06-27 06:01:55', '2020-06-27 06:01:55', '0');
INSERT INTO `tb_credit` VALUES ('12', 'TRSOUT-000021', '176400', '0', '2020-08-06 00:00:00', 'Payment 1', '2020-06-27 06:01:55', '2020-06-27 06:01:55', '0');
INSERT INTO `tb_credit` VALUES ('13', 'TRSOUT-000026', '504', '0', '2020-07-30 00:00:00', 'Payment 1', '2020-06-27 09:49:26', '2020-06-27 09:49:26', '0');
INSERT INTO `tb_credit` VALUES ('14', 'TRSOUT-000026', '504', '0', '2020-08-06 00:00:00', 'Payment 2', '2020-06-27 09:49:26', '2020-06-27 09:49:26', '0');
INSERT INTO `tb_credit` VALUES ('15', 'TRSOUT-000028', '68600', '0', '2020-07-29 00:00:00', 'Payment 1', '2020-06-29 08:02:00', '2020-06-29 08:02:00', '0');
INSERT INTO `tb_credit` VALUES ('16', 'TRSOUT-000028', '68600', '0', '2020-08-05 00:00:00', 'Payment 2', '2020-06-29 08:02:00', '2020-06-29 08:02:00', '0');

-- ----------------------------
-- Table structure for tb_customer
-- ----------------------------
DROP TABLE IF EXISTS `tb_customer`;
CREATE TABLE `tb_customer` (
  `customer_id` char(20) NOT NULL,
  `customer_name` varchar(50) DEFAULT NULL,
  `customer_address` text,
  `customer_type` enum('Retail','Special Retail','Distributor') DEFAULT NULL,
  `customer_telephone` varchar(20) DEFAULT NULL,
  `customer_saldo` int(11) DEFAULT NULL,
  `customer_additional_info` text,
  `customer_insert_date` datetime DEFAULT NULL,
  `customer_last_update_date` datetime DEFAULT NULL,
  `customer_is_delete` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_customer
-- ----------------------------
INSERT INTO `tb_customer` VALUES ('CUST-000001', 'dpqlpwldpqlw', 'lqowdkoqwkd', 'Retail', null, '0', 'plqplwpdlpqalwd', '2020-05-26 20:02:29', '2020-05-26 20:12:16', '1');
INSERT INTO `tb_customer` VALUES ('CUST-000002', 'Andi Surya', 'JL. Protokol No 1', 'Retail', null, '0', null, '2020-05-29 03:38:14', '2020-05-29 03:38:14', '0');
INSERT INTO `tb_customer` VALUES ('CUST-000003', 'Ardiansah', 'jl kemenyan 12', 'Distributor', '32125454565', '500000', '                    DA                       ', '2020-07-07 14:52:36', '2020-08-09 05:32:48', '0');
INSERT INTO `tb_customer` VALUES ('CUST-000004', 'Luhut', 'Kavling surga', 'Distributor', null, '1000000', 'null', '2020-06-27 17:05:10', '2020-06-27 17:22:42', '0');
INSERT INTO `tb_customer` VALUES ('CUST-000005', 'Dayyan Syehan Al Akbar', 'jl.surabaya', 'Distributor', '14045', '0', '                                      ', '2020-08-09 04:59:57', '2020-08-09 05:34:24', '0');
INSERT INTO `tb_customer` VALUES ('CUST-000006', 'Muna', 'jl.malang', 'Special Retail', '2319394301', '0', 'HAHAH', '2020-08-09 05:00:50', null, '0');

-- ----------------------------
-- Table structure for tb_detail_trans_in
-- ----------------------------
DROP TABLE IF EXISTS `tb_detail_trans_in`;
CREATE TABLE `tb_detail_trans_in` (
  `id_detail_trans_in` int(11) NOT NULL AUTO_INCREMENT,
  `detail_in_trans_in_id` varchar(20) NOT NULL,
  `detail_in_material_id` varchar(20) DEFAULT NULL,
  `detail_in_material_name` varchar(30) DEFAULT NULL,
  `detail_in_material_amount` int(11) DEFAULT NULL,
  `detail_in_material_price` int(11) DEFAULT NULL,
  `detail_in_merek_id` varchar(20) DEFAULT NULL,
  `detail_in_merek_name` varchar(30) DEFAULT NULL,
  `detail_in_supplier_id` varchar(20) DEFAULT NULL,
  `detail_in_supplier_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_detail_trans_in`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_detail_trans_in
-- ----------------------------
INSERT INTO `tb_detail_trans_in` VALUES ('1', 'TRSIN-000001', 'null', 'Cat Avitex', '100', '10000', 'null', 'null', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('2', 'TRSIN-000002', 'null', 'Cat Avitex', '1', '10000', 'null', 'null', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('3', 'TRSIN-000002', 'null', 'Semen Tiga Roda', '1', '10000', 'null', 'null', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('4', 'TRSIN-000003', 'null', 'Cat Avian', '1', '10000', 'null', 'null', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('5', 'TRSIN-000004', 'null', 'Cat Avian', '1', '10000', 'null', 'null', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('6', 'TRSIN-000005', 'null', '', '1', '10000', 'null', 'null', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('7', 'TRSIN-000005', 'null', '', null, '10000', 'null', 'null', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('8', 'TRSIN-000006', 'null', '', '1', '10000', 'null', 'Semen Tiga Roda', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('9', 'TRSIN-000007', 'null', '', '1', '10000', 'null', 'Cat Avitex', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('10', 'TRSIN-000007', 'null', '', '1', '10000', 'null', 'Semen Tiga Roda', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('11', 'TRSIN-000008', 'null', '', '1', '10000', 'null', 'Semen Tiga Roda', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('12', 'TRSIN-000009', 'null', '', '1', '10000', 'null', 'Cat Avitex', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('13', 'TRSIN-000009', 'null', '', '1', '10000', 'null', 'Cat Avitex', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('14', 'TRSIN-000010', 'null', '', '1', '10000', 'null', 'undefined', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('15', 'TRSIN-000010', 'null', '', '3', '10000', 'null', 'undefined', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('16', 'TRSIN-000010', 'null', '', '1', '10000', 'null', 'undefined', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('17', 'TRSIN-000011', 'null', '', '1', '10000', 'null', 'undefined', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('18', 'TRSIN-000011', 'null', '', '1', '10000', 'null', 'undefined', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('19', 'TRSIN-000012', 'null', '', '1', '10000', 'null', 'undefined', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('20', 'TRSIN-000012', 'null', '', '12', '10000', 'null', 'undefined', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('21', 'TRSIN-000013', 'null', '', '1', '10000', 'null', 'undefined', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('22', 'TRSIN-000015', 'null', 'Cat Avitex 500 ml', '1', '10000', 'null', '', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('23', 'TRSIN-000015', 'null', 'Semen Tiga Roda 25 kg', '1', '10000', 'null', '', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('24', 'TRSIN-000016', 'null', 'Cat Avitex 2 L', '1', '10000', 'null', 'undefined', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('25', 'TRSIN-000020', 'null', 'Cat Avitex 2 L', '1', '70000', 'null', 'undefined', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('26', 'TRSIN-000021', 'null', 'Semen Tiga Roda 10 kg', '3', '90000', 'null', 'undefined', 'null', 'null');
INSERT INTO `tb_detail_trans_in` VALUES ('27', 'TRSIN-000021', 'null', 'Semen Gresik 25kg 2B', '3', '250000', 'null', 'undefined', 'null', 'null');
INSERT INTO `tb_detail_trans_in` VALUES ('28', 'TRSIN-000022', 'null', 'Cat Avitex 500 ml', '2', '30000', 'null', 'undefined', 'null', 'null');
INSERT INTO `tb_detail_trans_in` VALUES ('29', 'TRSIN-000022', 'null', 'Semen Gresik 25kg 2B', '2', '250000', 'null', 'undefined', 'null', 'null');
INSERT INTO `tb_detail_trans_in` VALUES ('30', 'TRSIN-000023', 'null', 'Semen Tiga Roda 10 kg', '1', '90000', 'null', 'undefined', 'null', 'null');
INSERT INTO `tb_detail_trans_in` VALUES ('31', 'TRSIN-000023', 'null', 'Semen Tiga Roda 25 kg', '2', '140000', 'null', 'undefined', 'null', 'null');
INSERT INTO `tb_detail_trans_in` VALUES ('32', 'TRSIN-000024', 'null', 'Semen Gresik 20 kg', '1', '125000', 'null', 'undefined', 'null', 'null');
INSERT INTO `tb_detail_trans_in` VALUES ('33', 'TRSIN-000025', 'null', 'Cat Avian 1 L', '1', '55000', 'null', 'undefined', 'null', 'null');
INSERT INTO `tb_detail_trans_in` VALUES ('34', 'TRSIN-000026', 'null', 'Semen Tiga Roda 25 kg', '1', '140000', 'null', 'undefined', 'null', 'null');
INSERT INTO `tb_detail_trans_in` VALUES ('35', 'TRSIN-000026', 'null', 'Cat Avitex 500 ml', '1', '30000', 'null', 'undefined', 'null', 'null');
INSERT INTO `tb_detail_trans_in` VALUES ('36', 'TRSIN-000027', 'null', 'Semen Tiga Roda 10 kg', '1', '100000', 'null', 'undefined', 'null', 'null');
INSERT INTO `tb_detail_trans_in` VALUES ('37', 'TRSIN-000027', 'null', 'Semen Merah Putih 25 kg', '1', '135000', 'null', 'undefined', 'null', 'null');
INSERT INTO `tb_detail_trans_in` VALUES ('38', 'TRSIN-000028', 'null', 'Cat Avian 1 L', '50', '55000', 'null', '', 'null', 'null');
INSERT INTO `tb_detail_trans_in` VALUES ('39', 'TRSIN-000029', 'null', 'Cat Avitex 2 L', '10', '70000', 'null', '', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('40', 'TRSIN-000030', 'null', 'Cat Avitex 500 ml', '1', '30000', 'null', '', 'SUPLL-000004', 'PT BETON KUAT');
INSERT INTO `tb_detail_trans_in` VALUES ('41', 'TRSIN-000030', 'null', 'Cat Avitex 500 ml', '2', '30000', 'null', '', 'SUPLL-000004', 'PT BETON KUAT');

-- ----------------------------
-- Table structure for tb_detail_trans_out
-- ----------------------------
DROP TABLE IF EXISTS `tb_detail_trans_out`;
CREATE TABLE `tb_detail_trans_out` (
  `id_detail_trans_out` int(11) NOT NULL AUTO_INCREMENT,
  `detail_out_trans_out_id` varchar(20) NOT NULL,
  `detail_out_material_id` varchar(20) DEFAULT NULL,
  `detail_out_material_name` varchar(30) DEFAULT NULL,
  `detail_out_material_amount` int(11) DEFAULT NULL,
  `detail_out_material_price` int(11) DEFAULT NULL,
  `detail_out_merek_id` varchar(20) DEFAULT NULL,
  `detail_out_merek_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_detail_trans_out`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_detail_trans_out
-- ----------------------------
INSERT INTO `tb_detail_trans_out` VALUES ('1', 'TRSOUT-000001', null, 'Gips', '12', '210000', null, 'Cahya Asia');
INSERT INTO `tb_detail_trans_out` VALUES ('2', 'TRSOUT-000002', null, 'Tembaga', '2', '16000', null, 'TembagaPura');
INSERT INTO `tb_detail_trans_out` VALUES ('3', 'TRSOUT-000003', null, 'Paku', '20', '1300', null, 'Other');
INSERT INTO `tb_detail_trans_out` VALUES ('7', 'TRSOUT-000005', 'null', 'null', '1', '10000', 'null', 'Cat Avitex 2 L');
INSERT INTO `tb_detail_trans_out` VALUES ('8', 'TRSOUT-000005', 'null', 'null', '1', '10000', 'null', 'Semen Tiga Roda 25 kg');
INSERT INTO `tb_detail_trans_out` VALUES ('9', 'TRSOUT-000005', 'null', 'null', '1', '10000', 'null', 'Semen Tiga Roda 10 kg');
INSERT INTO `tb_detail_trans_out` VALUES ('10', 'TRSOUT-000006', 'null', 'null', '1', '10000', 'null', 'Cat Avitex 500 ml');
INSERT INTO `tb_detail_trans_out` VALUES ('11', 'TRSOUT-000006', 'null', 'null', '1', '10000', 'null', 'Semen Tiga Roda 10 kg');
INSERT INTO `tb_detail_trans_out` VALUES ('12', 'TRSOUT-000007', 'null', 'null', '2', '10000', 'null', 'Cat Avian 1 L');
INSERT INTO `tb_detail_trans_out` VALUES ('13', 'TRSOUT-000007', 'null', 'null', '1', '10000', 'null', 'Semen Merah Putih 25 kg');
INSERT INTO `tb_detail_trans_out` VALUES ('14', 'TRSOUT-000008', 'null', 'null', '2', '10000', 'null', 'Cat Avian 1 L');
INSERT INTO `tb_detail_trans_out` VALUES ('15', 'TRSOUT-000008', 'null', 'null', '1', '10000', 'null', 'Semen Merah Putih 25 kg');
INSERT INTO `tb_detail_trans_out` VALUES ('16', 'TRSOUT-000009', 'null', 'null', '1', '10000', 'null', 'Cat Avitex 500 ml');
INSERT INTO `tb_detail_trans_out` VALUES ('17', 'TRSOUT-000010', 'null', 'null', '1', '10000', 'null', 'Semen Tiga Roda 10 kg');
INSERT INTO `tb_detail_trans_out` VALUES ('18', 'TRSOUT-000010', 'null', 'null', '1', '10000', 'null', 'Semen Tiga Roda 25 kg');
INSERT INTO `tb_detail_trans_out` VALUES ('19', 'TRSOUT-000011', 'null', 'null', '2', '10000', 'null', 'Cat Avian 1 L');
INSERT INTO `tb_detail_trans_out` VALUES ('20', 'TRSOUT-000011', 'null', 'null', '1', '10000', 'null', 'Semen Tiga Roda 25 kg');
INSERT INTO `tb_detail_trans_out` VALUES ('21', 'TRSOUT-000012', 'null', 'null', '1', '10000', 'null', 'Cat Avitex 500 ml');
INSERT INTO `tb_detail_trans_out` VALUES ('22', 'TRSOUT-000012', 'null', 'null', '1', '10000', 'null', 'Semen Gresik 25kg 2B');
INSERT INTO `tb_detail_trans_out` VALUES ('23', 'TRSOUT-000013', 'null', 'null', '1', '10000', 'null', 'Cat Avitex 500 ml');
INSERT INTO `tb_detail_trans_out` VALUES ('24', 'TRSOUT-000013', 'null', 'null', '1', '10000', 'null', 'Semen Gresik 25kg 2B');
INSERT INTO `tb_detail_trans_out` VALUES ('25', 'TRSOUT-000014', 'null', 'null', '1', '10000', 'null', 'Semen Tiga Roda 10 kg');
INSERT INTO `tb_detail_trans_out` VALUES ('26', 'TRSOUT-000014', 'null', 'null', '1', '10000', 'null', 'Semen Merah Putih 25 kg');
INSERT INTO `tb_detail_trans_out` VALUES ('27', 'TRSOUT-000015', 'null', 'null', '1', '10000', 'null', 'Semen Tiga Roda 25 kg');
INSERT INTO `tb_detail_trans_out` VALUES ('28', 'TRSOUT-000015', 'null', 'null', '1', '10000', 'null', 'Semen Gresik 25kg 2B');
INSERT INTO `tb_detail_trans_out` VALUES ('31', 'TRSOUT-000017', 'null', 'Semen Tiga Roda 25 kg', '1', '10000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('32', 'TRSOUT-000017', 'null', 'Semen Gresik 25kg 2B', '1', '10000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('33', 'TRSOUT-000018', 'null', 'Semen Tiga Roda 10 kg', '1', '10000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('34', 'TRSOUT-000018', 'null', 'Semen Gresik 25kg 2B', '1', '10000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('35', 'TRSOUT-000019', 'null', 'Semen Tiga Roda 10 kg', '1', '10000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('36', 'TRSOUT-000019', 'null', 'Semen Gresik 25kg 2B', '1', '10000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('37', 'TRSOUT-000019', 'null', 'Semen Gresik 20 kg', '1', '10000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('38', 'TRSOUT-000020', 'null', 'Cat Avitex 500 ml', '1', '30000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('39', 'TRSOUT-000021', 'null', 'Semen Gresik 20 kg', '1', '125000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('40', 'TRSOUT-000021', 'null', 'Cat Avian 1 L', '1', '55000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('41', 'TRSOUT-000022', 'null', 'Semen Tiga Roda 10 kg', '1', '90000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('42', 'TRSOUT-000023', 'null', 'Cat Avitex 2 L', '1', '70000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('43', 'TRSOUT-000024', 'null', 'Semen Gresik 25kg 2B', '1', '250000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('44', 'TRSOUT-000024', 'null', 'Semen Tiga Roda 10 kg', '1', '90000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('45', 'TRSOUT-000025', 'null', 'Semen Tiga Roda 25 kg', '4', '140000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('46', 'TRSOUT-000026', 'null', 'Cat Avitex 500 ml', '1', '30000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('47', 'TRSOUT-000026', 'null', 'Semen Gresik 25kg 2B', '4', '250000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('48', 'TRSOUT-000026', 'null', 'Cat Avitex 500 ml', '2', '30000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('49', 'TRSOUT-000026', 'null', 'Cat Avitex 500 ml', '1', '30000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('50', 'TRSOUT-000027', 'null', 'Semen Tiga Roda 10 kg', '1', '90000', 'null', 'undefined');
INSERT INTO `tb_detail_trans_out` VALUES ('51', 'TRSOUT-000028', 'null', 'Cat Avitex 2 L', '2', '70000', 'null', '');

-- ----------------------------
-- Table structure for tb_discount
-- ----------------------------
DROP TABLE IF EXISTS `tb_discount`;
CREATE TABLE `tb_discount` (
  `discount_id` varchar(20) NOT NULL,
  `discount_percentage` int(5) DEFAULT NULL,
  `discount_insert_date` datetime DEFAULT NULL,
  `discount_last_update_date` datetime DEFAULT NULL,
  `discount_is_delete` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`discount_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_discount
-- ----------------------------
INSERT INTO `tb_discount` VALUES ('DSC-000001', '0', '2020-05-26 13:26:00', '2020-05-27 17:41:40', '1');
INSERT INTO `tb_discount` VALUES ('DSC-000002', '10', '2020-05-26 13:26:05', '2020-05-26 13:55:23', '0');
INSERT INTO `tb_discount` VALUES ('DSC-000003', '2', '2020-05-26 13:26:09', '2020-05-26 13:26:09', '0');
INSERT INTO `tb_discount` VALUES ('DSC-000004', '35', '2020-05-26 13:29:51', '2020-05-26 13:29:51', '0');
INSERT INTO `tb_discount` VALUES ('DSC-000005', '0', '2020-05-27 17:40:49', '2020-05-27 17:40:49', '0');
INSERT INTO `tb_discount` VALUES ('DSC-000006', '10', '2020-05-29 03:15:51', '2020-05-29 03:15:51', '0');
INSERT INTO `tb_discount` VALUES ('DSC-000007', '2', '2020-06-06 01:51:54', '2020-06-06 01:51:54', '0');
INSERT INTO `tb_discount` VALUES ('DSC-000008', '2', '2020-06-06 01:52:23', '2020-06-06 01:52:23', '0');

-- ----------------------------
-- Table structure for tb_material
-- ----------------------------
DROP TABLE IF EXISTS `tb_material`;
CREATE TABLE `tb_material` (
  `material_id` varchar(20) NOT NULL,
  `material_name` varchar(50) DEFAULT NULL,
  `material_harga_beli` int(11) DEFAULT NULL,
  `material_merek_id` varchar(20) DEFAULT NULL,
  `material_merek_name` varchar(50) DEFAULT NULL,
  `material_satuan` varchar(10) DEFAULT NULL,
  `material_satuan_amount` int(10) DEFAULT NULL,
  `material_insert_date` datetime DEFAULT NULL,
  `material_last_update_date` datetime DEFAULT NULL,
  `material_is_delete` enum('0','1') DEFAULT NULL,
  `material_harga_eceran` int(11) DEFAULT NULL,
  `material_harga_reguler` int(11) DEFAULT NULL,
  `material_harga_distributor` int(11) DEFAULT NULL,
  PRIMARY KEY (`material_id`),
  KEY `material_merek_id` (`material_merek_id`),
  CONSTRAINT `tb_material_ibfk_1` FOREIGN KEY (`material_merek_id`) REFERENCES `tb_merek` (`merek_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_material
-- ----------------------------
INSERT INTO `tb_material` VALUES ('MTR-000001', 'Cat Avitex 2 L', '70000', 'MRK-000004', 'Cat Avitex', 'L - ML', '1000', '2020-05-26 16:55:54', '2020-05-26 17:25:56', '1', '76000', '74000', '70000');
INSERT INTO `tb_material` VALUES ('MTR-000002', 'Cat Avitex 500 ml', '30000', 'MRK-000004', 'Cat Avitex', 'L - ML', '1000', '2020-05-26 16:58:43', '2020-05-26 16:58:43', '0', '20300', '40000', '35000');
INSERT INTO `tb_material` VALUES ('MTR-000003', 'Semen Tiga Roda 10 kg', '100000', 'MRK-000002', 'Semen Tiga Roda', 'KG - Gram', '1000', '2020-05-26 16:59:01', '2020-06-27 16:41:04', '0', '115000', '110000', '105000');
INSERT INTO `tb_material` VALUES ('MTR-000004', 'Semen Tiga Roda 40 kg', '140000', 'MRK-000002', 'Semen Tiga Roda', 'KG - Gram', '1000', '2020-05-26 17:07:52', '2020-08-09 07:25:54', '0', '149000', '145000', '142000');
INSERT INTO `tb_material` VALUES ('MTR-000005', 'Semen Merah Putih 25 kg', '135000', 'MRK-000005', 'Semen Merah Putih', 'KG - Gram', '1000', '2020-05-26 17:18:03', '2020-06-27 16:40:09', '0', '142000', '140000', '139000');
INSERT INTO `tb_material` VALUES ('MTR-000006', 'Cat Avian 1 L', '55000', 'MRK-000001', 'Cat Avian', 'L - ML', '1000', '2020-05-29 03:28:10', '2020-05-29 03:28:10', '0', '61000', '60000', '57000');
INSERT INTO `tb_material` VALUES ('MTR-000007', 'Semen Gresik 20 kg', '125000', 'MRK-000003', 'Semen Gresik', 'KG - Gram', '1000', '2020-06-11 15:32:23', '2020-06-11 15:32:23', '0', '131000', '130000', '123000');
INSERT INTO `tb_material` VALUES ('MTR-000008', 'Semen Gresik 25kg 2B', '250000', 'MRK-000003', 'Semen Gresik', 'KG - Gram', '1000', '2020-06-26 05:00:29', '2020-06-26 05:00:29', '0', '320000', '300000', '280000');
INSERT INTO `tb_material` VALUES ('MTR-000009', 'lem rajawali kecil', '20000', null, null, 'L - ML', '1000', '2020-08-09 06:50:58', null, '0', '22000', '21000', '23000');
INSERT INTO `tb_material` VALUES ('MTR-000010', 'lem rajawali kecil', '20000', null, null, 'L - ML', '1000', '2020-08-09 06:51:28', null, '0', '23500', '23000', '22000');
INSERT INTO `tb_material` VALUES ('MTR-000011', 'lem rajawali besar', '30000', null, null, 'L - ML', '1000', '2020-08-09 06:54:12', null, '0', '35000', '34000', '32000');
INSERT INTO `tb_material` VALUES ('MTR-000012', 'Kabel', '5000', null, null, 'M - CM', '100', null, '2020-08-10 09:08:29', null, '7500', '7000', '6500');
INSERT INTO `tb_material` VALUES ('MTR-000013', 'Cat NoDrop', '100000', null, null, 'M - CM', '100', null, '2020-08-10 09:25:03', null, '140000', '130000', '125000');

-- ----------------------------
-- Table structure for tb_membership
-- ----------------------------
DROP TABLE IF EXISTS `tb_membership`;
CREATE TABLE `tb_membership` (
  `member_id` varchar(10) NOT NULL,
  `member_name` varchar(50) DEFAULT NULL,
  `member_poin` int(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `is_delete` int(1) DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_membership
-- ----------------------------

-- ----------------------------
-- Table structure for tb_merek
-- ----------------------------
DROP TABLE IF EXISTS `tb_merek`;
CREATE TABLE `tb_merek` (
  `merek_id` varchar(20) NOT NULL,
  `merek_name` varchar(50) DEFAULT NULL,
  `merek_insert_date` datetime DEFAULT NULL,
  `merek_last_update_date` datetime DEFAULT NULL,
  `merek_is_delete` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`merek_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_merek
-- ----------------------------
INSERT INTO `tb_merek` VALUES ('MRK-000001', 'Cat Avian 1', '2020-05-26 14:16:14', '2020-06-27 16:21:08', '0');
INSERT INTO `tb_merek` VALUES ('MRK-000002', 'Semen Tiga Roda', '2020-05-26 14:22:35', '2020-05-29 03:26:42', '1');
INSERT INTO `tb_merek` VALUES ('MRK-000003', 'Semen Gresik', '2020-05-26 14:23:22', '2020-05-26 14:23:22', '0');
INSERT INTO `tb_merek` VALUES ('MRK-000004', 'Cat Avitex', '2020-05-26 14:56:18', '2020-05-26 14:56:18', '1');
INSERT INTO `tb_merek` VALUES ('MRK-000005', 'Semen Merah Putih', '2020-05-27 17:46:07', '2020-05-27 17:46:07', '0');

-- ----------------------------
-- Table structure for tb_satuan
-- ----------------------------
DROP TABLE IF EXISTS `tb_satuan`;
CREATE TABLE `tb_satuan` (
  `satuan_id` varchar(10) NOT NULL,
  `satuan_atas` varchar(20) DEFAULT NULL,
  `satuan_bawah` varchar(20) DEFAULT NULL,
  `ratio_satuan` int(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `satuan_is_delete` int(1) DEFAULT NULL,
  PRIMARY KEY (`satuan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_satuan
-- ----------------------------
INSERT INTO `tb_satuan` VALUES ('STN-000001', 'M', 'CM', '100', '2020-08-10 00:52:24', '0');
INSERT INTO `tb_satuan` VALUES ('STN-000002', 'M^2', 'CM^2', '10000', '2020-08-10 08:48:32', '0');
INSERT INTO `tb_satuan` VALUES ('STN-000003', 'KG', 'Gram', '1000', '2020-08-10 08:45:44', '0');
INSERT INTO `tb_satuan` VALUES ('STN-000004', 'L', 'ML', '1000', '2020-08-10 09:31:13', '0');

-- ----------------------------
-- Table structure for tb_supplier
-- ----------------------------
DROP TABLE IF EXISTS `tb_supplier`;
CREATE TABLE `tb_supplier` (
  `supplier_id` varchar(20) NOT NULL,
  `supplier_name` varchar(50) DEFAULT NULL,
  `supplier_address` text,
  `supplier_telphone` char(20) DEFAULT NULL,
  `supplier_fax` varchar(20) DEFAULT NULL,
  `supplier_insert_date` datetime DEFAULT NULL,
  `supplier_last_update_date` datetime DEFAULT NULL,
  `supplier_is_delete` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_supplier
-- ----------------------------
INSERT INTO `tb_supplier` VALUES ('SUPLL-000002', 'kadMON', 'END', '029414910414', '0124001-94140', null, null, '1');
INSERT INTO `tb_supplier` VALUES ('SUPLL-000004', 'PT BETON Lemah', '   JL PROKLAMASI NO 123 ', '  085785456342', '082418210', '2020-08-09 04:03:15', '2020-06-03 07:57:08', '0');
INSERT INTO `tb_supplier` VALUES ('SUPLL-000005', 'PT JAYA BETO', ' JL PURI CEMPAKA PUTIH NO 1 ', '02841824', '082418210', '2020-05-26 13:16:31', '2020-06-27 15:34:29', '0');
INSERT INTO `tb_supplier` VALUES ('SUPLL-000010', 'CV Bata Gor', 'Jl MUABA ABIBA', ' 081532647588', '12471-7129-1248', '2020-05-30 09:45:33', '2020-06-03 22:28:49', '0');
INSERT INTO `tb_supplier` VALUES ('SUPLL-000011', 'PT Empat Roda', 'JL PONCOKUSUMO', '081532647588', '12471-7129-1248', '2020-06-03 07:58:07', '2020-06-03 07:58:07', '0');
INSERT INTO `tb_supplier` VALUES ('SUPLL-000013', 'Dayyan Syehan Al Akbar', 'FAG', ' 081532647588', '12313-43459', '2020-06-05 09:50:04', '2020-06-05 09:50:04', '0');
INSERT INTO `tb_supplier` VALUES ('SUPLL-000014', 'Kadmon', '2319394301', null, null, '2020-08-09 03:09:56', null, '1');
INSERT INTO `tb_supplier` VALUES ('SUPLL-000015', 'Jalda', 'EDEN', '02392103032', null, '2020-08-09 03:11:15', null, '0');

-- ----------------------------
-- Table structure for tb_trans_in
-- ----------------------------
DROP TABLE IF EXISTS `tb_trans_in`;
CREATE TABLE `tb_trans_in` (
  `trans_in_id` varchar(20) NOT NULL,
  `trans_in_payment_amount` int(11) DEFAULT NULL,
  `trans_in_payment_type` enum('Cash','Credit') DEFAULT NULL,
  `trans_in_due_date` datetime DEFAULT NULL,
  `trans_in_additional_info` text,
  `trans_in_insert_date` datetime DEFAULT NULL,
  `trans_in_last_update_date` datetime DEFAULT NULL,
  `trans_in_is_delete` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`trans_in_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_trans_in
-- ----------------------------
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000001', '1000000', 'Credit', '2020-05-05 20:20:20', 'kOk', '2020-06-19 11:02:13', '2020-06-27 08:08:36', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000002', '20000', 'Cash', '2020-05-05 20:20:20', 'test', '2020-06-19 11:09:59', '2020-06-28 09:00:05', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000003', '10000', 'Cash', '2020-05-05 20:20:20', 'kok', '2020-06-19 11:10:49', '2020-06-27 08:08:53', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000004', '10000', 'Credit', '2020-05-05 20:20:20', 'fagg', '2020-06-19 11:26:15', '2020-06-27 08:09:02', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000005', '110000', 'Credit', '2020-05-05 20:20:20', 'test1', '2020-06-19 13:46:28', '2020-07-03 10:33:54', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000006', '10000', 'Credit', '2020-05-05 20:20:20', 'fagg1', '2020-06-19 13:59:18', '2020-06-27 08:09:53', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000007', '20000', 'Cash', '2020-07-28 01:04:24', '', '2020-06-20 09:06:45', '2020-06-27 18:04:24', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000008', '10000', 'Cash', '2020-07-28 04:07:11', 'dd', '2020-06-20 13:29:50', '2020-06-28 09:07:12', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000009', '20000', 'Cash', '2020-05-05 20:20:20', 'ttt', '2020-06-20 13:33:27', '2020-06-20 13:33:27', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000010', '50000', 'Cash', '2020-05-05 20:20:20', 'fagg', '2020-06-26 06:23:18', '2020-06-27 08:10:18', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000011', '20000', 'Cash', '2020-05-05 20:20:20', '', '2020-06-26 15:27:20', '2020-06-27 08:10:34', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000012', '130000', 'Cash', '2020-05-05 20:20:20', '', '2020-06-26 16:27:20', '2020-06-28 09:33:14', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000013', '10000', 'Cash', '2020-05-05 20:20:20', '', '2020-06-27 06:39:17', '2020-06-28 09:33:01', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000014', '120000', 'Cash', '2020-05-05 20:20:20', '', '2020-06-27 06:45:51', '2020-06-27 09:58:46', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000015', '20000', 'Credit', '2020-05-05 20:20:20', '', '2020-06-27 06:48:53', '2020-06-28 09:20:18', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000016', '70000', 'Credit', '2020-05-05 20:20:20', 'null', '2020-06-27 06:49:40', '2020-06-27 06:49:40', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000017', '90000', 'Cash', '2020-05-05 20:20:20', 'null', '2020-06-27 06:53:15', '2020-06-27 06:53:15', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000018', '90000', 'Cash', '2020-05-05 20:20:20', 'null', '2020-06-27 06:53:26', '2020-06-27 06:53:26', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000019', '30000', 'Cash', '2020-05-05 20:20:20', 'null', '2020-06-27 06:53:55', '2020-06-27 06:53:55', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000020', '70000', 'Cash', '2020-05-05 20:20:20', 'null', '2020-06-27 06:54:28', '2020-06-27 06:54:28', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000021', '1020000', 'Cash', '2020-05-05 20:20:20', '', '2020-06-27 07:50:11', '2020-06-27 08:43:44', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000022', '560000', 'Cash', '2020-05-05 20:20:20', 'null', '2020-06-27 08:12:48', '2020-06-27 08:12:48', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000023', '370000', 'Cash', '2020-05-05 20:20:20', '', '2020-06-27 08:41:29', '2020-06-27 08:43:35', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000024', '125000', 'Credit', '2020-07-28 04:24:13', 'take a beer', '2020-06-28 09:24:14', '2020-06-28 09:32:48', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000025', '55000', 'Cash', '2020-07-28 04:28:06', 'null', '2020-06-28 09:28:07', '2020-06-28 09:28:07', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000026', '170000', 'Cash', '2020-07-28 05:48:08', 'null', '2020-06-28 10:48:09', '2020-06-28 10:48:09', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000027', '235000', 'Cash', '2020-07-28 05:49:44', 'null', '2020-06-28 10:49:45', '2020-06-28 10:49:45', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000028', '2', 'Cash', '2020-07-29 12:19:41', 'null', '2020-06-29 05:19:44', '2020-06-29 05:19:44', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000029', '700000', 'Cash', '2020-07-29 03:00:00', 'null', '2020-06-29 08:00:01', '2020-06-29 08:00:01', '0');
INSERT INTO `tb_trans_in` VALUES ('TRSIN-000030', '90000', 'Credit', '2020-08-02 04:19:01', 'null', '2020-07-02 09:19:01', '2020-07-02 09:19:01', '0');

-- ----------------------------
-- Table structure for tb_trans_out
-- ----------------------------
DROP TABLE IF EXISTS `tb_trans_out`;
CREATE TABLE `tb_trans_out` (
  `trans_out_id` varchar(20) NOT NULL,
  `trans_out_customer_id` varchar(20) DEFAULT NULL,
  `trans_out_customer_name` varchar(20) DEFAULT NULL,
  `trans_out_payment_amount` int(11) DEFAULT NULL,
  `trans_out_payment_type` enum('Cash','Credit') DEFAULT NULL,
  `trans_out_delivery_type` enum('Delivery','Takeaway') DEFAULT NULL,
  `trans_out_delivery_address` text,
  `trans_out_due_date` datetime DEFAULT NULL,
  `trans_out_additional_info` text,
  `trans_out_insert_date` datetime DEFAULT NULL,
  `trans_out_last_update_date` datetime DEFAULT NULL,
  `trans_out_is_delete` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`trans_out_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_trans_out
-- ----------------------------
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000001', 'null', 'Sap', '20000', 'Credit', 'Takeaway', '-', '2020-12-20 01:01:01', 'info', '2020-06-22 12:07:11', '2020-06-22 12:07:11', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000002', 'null', 'Sap', '20000', 'Credit', 'Takeaway', '-', '2020-12-20 01:01:01', 'info', '2020-06-22 12:08:11', '2020-06-22 12:08:11', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000003', 'null', '123456780', '49000', 'Cash', 'Takeaway', '-', '2020-12-11 00:00:00', 'sfdfdfd', '2020-06-22 12:25:09', '2020-06-22 12:25:09', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000004', 'null', '123456780', '294000', 'Cash', 'Takeaway', '-', '2020-12-11 00:00:00', 'retarf', '2020-06-26 06:49:39', '2020-06-26 06:49:39', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000005', 'null', '123456780', '294000', 'Cash', 'Takeaway', '-', '2020-12-11 00:00:00', 'retarf', '2020-06-26 06:52:53', '2020-06-26 06:52:53', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000006', 'null', '135133212', '117600', 'Credit', 'Takeaway', '-', '2020-12-11 00:00:00', 'tod', '2020-06-26 06:57:23', '2020-06-26 06:57:23', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000007', 'null', '135133212', '186200', 'Credit', 'Takeaway', '-', '2020-12-11 00:00:00', 'ddas', '2020-06-26 06:58:40', '2020-06-26 06:58:40', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000008', 'null', '135133212', '186200', 'Credit', 'Takeaway', '-', '2020-12-11 00:00:00', 'ddas', '2020-06-26 06:59:08', '2020-06-26 06:59:08', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000009', 'null', '135133212', '29400', 'Credit', 'Takeaway', '-', '2020-12-11 00:00:00', 'edd', '2020-06-26 06:59:50', '2020-06-26 06:59:50', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000010', 'null', 'Andi Surya', '225400', 'Cash', 'Takeaway', '-', '2020-12-11 00:00:00', 'fagg', '2020-06-26 07:03:13', '2020-06-26 07:03:13', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000011', 'null', 'Ardiansah', '245000', 'Credit', 'Takeaway', '-', '2020-12-11 00:00:00', 'top', '2020-06-27 03:01:21', '2020-06-27 03:01:21', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000012', 'null', 'Andi Surya', '274400', 'Credit', 'Takeaway', '-', '2020-12-11 00:00:00', 'test', '2020-06-27 03:07:51', '2020-06-27 03:07:51', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000013', 'null', 'Andi Surya', '274400', 'Credit', 'Takeaway', '-', '2020-12-11 00:00:00', 'test', '2020-06-27 03:08:03', '2020-06-27 03:08:03', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000014', 'null', 'Andi Surya', '220500', 'Credit', 'Takeaway', '-', '2020-12-11 00:00:00', 'top', '2020-06-27 03:10:43', '2020-06-27 03:10:43', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000015', 'null', 'Andi Surya', '382200', 'Credit', 'Takeaway', '-', '2020-12-11 00:00:00', 'tes', '2020-06-27 03:12:14', '2020-06-27 03:12:14', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000016', 'null', 'Andi Surya', '382200', 'Cash', 'Takeaway', '-', '2020-12-11 00:00:00', 'yes', '2020-06-27 03:24:34', '2020-06-27 03:24:34', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000017', 'null', 'Andi Surya', '382200', 'Cash', 'Takeaway', '-', '2020-12-11 00:00:00', 'yes', '2020-06-27 03:25:58', '2020-06-27 03:25:58', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000018', 'null', 'Andi Surya', '333200', 'Cash', 'Takeaway', '-', '2020-12-11 00:00:00', 'ss', '2020-06-27 03:29:38', '2020-06-27 03:29:38', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000019', 'null', 'Andi Surya', '455700', 'Credit', 'Takeaway', '-', '2020-12-11 00:00:00', 'pante', '2020-06-27 03:33:03', '2020-06-27 03:33:03', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000020', 'null', 'Andi Surya', '29400', 'Cash', 'Takeaway', '-', '2020-12-11 00:00:00', 'das', '2020-06-27 05:58:58', '2020-06-27 05:58:58', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000021', 'null', 'Andi Surya', '176400', 'Credit', 'Takeaway', '-', '2020-12-11 00:00:00', 'mantap', '2020-06-27 06:01:55', '2020-06-27 06:01:55', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000022', 'null', 'Andi Surya', '88200', 'Cash', 'Takeaway', '-', '2020-12-11 00:00:00', 'COK', '2020-06-27 06:31:38', '2020-06-27 06:31:38', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000023', 'null', 'Andi Surya', '68600', 'Cash', 'Takeaway', '-', '2020-12-11 00:00:00', 'null', '2020-06-27 09:15:30', '2020-06-27 09:15:30', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000024', 'null', 'Andi Surya', '343200', 'Cash', 'Delivery', 'JL. Protokol No 1', '2020-12-11 00:00:00', 'null', '2020-06-27 09:21:53', '2020-06-27 09:21:53', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000025', 'null', 'Andi Surya', '548800', 'Cash', 'Takeaway', '-', '2020-12-11 00:00:00', 'null', '2020-06-27 09:26:18', '2020-06-27 09:26:18', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000026', 'null', 'Ardiansah', '1008000', 'Credit', 'Takeaway', '-', '2020-12-11 00:00:00', 'null', '2020-06-27 09:49:26', '2020-06-27 09:49:26', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000027', 'null', 'Andi Surya', '89200', 'Cash', 'Delivery', 'JL. Protokol No 1', '2020-12-11 00:00:00', 'null', '2020-06-27 09:57:27', '2020-06-27 09:57:27', '0');
INSERT INTO `tb_trans_out` VALUES ('TRSOUT-000028', 'null', 'Andi Surya', '137200', 'Credit', 'Takeaway', '-', '2020-07-29 03:02:00', 'null', '2020-06-29 08:02:00', '2020-06-29 08:02:00', '0');

-- ----------------------------
-- Table structure for tb_utang
-- ----------------------------
DROP TABLE IF EXISTS `tb_utang`;
CREATE TABLE `tb_utang` (
  `utang_id` varchar(10) NOT NULL,
  `utang_trans_in_id` varchar(10) DEFAULT NULL,
  `utang_amount` int(15) DEFAULT NULL,
  `utang_paid` int(1) DEFAULT NULL,
  `utang_due_date` datetime DEFAULT NULL,
  `utang_information` text,
  `is_delete` int(1) DEFAULT '0',
  PRIMARY KEY (`utang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_utang
-- ----------------------------
